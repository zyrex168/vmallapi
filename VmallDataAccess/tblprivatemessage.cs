//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace VmallDataAccess
{
    using System;
    using System.Collections.Generic;
    
    public partial class tblprivatemessage
    {
        public long fldID { get; set; }
        public Nullable<System.DateTime> fldDateTime { get; set; }
        public Nullable<long> fldFrMID { get; set; }
        public string fldFrType { get; set; }
        public Nullable<long> fldToMID { get; set; }
        public string fldToType { get; set; }
        public string fldSubject { get; set; }
        public string fldContent { get; set; }
        public Nullable<bool> fldFrRead { get; set; }
        public Nullable<bool> fldToRead { get; set; }
        public Nullable<bool> fldFrDelete { get; set; }
        public Nullable<bool> fldToDelete { get; set; }
        public Nullable<long> fldReplyID { get; set; }
    }
}
