//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace VmallDataAccess
{
    using System;
    using System.Collections.Generic;
    
    public partial class tblgroupsales_leadership_hist
    {
        public System.DateTime fldDate { get; set; }
        public long fldMID { get; set; }
        public long fldFMID { get; set; }
        public long fldTID { get; set; }
        public Nullable<decimal> fldGSP { get; set; }
        public Nullable<decimal> fldBF { get; set; }
        public Nullable<decimal> fldCF { get; set; }
        public Nullable<int> fldLevel { get; set; }
        public Nullable<short> fldProcess { get; set; }
    }
}
