//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace VmallDataAccess
{
    using System;
    using System.Collections.Generic;
    
    public partial class tblsetting
    {
        public long fldID { get; set; }
        public string fldSetting { get; set; }
        public string fldValue { get; set; }
        public string fldDescription { get; set; }
    }
}
