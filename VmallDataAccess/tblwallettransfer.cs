//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace VmallDataAccess
{
    using System;
    using System.Collections.Generic;
    
    public partial class tblwallettransfer
    {
        public long fldID { get; set; }
        public long fldFrMID { get; set; }
        public string fldFrWallet { get; set; }
        public long fldToMID { get; set; }
        public string fldToWallet { get; set; }
        public Nullable<decimal> fldAmount { get; set; }
        public Nullable<decimal> fldFees { get; set; }
        public string fldReference { get; set; }
        public Nullable<System.DateTime> fldDateTime { get; set; }
        public Nullable<long> fldCreator { get; set; }
        public string fldCreatorType { get; set; }
    }
}
