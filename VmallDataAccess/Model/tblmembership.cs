﻿using FluentValidation;
using FluentValidation.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VmallDataAccess
{
    [Validator(typeof(TblMembershipValidator))]
    public partial class tblmembership
    {
        public string fldPlacementCode { get; set; }
    }

    public class TblMembershipValidator : AbstractValidator<tblmembership>
    {
        public TblMembershipValidator()
        {
            RuleFor(x => x.fldSponsorCode).NotEmpty().WithMessage("*Invalid sponsor ID")
                                          .Length(0, 100).WithMessage("*Invalid sponsor ID");

            RuleFor(x => x.fldPlacementCode).NotEmpty().WithMessage("*Invalid placement ID")
                                          .Length(0, 100).WithMessage("*Invalid placement ID");

            RuleFor(x => x.fldCode).NotEmpty().WithMessage("*Member ID cannot be blank")
                                          .Length(0, 20).WithMessage("*Invalid Member ID");

            RuleFor(x => x.fldName).NotEmpty().WithMessage("*Name cannot be blank")
                                          .Length(0, 500).WithMessage("*Invalid Name");

            RuleFor(x => x.fldEMail).NotEmpty().WithMessage("*E-mail address cannot be blank")
                                    .EmailAddress().WithMessage("*Invalid e-mail address")
                                    .Length(0, 50).WithMessage("*Invalid e-mail address");

            RuleFor(x => x.fldWeChatID).NotEmpty().WithMessage("*WeChat ID cannot be blank!")
                                    .Length(0, 50).WithMessage("*Invalid WeChat ID");

            RuleFor(x => x.fldMobileNo).NotEmpty().WithMessage("*Contact number cannot be blank")
                                    .Length(0, 30).WithMessage("*Invalid Contact number");

            RuleFor(x => x.fldCountryID).NotEmpty().WithMessage("*Please select a country")
                                    .Length(0, 2).WithMessage("Invalid Country");

            RuleFor(x => x.fldAccNo).NotEmpty().WithMessage("*Bank account number cannot be blank")
                                          .Length(0, 40).WithMessage("*Invalid Bank account");

            RuleFor(x => x.fldAccHolder).NotEmpty().WithMessage("*Bank account name cannot be blank")
                                          .Length(0, 100).WithMessage("*Invalid Bank account name");

            RuleFor(x => x.fldBankID).GreaterThan(0).WithMessage("*Bank account name cannot be blank");
                                          
        }

    }
}
