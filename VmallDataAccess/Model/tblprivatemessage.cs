﻿using FluentValidation;
using FluentValidation.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VmallDataAccess
{
    [Validator(typeof(TblprivatemessageValidator))]
    public partial class tblprivatemessage
    {
        public string toMember;
    }

    public class TblprivatemessageValidator : AbstractValidator<tblprivatemessage>
    {
        public TblprivatemessageValidator()
        {
            RuleFor(x => x.fldSubject).NotEmpty().WithMessage("Subject cannot be blank");
            RuleFor(x => x.fldContent).NotEmpty().WithMessage("Content cannot be blank");
        }

    }
}
