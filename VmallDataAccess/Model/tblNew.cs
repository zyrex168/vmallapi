﻿using FluentValidation;
using FluentValidation.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VmallDataAccess
{
    [Validator(typeof(tblNewValidator))]
    public partial class tblnew
    {
    }

    public class tblNewValidator : AbstractValidator<tblnew>
    {
        public tblNewValidator()
        {
            List<string> lstStatus = new List<string>  { "Y", "D", "N" };

            //RuleFor(x => x.fldCreatorName).NotEmpty().WithMessage("*Creator name cannot be blank!");
            RuleFor(x => x.fldSubject).NotEmpty().WithMessage("*Subject cannot be blank!");
            RuleFor(x => x.fldContent).NotEmpty().WithMessage("*Content cannot be blank!");
            RuleFor(x => x.fldStatus).Must(x => lstStatus.Contains(x)).WithMessage("Invalid status");
            
        }
    }
}
