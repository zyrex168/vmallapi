﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VmallService.ResponseModel
{
    public class WalletTranType
    {
        public List<string> C { get; set; }
        public List<string> E { get; set; }
        public List<string> B { get; set; }
        public List<string> W { get; set; }
        public List<string> O { get; set; }
        public List<string> A { get; set; }
        public List<string> S { get; set; }
        public List<string> L { get; set; }
    }
}