﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VmallService.ResponseModel
{
    public class PlacementTreeModel
    {
        public string fldName { get; set; }
        public string fldCode { get; set; }
        public short fldSales { get; set; } //account type, 0="False Account" 1="True Account" 2="Free Account"
        public int fldRank { get; set; }
        public int organization { get; set; }
        public DateTime fldDateJoin { get; set; }
        public object tree { get; set; }
    }
}