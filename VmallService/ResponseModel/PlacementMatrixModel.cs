﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VmallService.ResponseModel
{
    public class PlacementMatrixModel
    {
        //public string fldCode { get; set; }
        //public int fldMid { get; set; }
        //public string fldName { get; set; }
        //public long fldSponsorID { get; set; }
        //public string fldSponsorCode { get; set; }
        //public int fldAc { get; set; }
        //public int fldRank { get; set; }
        //public string fldStatus { get; set; }
        //public short fldSales { get; set; } //account type, 0="False Account" 1="True Account" 2="Free Account"
        public decimal totalGPV { get; set; }
        public decimal totalBfLeftGPV { get; set; }
        public decimal totalBfRightGPV { get; set; }
        public decimal totalCurrentLeftGPV { get; set; }
        public decimal totalCurrentRightGPV { get; set; }

        public Node tree;

        //public class Tree
        //{
        //    public Node root;
        //    public Node left;
        //    public Node right;
        //}

        public class Node
        {
            public string fldName { get; set; }
            public string fldCode { get; set; }
            public int fldAc { get; set; }
            public string fldStatus { get; set; }
            public short fldSales { get; set; } //account type, 0="False Account" 1="True Account" 2="Free Account"
            public long fldSponsorID { get; set; }
            public string fldSponsorCode { get; set; }
            public int fldMid { get; set; }
            public int fldPlacementMID { get; set; }
            public int fldPlacementAc { get; set; }
            public int fldPlacementGroup { get; set; }
            public int fldRank { get; set; }
            public Node Left;
            public Node Right;
        }
    }
}