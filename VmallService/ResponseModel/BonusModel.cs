﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VmallService.ResponseModel
{
    public class BonusModel
    {
        public double fldMatching { get; set; }
        public double fldPairing { get; set; }
        public double fldLeadership { get; set; }
        public double fldUnilevel { get; set; }
        public double fldSponsorReward { get; set; }
    }
}