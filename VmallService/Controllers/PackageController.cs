﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Web.Http;
using VmallDataAccess;
using VmallService.Base;
using VmallService.Base.Filters;
using VmallService.CommonFunction;
using VmallService.Controllers.Base;
using VmallService.RequestModel;
using VmallService.ResponseModel;

namespace VmallService.Controllers
{
    [BasicAuthentication]
    [RoutePrefix("api/package")]
    public class PackageController : BaseController
    {
        [HttpGet]
        [Route("direct/{sponsorId}/{packageId}/{year}/{month}")]
        public IHttpActionResult GetPackage(long sponsorId, long packageId, int year, int month)
        {
            long userId = GetUserId();
            using (VmallDbEntities entities = new VmallDbEntities())
            {

                var query = from t in entities.tbltransactions
                            join m in entities.tblmemberships on t.fldMID equals m.fldID
                            where m.fldSponsorID == sponsorId && t.fldPackageID == packageId &&
                                  ((DateTime)t.fldDateTime).Year == year &&
                                  ((DateTime)t.fldDateTime).Month == month
                            select new { t.fldID };

                RecordCount objOut = new RecordCount
                {
                    count = query.Count()
                };


                return Ok(objOut);
            }
        }

        [HttpPost]
        [Route("upgrade")]
        public IHttpActionResult UpgradePackage([FromBody] PackageUpgradeModel request)
        {
            long userId = GetUserId();
            String strPrefix = "UG";

            if (base.isAdmin)
                return BadRequest();

            string strRef = FuncTool.GenerateRef(strPrefix);
            string strErr = ValidationUpgradePackage(request);
            decimal dcFromMemberBf = 0;
            decimal dmFromMemberNewBal = 0;

            if (strErr != "")
            {
                ModelState.AddModelError("", strErr);
                return BadRequest(ModelState);

            }


            using (VmallDbEntities entities = new VmallDbEntities())
            {

                var member = entities.tblmemberships.SingleOrDefault(x => x.fldCode == request.memberId);
                var package = entities.tblpackages.SingleOrDefault(x => x.fldID == request.fldRank);
                var fromWalletMember = entities.tblmemberships.SingleOrDefault(x => x.fldID == userId);
                var currentPackage = entities.tblpackages.SingleOrDefault(x => x.fldID == member.fldRank);

                member.fldRank = request.fldRank;
                member.fldAWallet = package.fldPrice;
                member.fldWWallet = Convert.ToDecimal((double)(package.fldPrice) * 0.6);

                #region EP(UPGRADE Point)
                if (request.paymentType.Contains("EP"))
                {
                    dcFromMemberBf = 0;
                    dmFromMemberNewBal = 0;


                    dcFromMemberBf = (decimal)fromWalletMember.fldEWallet;
                    dmFromMemberNewBal = (decimal)fromWalletMember.fldEWallet - request.fldEWallet;

                    //update balance
                    fromWalletMember.fldEWallet = dmFromMemberNewBal;

                    // trf out from user
                    tblewallettransaction walletTrx = new tblewallettransaction();
                    walletTrx.fldMID = fromWalletMember.fldID;
                    walletTrx.fldType = "UPGRADE";
                    walletTrx.fldReferenceID = strRef;
                    walletTrx.fldCreator = userId;
                    walletTrx.fldDateTime = DateTime.Now;
                    walletTrx.fldCreatorType = base.isAdmin ? "A" : "M";
                    walletTrx.fldBF = dcFromMemberBf;
                    walletTrx.fldIn = 0;
                    walletTrx.fldOut = request.fldEWallet;
                    walletTrx.fldCF = dmFromMemberNewBal;
                    walletTrx.fldRemark = "Upgrade from  " + currentPackage.fldPackage + " to " + package.fldPackage + ", A/C: " + request.placementAcct + ", Price: " + ((decimal)package.fldPrice).ToString("###,###,##0.00");
                    entities.tblewallettransactions.Add(walletTrx);

                }
                #endregion

                #region CP(Cash Point)
                if (request.paymentType.Contains("CP"))
                {
                    dcFromMemberBf = (decimal)fromWalletMember.fldCWallet;
                    dmFromMemberNewBal = (decimal)fromWalletMember.fldCWallet - request.fldCWallet;

                    //update balance
                    fromWalletMember.fldCWallet = dmFromMemberNewBal;

                    // trf out from user
                    tblcwallettransaction walletTrx = new tblcwallettransaction();
                    walletTrx.fldMID = fromWalletMember.fldID;
                    walletTrx.fldType = "UPGRADE";
                    walletTrx.fldReferenceID = strRef;
                    walletTrx.fldCreator = userId;
                    walletTrx.fldDateTime = DateTime.Now;
                    walletTrx.fldCreatorType = base.isAdmin ? "A" : "M";
                    walletTrx.fldBF = dcFromMemberBf;
                    walletTrx.fldIn = 0;
                    walletTrx.fldOut = request.fldCWallet;
                    walletTrx.fldCF = dmFromMemberNewBal;
                    walletTrx.fldRemark = "Upgrade from  " + currentPackage.fldPackage + " to " + package.fldPackage + ", A/C: " + request.placementAcct + ", Price: " + ((decimal)package.fldPrice).ToString("###,###,##0.00");

                    entities.tblcwallettransactions.Add(walletTrx);

                }
                #endregion

                #region SP(Sold Point)
                if (request.paymentType.Contains("SP"))
                {
                    dcFromMemberBf = (decimal)fromWalletMember.fldOWallet;
                    dmFromMemberNewBal = (decimal)fromWalletMember.fldOWallet - request.fldOWallet;

                    //update balance
                    fromWalletMember.fldOWallet = dmFromMemberNewBal;

                    // trf out from user
                    tblowallettransaction walletTrx = new tblowallettransaction();
                    walletTrx.fldMID = fromWalletMember.fldID;
                    walletTrx.fldType = "UPGRADE";
                    walletTrx.fldReferenceID = strRef;
                    walletTrx.fldCreator = userId;
                    walletTrx.fldDateTime = DateTime.Now;
                    walletTrx.fldCreatorType = base.isAdmin ? "A" : "M";
                    walletTrx.fldBF = dcFromMemberBf;
                    walletTrx.fldIn = 0;
                    walletTrx.fldOut = request.fldOWallet;
                    walletTrx.fldCF = dmFromMemberNewBal;
                    walletTrx.fldRemark = "Upgrade from  " + currentPackage.fldPackage + " to " + package.fldPackage + ", A/C: " + request.placementAcct + ", Price: " + ((decimal)package.fldPrice).ToString("###,###,##0.00");

                    entities.tblowallettransactions.Add(walletTrx);

                }

                #endregion

                #region Transaction
                string strRemark = "";
                strRemark = "Upgrade from  " + currentPackage.fldPackage + " to " + package.fldPackage + ", A/C: " + request.placementAcct + ", Price: " + ((decimal)package.fldPrice).ToString("###,###,##0.00");
                if (request.paymentType.Contains("EP"))
                    strRemark = strRemark + ", EP Paid: " + request.fldEWallet;
                if (request.paymentType.Contains("CP"))
                    strRemark = strRemark + ", CP Paid: " + request.fldCWallet;
                if (request.paymentType.Contains("SP"))
                    strRemark = strRemark + ", SP Paid: " + request.fldOWallet;


                tbltransaction trx = new tbltransaction();
                trx.fldDateTime = DateTime.Now;
                trx.fldMID = member.fldID;
                trx.fldPlacementAC = 0;
                trx.fldType = "UPGRADE";
                trx.fldReferenceID = strRef;
                trx.fldFrRank = 0;
                trx.fldPackageID = package.fldID;
                trx.fldWallet = "C";
                trx.fldAmount = package.fldPrice;
                trx.fldFee = 0;
                trx.fldWalletMID = fromWalletMember == null ? 0 : fromWalletMember.fldID;
                trx.fldPaymentType = request.paymentType;
                trx.fldRemark = strRemark;
                trx.fldCreator = userId;
                trx.fldCreatorType = base.isAdmin ? "A" : "M";
                trx.fldSales = member.fldSales;
                trx.fldProcess = 0;
                trx.fldVClubAcc = request.freeVclub;
                trx.fldVClubAccProcess = 1;

                entities.tbltransactions.Add(trx);
                #endregion

                #region tblunittree
                var tree = entities.tblunittrees.Where(x => x.fldMID == member.fldID).ToList();

                if(tree != null)
                {
                    foreach (tblunittree item in tree)
                    {
                        if(item.fldAc == 1)
                            item.fldRank = request.fldRank;

                        if(request.fldRank > 2)
                        {
                            if (item.fldAc == 2 || item.fldAc == 3)
                            {
                                item.fldRank = request.fldRank - 1;
                                item.fldStatus = "Y";
                            }
                                
                        }
                    }
                }

                #endregion

                #region tblbinary
                var binary = entities.tblbinaries.Where(x => x.fldMID == member.fldID).ToList();

                if (binary != null)
                {
                    foreach (tblbinary item in binary)
                    {
                        if (request.fldRank > 2)
                        {
                            if (item.fldAc == 2 || item.fldAc == 3)
                            {
                                item.fldStatus = "Y";
                            }

                        }
                    }
                }

                #endregion
                entities.SaveChanges();

                return Ok();
            }
        }

        private string ValidationUpgradePackage(PackageUpgradeModel request)
        {
            string strErr = "";
            long userId = GetUserId();

            using (VmallDbEntities entities = new VmallDbEntities())
            {
                var member = entities.tblmemberships.SingleOrDefault(x => x.fldCode == request.memberId);
                var package = entities.tblpackages.SingleOrDefault(x => x.fldID == request.fldRank);
                var walletMember = entities.tblmemberships.SingleOrDefault(x => x.fldID == userId);
                var currentPackage = new tblpackage();

                if (member == null)
                {
                    strErr = "*Member ID not found";
                }
                else
                {
                    currentPackage = entities.tblpackages.SingleOrDefault(x => x.fldID == member.fldRank);
                }

                if (strErr == "")
                {
                    if(request.fldRank <= member.fldRank)
                    {
                        strErr = "Package must be higher";
                    }
                }

                if (strErr == "")
                {
                    

                    if ((request.fldCWallet + request.fldEWallet + request.fldOWallet + currentPackage.fldPrice) != package.fldPrice)
                    {
                        strErr = "*Invalid amount";
                    }
                }

                if (strErr == "")
                {
                    if (request.paymentType.Contains("EP"))
                    {
                        if (request.fldEWallet < (Convert.ToDecimal((double)((package.fldPrice - currentPackage.fldPrice)) * 0.5)))
                        {
                            strErr = "EP(UPGRADE Point) payment amount must be or more than 50.0% of total price " + package.fldPrice;
                        }
                    }
                }


                if (strErr == "")
                {
                    if (request.paymentType.Contains("EP") && walletMember.fldEWallet < request.fldEWallet)
                    {
                        strErr = "*Insufficient wallet balance";
                    }
                }

                if (strErr == "")
                {
                    if (request.paymentType.Contains("CP") && walletMember.fldCWallet < request.fldCWallet)
                    {
                        strErr = "*Insufficient wallet balance";
                    }
                }

                if (strErr == "")
                {
                    if (request.paymentType.Contains("SP") && walletMember.fldOWallet < request.fldOWallet)
                    {
                        strErr = "*Insufficient wallet balance";
                    }
                }

            }
            return strErr;
        }

        
    }
}
