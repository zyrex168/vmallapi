﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Web.Http;
using VmallDataAccess;
using VmallService.Base;
using VmallService.Base.Filters;
using VmallService.Controllers.Base;
using VmallService.ResponseModel;

namespace VmallService.Controllers
{
    [RoutePrefix("api/login")]
    public class LoginController : BaseController
    {
        [BasicAuthentication]
        [Route("")]
        public IHttpActionResult Get()
        {
            long userId = GetUserId();
            
            //dynamic queryUser = null;
            string strUserType = "M";
            LoginModel loginModel = new LoginModel();

            using (VmallDbEntities entities = new VmallDbEntities())
            {

                //var queryUser = entities.tblmemberships.SingleOrDefault(x => x.fldCode.Equals(userName, StringComparison.OrdinalIgnoreCase))
                var queryUser = entities.tblmemberships.Where(x => x.fldID == userId)
                                                        .Select(c => new
                                                        {
                                                            c.fldMemberCode,
                                                            c.fldName,
                                                            c.fldNickName,
                                                            c.fldICNo,
                                                            c.fldDOB,
                                                            c.fldSex,
                                                            c.fldRace,
                                                            c.fldEMail,
                                                            c.fldDateJoin,
                                                            fldLevel = 0,
                                                            isAdmin = false
                                                        })
                                                        .SingleOrDefault();


                if(queryUser == null)
                {
                    var adminUser = entities.tbladmins.Where(x => x.fldID == userId)
                                                        .Select(c => new
                                                        {
                                                            fldMemberCode = c.fldCode,
                                                            fldName = c.fldName,
                                                            fldNickName = "",
                                                            fldICNo = c.fldICNo,
                                                            fldDOB = "",
                                                            fldSex = "",
                                                            fldRace = "",
                                                            fldEMail = "",
                                                            fldDateJoin = "",
                                                            fldLevel = c.fldLevel == 0? 1: c.fldLevel,
                                                            isAdmin = true
                                                        })
                                                        .SingleOrDefault();

                    strUserType = "A";

                    var adminMenu = from menu in entities.tbladminmenus
                                    from role in entities.tblrolemenus.Where(x=>x.fldMenuID == menu.fldMenuID && x.fldRoleID == adminUser.fldLevel)
                                    where menu.fldDisplay == true && menu.fldStatus == true && role.fldUserType == strUserType
                                    select menu;


                    loginModel.Menu = adminMenu.ToArray();
                    loginModel.Profile = adminUser;

                    var adminUserUpdate = entities.tbladmins.Where(x => x.fldID == userId).SingleOrDefault();
                    adminUserUpdate.fldLastLogin = DateTime.Now;
                    entities.SaveChanges();
                }
                else
                {
                    var queryMenu = from menu in entities.tblmembermenus
                                    join role in entities.tblrolemenus on menu.fldMenuID equals role.fldMenuID
                                    where menu.fldDisplay == true && menu.fldStatus == true && role.fldUserType == strUserType
                                    select menu;


                    loginModel.Menu = queryMenu.ToArray();
                    loginModel.Profile = queryUser;

                    var userUpdate = entities.tblmemberships.Where(x => x.fldID == userId).SingleOrDefault();
                    userUpdate.fldLastLogin = DateTime.Now;
                    entities.SaveChanges();
                }
                

                //return Request.CreateResponse(HttpStatusCode.OK, loginModel);
                return Ok(loginModel);
            }
        }
    }
}
