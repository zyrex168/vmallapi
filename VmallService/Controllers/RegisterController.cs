﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Web.Http;
using VmallDataAccess;
using VmallService.Base;
using VmallService.Base.Filters;
using VmallService.CommonFunction;
using VmallService.Controllers.Base;
using VmallService.RequestModel;

namespace VmallService.Controllers
{
    [RoutePrefix("api/register")]
    [BasicAuthentication]
    public class RegisterController : BaseController
    {
        [Route("")]
        [HttpPost]
        public IHttpActionResult Register([FromBody] RegisterMemberModel request)
        {
            long userId = base.GetUserId();
            string strRef = GenerateRef();

            //if (!isAdmin)
            //    return BadRequest();

            string strErr = ValidationRegister(request);

            if (strErr != "")
            {
                ModelState.AddModelError("", strErr);
                return BadRequest(ModelState);

            }


            using (VmallDbEntities entities = new VmallDbEntities())
            {
                DbContextTransaction transaction = entities.Database.BeginTransaction();

                try
                {


                    var sponsor = entities.tblmemberships.SingleOrDefault(x => x.fldCode == request.fldSponsorCode);
                    var package = entities.tblpackages.SingleOrDefault(x => x.fldID == request.fldRank);
                    var fromWalletMember = entities.tblmemberships.SingleOrDefault(x => x.fldCode == request.walletMemberId);

                    tblmembership member = new tblmembership();

                    member.fldLanguage = "ZH-CN";
                    member.fldCode = request.memberId;
                    member.fldMemberCode = request.memberId;
                    member.fldExpiryDate = DateTime.MinValue;
                    member.fldGroup = 0;
                    member.fldExpire = DateTime.MinValue;
                    member.fldStockistDate = DateTime.MinValue;
                    member.fldDL = 0;

                    member.fldName = request.fldName;
                    member.fldNickName = request.fldName;
                    member.fldDOB = DateTime.MinValue;
                    member.fldICNo = request.fldICNo;
                    member.fldEMail = request.fldEMail;
                    member.fldWeChatID = request.fldWeChatID;
                    member.fldMobileNo = request.fldMobileNo;
                    member.fldAddress1 = request.fldAddress1;
                    member.fldPostCode = request.fldPostCode;
                    member.fldCountryID = request.fldCountryID;
                    member.fldSex = "M";
                    member.fldRace = "O";
                    member.fldPassword = request.fldMobileNo.Length >= 6 ? request.fldMobileNo.Substring(0, 6) : request.fldMobileNo;
                    member.fldTransactionPassword = member.fldPassword;
                    member.fldStatus = "A";
                    member.fldType = 1;
                    member.fldAutoWithdraw = "Y";
                    member.fldDateJoin = DateTime.Now;

                    member.fldSales = request.fldSales;
                    member.fldRank = request.fldRank;

                    member.fldBRelationship = request.fldBRelationship ?? "";
                    member.fldBName = request.fldBName ?? "";
                    member.fldBIC = request.fldBIC ?? "";
                    member.fldBContactNo = request.fldBContactNo;
                    member.fldBAddress = request.fldBAddress ?? "";
                    member.fldBPostcode = request.fldBPostcode ?? "";
                    member.fldBCountryID = request.fldBCountryID;

                    member.fldAccNo = request.fldAccNo;
                    member.fldAccHolder = request.fldAccHolder;
                    member.fldBankID = request.fldBankID;
                    member.fldBankBranch = request.fldBankBranch;

                    member.fldSponsorID = sponsor.fldID;
                    member.fldSponsorCode = request.fldSponsorCode;
                    member.fldSponsorLevel = request.placementAcct;
                    member.fldRegisterID = strRef;

                    member.fldShare = 0;
                    member.fldShare2 = 0;
                    member.fldLastLoginAttempt = DateTime.MinValue;
                    member.fldLastLogin = DateTime.MinValue;
                    member.fldLastLogout = DateTime.MinValue;
                    member.fldLoginAttempt = 0;
                    member.fldTransactionAttempt = 0;

                    member.fldActivate = "Y";
                    member.fldSCRank = 0;
                    member.fldF = 0;
                    member.fldLD = 0;
                    member.fldLDProcess = 0;
                    member.fldAWallet = 0;
                    member.fldBWallet = 0;
                    member.fldCWallet = 0;
                    member.fldEWallet = 0;
                    member.fldOWallet = 0;
                    member.fldRWallet = 0;
                    member.fldWWallet = 0;


                    decimal dcFromMemberBf = 0;
                    decimal dmFromMemberNewBal = 0;

                    if (request.fldSales == 1)
                    {
                        member.fldAWallet = package.fldPrice;
                        member.fldWWallet = Convert.ToDecimal((double)(package.fldPrice) * 0.6);

                        #region EP(Register Point)
                        if (request.paymentType.Contains("EP"))
                        {
                            dcFromMemberBf = 0;
                            dmFromMemberNewBal = 0;


                            dcFromMemberBf = (decimal)fromWalletMember.fldEWallet;
                            dmFromMemberNewBal = (decimal)fromWalletMember.fldEWallet - request.fldEWallet;

                            //update balance
                            fromWalletMember.fldEWallet = dmFromMemberNewBal;

                            // trf out from user
                            tblewallettransaction walletTrx = new tblewallettransaction();
                            walletTrx.fldMID = fromWalletMember.fldID;
                            walletTrx.fldType = "REGISTER";
                            walletTrx.fldReferenceID = strRef;
                            walletTrx.fldCreator = userId;
                            walletTrx.fldDateTime = DateTime.Now;
                            walletTrx.fldCreatorType = base.isAdmin ? "A" : "M";
                            walletTrx.fldBF = dcFromMemberBf;
                            walletTrx.fldIn = 0;
                            walletTrx.fldOut = request.fldEWallet;
                            walletTrx.fldCF = dmFromMemberNewBal;
                            walletTrx.fldRemark = "Register: " + request.memberId + ", Package: " + package.fldPackage + ", Price: " + ((decimal)package.fldPrice).ToString("###,###,##0.00");
                            entities.tblewallettransactions.Add(walletTrx);

                        }
                        #endregion

                        #region CP(Cash Point)
                        if (request.paymentType.Contains("CP"))
                        {
                            dcFromMemberBf = (decimal)fromWalletMember.fldCWallet;
                            dmFromMemberNewBal = (decimal)fromWalletMember.fldCWallet - request.fldCWallet;

                            //update balance
                            fromWalletMember.fldCWallet = dmFromMemberNewBal;

                            // trf out from user
                            tblcwallettransaction walletTrx = new tblcwallettransaction();
                            walletTrx.fldMID = fromWalletMember.fldID;
                            walletTrx.fldType = "REGISTER";
                            walletTrx.fldReferenceID = strRef;
                            walletTrx.fldCreator = userId;
                            walletTrx.fldDateTime = DateTime.Now;
                            walletTrx.fldCreatorType = base.isAdmin ? "A" : "M";
                            walletTrx.fldBF = dcFromMemberBf;
                            walletTrx.fldIn = 0;
                            walletTrx.fldOut = request.fldCWallet;
                            walletTrx.fldCF = dmFromMemberNewBal;
                            walletTrx.fldRemark = "Register: " + request.memberId + ", Package: " + package.fldPackage + ", Price: " + ((decimal)package.fldPrice).ToString("###,###,##0.00");

                            entities.tblcwallettransactions.Add(walletTrx);

                        }
                        #endregion

                        #region SP(Sold Point)
                        if (request.paymentType.Contains("SP"))
                        {
                            dcFromMemberBf = (decimal)fromWalletMember.fldOWallet;
                            dmFromMemberNewBal = (decimal)fromWalletMember.fldOWallet - request.fldOWallet;

                            //update balance
                            fromWalletMember.fldOWallet = dmFromMemberNewBal;

                            // trf out from user
                            tblowallettransaction walletTrx = new tblowallettransaction();
                            walletTrx.fldMID = fromWalletMember.fldID;
                            walletTrx.fldType = "REGISTER";
                            walletTrx.fldReferenceID = strRef;
                            walletTrx.fldCreator = userId;
                            walletTrx.fldDateTime = DateTime.Now;
                            walletTrx.fldCreatorType = base.isAdmin ? "A" : "M";
                            walletTrx.fldBF = dcFromMemberBf;
                            walletTrx.fldIn = 0;
                            walletTrx.fldOut = request.fldOWallet;
                            walletTrx.fldCF = dmFromMemberNewBal;
                            walletTrx.fldRemark = "Register: " + request.memberId + ", Package: " + package.fldPackage + ", Price: " + ((decimal)package.fldPrice).ToString("###,###,##0.00");

                            entities.tblowallettransactions.Add(walletTrx);

                        }
                        #endregion
                    }


                    entities.tblmemberships.Add(member);
                    entities.SaveChanges();

                    #region tbltransaction
                    tbltransaction trx = null;
                    if (request.fldSales == 1)
                    {
                        string strRemark = "";
                        strRemark = "Register member: " + request.memberId + ", Package: " + package.fldPackage + ", Price: " + ((decimal)package.fldPrice).ToString("###,###,##0.00");
                        if (request.paymentType.Contains("EP"))
                            strRemark = strRemark + ", EP Paid: " + request.fldEWallet;
                        if (request.paymentType.Contains("CP"))
                            strRemark = strRemark + ", CP Paid: " + request.fldCWallet;
                        if (request.paymentType.Contains("SP"))
                            strRemark = strRemark + ", SP Paid: " + request.fldOWallet;


                        trx = new tbltransaction();
                        trx.fldDateTime = DateTime.Now;
                        trx.fldMID = member.fldID;
                        trx.fldPlacementAC = 0;
                        trx.fldType = "REGISTER";
                        trx.fldReferenceID = strRef;
                        trx.fldFrRank = 0;
                        trx.fldPackageID = package.fldID;
                        trx.fldWallet = "C";
                        trx.fldAmount = package.fldPrice;
                        trx.fldFee = 0;
                        trx.fldWalletMID = fromWalletMember == null ? 0 : fromWalletMember.fldID;
                        trx.fldPaymentType = request.paymentType;
                        trx.fldRemark = strRemark;
                        trx.fldCreator = userId;
                        trx.fldCreatorType = base.isAdmin ? "A" : "M";
                        trx.fldSales = request.fldSales;
                        trx.fldProcess = 0;
                        trx.fldVClubAcc = request.freeVclub;
                        trx.fldVClubAccProcess = 1;

                        entities.tbltransactions.Add(trx);
                    }
                    #endregion


                    if (request.fldSales == 1)
                    {
                        MySql.Data.MySqlClient.MySqlParameter paramMemberId = new MySql.Data.MySqlClient.MySqlParameter("@MemberID", member.fldID);
                        MySql.Data.MySqlClient.MySqlParameter paramTranId = new MySql.Data.MySqlClient.MySqlParameter("@TransactionID", trx.fldID);
                        int iResult = entities.Database.ExecuteSqlCommand("call spUpdate_Sales(@MemberID, @TransactionID)", paramMemberId, paramTranId);

                        //var result = entities.Database.SqlQuery <int>("call spUpdate_Sales(@MemberID, @TransactionID)", paramMemberId, paramTranId);

                    }

                    #region tblunittree
                    tblunittree tree = new tblunittree();
                    var placementMember = entities.tblmemberships.SingleOrDefault(x => x.fldCode == request.placementId);

                    tree.fldMID = Convert.ToInt32(member.fldID);
                    tree.fldAc = 1;
                    tree.fldPlacementMID = Convert.ToInt32(placementMember.fldID);
                    tree.fldPlacementAc = request.placementAcct;
                    tree.fldPlacementGroup = request.placementPosition;
                    tree.fldRank = package.fldID;
                    tree.fldDate = DateTime.Now;
                    tree.fldAmount = 0;
                    tree.fldTransactionID = "";
                    tree.fldStatus = "Y";
                    tree.fldPlacementLevel = 1;
                    tree.fldPlacementLevel2 = 1;
                    entities.tblunittrees.Add(tree);


                    tree = new tblunittree();
                    tree.fldMID = Convert.ToInt32(member.fldID);
                    tree.fldAc = 2;
                    tree.fldPlacementMID = Convert.ToInt32(member.fldID);
                    tree.fldPlacementAc = 1;
                    tree.fldPlacementGroup = 1;
                    tree.fldRank = package.fldID > 2 ? package.fldID - 1 : 0;
                    tree.fldDate = DateTime.Now;
                    tree.fldAmount = 0;
                    tree.fldTransactionID = "";
                    tree.fldStatus = package.fldID > 2 ? "Y" : "N";
                    tree.fldPlacementLevel = 1;
                    tree.fldPlacementLevel2 = 1;
                    entities.tblunittrees.Add(tree);

                    tree = new tblunittree();
                    tree.fldMID = Convert.ToInt32(member.fldID);
                    tree.fldAc = 3;
                    tree.fldPlacementMID = Convert.ToInt32(member.fldID);
                    tree.fldPlacementAc = 1;
                    tree.fldPlacementGroup = 2;
                    tree.fldRank = package.fldID > 2 ? package.fldID - 1 : 0;
                    tree.fldDate = DateTime.Now;
                    tree.fldAmount = 0;
                    tree.fldTransactionID = "";
                    tree.fldStatus = package.fldID > 2 ? "Y" : "N";
                    tree.fldPlacementLevel = 1;
                    tree.fldPlacementLevel2 = 1;
                    entities.tblunittrees.Add(tree);

                    #endregion

                    #region tblbinary
                    tblbinary binary = new tblbinary();
                    binary.fldDate = DateTime.Now;
                    binary.fldMID = Convert.ToInt32(member.fldID);
                    binary.fldAc = 1;
                    binary.fldPMID = Convert.ToInt32(placementMember.fldID);
                    binary.fldPAc = request.placementAcct;
                    binary.fldGroup = request.placementPosition;
                    binary.fldStatus = "Y";
                    binary.fldBF1 = 0;
                    binary.fldBF2 = 0;
                    binary.fldCF1 = 0;
                    binary.fldCF2 = 0;
                    binary.fldFL1 = 0;
                    binary.fldFL2 = 0;
                    binary.fldMatch = 0;
                    binary.fldPair = 0;
                    binary.fldMax = 0;
                    binary.fldBonus = 0;
                    binary.fldSize1 = 0;
                    binary.fldSize2 = 0;

                    entities.tblbinaries.Add(binary);

                    binary = new tblbinary();
                    binary.fldDate = DateTime.Now;
                    binary.fldMID = Convert.ToInt32(member.fldID);
                    binary.fldAc = 2;
                    binary.fldPMID = Convert.ToInt32(member.fldID);
                    binary.fldPAc = 1;
                    binary.fldGroup = 1;
                    binary.fldStatus = package.fldID > 2 ? "Y" : "N";
                    binary.fldBF1 = 0;
                    binary.fldBF2 = 0;
                    binary.fldCF1 = 0;
                    binary.fldCF2 = 0;
                    binary.fldFL1 = 0;
                    binary.fldFL2 = 0;
                    binary.fldMatch = 0;
                    binary.fldPair = 0;
                    binary.fldMax = 0;
                    binary.fldBonus = 0;
                    binary.fldSize1 = 0;
                    binary.fldSize2 = 0;
                    entities.tblbinaries.Add(binary);

                    binary = new tblbinary();
                    binary.fldDate = DateTime.Now;
                    binary.fldMID = Convert.ToInt32(member.fldID);
                    binary.fldAc = 3;
                    binary.fldPMID = Convert.ToInt32(member.fldID);
                    binary.fldPAc = 1;
                    binary.fldGroup = 2;
                    binary.fldStatus = package.fldID > 2 ? "Y" : "N";
                    binary.fldBF1 = 0;
                    binary.fldBF2 = 0;
                    binary.fldCF1 = 0;
                    binary.fldCF2 = 0;
                    binary.fldFL1 = 0;
                    binary.fldFL2 = 0;
                    binary.fldMatch = 0;
                    binary.fldPair = 0;
                    binary.fldMax = 0;
                    binary.fldBonus = 0;
                    binary.fldSize1 = 0;
                    binary.fldSize2 = 0;
                    entities.tblbinaries.Add(binary);
                    #endregion

                    #region tblgroupsales
                    tblgroupsale groupSales = new tblgroupsale();
                    groupSales.fldYM = DateTime.Now.ToString("yyyyMM");
                    groupSales.fldDate = DateTime.Now;
                    groupSales.fldMID = (int) member.fldID;
                    groupSales.fldSP = package.fldPrice;
                    groupSales.fldGSP = 0;

                    entities.tblgroupsales.Add(groupSales);
                    #endregion

                    entities.SaveChanges();
                    SendEmail(member);

                    transaction.Commit();

                    

                    return Ok();

                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw ex;
                }
            }
        }

        private string GenerateRef()
        {
            DateTime dt = DateTime.Now;
            string month = "";

            switch (dt.Month)
            {
                case 1:
                    month = "J";
                    break;
                case 2:
                    month = "F";
                    break;
                case 3:
                    month = "M";
                    break;
                case 4:
                    month = "A";
                    break;
                case 5:
                    month = "Y";
                    break;
                case 6:
                    month = "N";
                    break;
                case 7:
                    month = "L";
                    break;
                case 8:
                    month = "G";
                    break;
                case 9:
                    month = "S";
                    break;
                case 10:
                    month = "O";
                    break;
                case 11:
                    month = "V";
                    break;
                case 12:
                    month = "D";
                    break;
            }

            string str = "RG" + month + dt.ToString("dd") + dt.ToString("yy") + dt.ToString("HHmmfff");
            return str;

        }

        private string ValidationRegister(RegisterMemberModel request)
        {
            string strErr = "";
            using (VmallDbEntities entities = new VmallDbEntities())
            {
                var member = entities.tblmemberships.SingleOrDefault(x => x.fldCode == request.memberId);
                var walletMember = entities.tblmemberships.SingleOrDefault(x => x.fldCode == request.walletMemberId);
                var sponsor = entities.tblmemberships.SingleOrDefault(x => x.fldCode == request.fldSponsorCode);
                var package = entities.tblpackages.SingleOrDefault(x => x.fldID == request.fldRank);
                var placementMember = entities.tblmemberships.SingleOrDefault(x => x.fldCode == request.placementId);

                if (member != null)
                {
                    strErr = "*Member ID already exists";
                }

                if (strErr == "")
                {
                    if (sponsor == null)
                    {
                        strErr = "*Invalid sponsor ID";
                    }
                }

                if (strErr == "")
                {
                    if (placementMember == null)
                    {
                        strErr = "*Invalid placement ID";
                    }
                }

                //placement
                if (strErr == "")
                {
                    var placement = entities.tblunittrees.SingleOrDefault(x => x.fldPlacementMID == placementMember.fldID &&
                                                                          x.fldPlacementAc == request.placementAcct &&
                                                                          x.fldPlacementGroup == request.placementPosition);

                    if (placement != null)
                    {
                        strErr = "*Placement account (" + request.placementPosition + ") full";
                    }
                }

                if (request.paymentType.ToLower() == "cash" || request.paymentType.ToLower() == "creditcard" || request.paymentType.ToLower() == "cheque")
                {

                }
                else
                {
                    if (strErr == "")
                    {
                        if (walletMember == null)
                        {
                            strErr = "Wallet Member ID not found";
                        }
                    }

                    if (strErr == "")
                    {
                        if ((request.fldCWallet + request.fldEWallet + request.fldOWallet) != package.fldPrice)
                        {
                            strErr = "*Invalid amount";
                        }
                    }

                    if (strErr == "")
                    {
                        if (request.paymentType.Contains("EP"))
                        {
                            if (request.fldEWallet < (Convert.ToDecimal((double)(package.fldPrice) * 0.5)))
                            {
                                strErr = "EP(Register Point) payment amount must be or more than 50.0% of total price " + package.fldPrice;
                            }
                        }
                    }

                    if (strErr == "")
                    {
                        if (request.paymentType.Contains("CP") && request.paymentType.Contains("SP"))
                        {
                            if (request.fldCWallet < (Convert.ToDecimal((double)(package.fldPrice) * 0.1)) ||
                                request.fldOWallet < (Convert.ToDecimal((double)(package.fldPrice) * 0.1)))
                            {
                                strErr = "CP(Cash Point) / SP(Sold Point) payment amount must be or more than 10.0% of total price " + package.fldPrice;
                            }
                        }
                    }

                    if (strErr == "")
                    {
                        if (request.paymentType.Contains("EP") && walletMember.fldEWallet < request.fldEWallet)
                        {
                            strErr = "*Insufficient wallet balance";
                        }
                    }

                    if (strErr == "")
                    {
                        if (request.paymentType.Contains("CP") && walletMember.fldCWallet < request.fldCWallet)
                        {
                            strErr = "*Insufficient wallet balance";
                        }
                    }

                    if (strErr == "")
                    {
                        if (request.paymentType.Contains("SP") && walletMember.fldOWallet < request.fldOWallet)
                        {
                            strErr = "*Insufficient wallet balance";
                        }
                    }
                }
            }




            return strErr;
        }

        private void SendEmail(tblmembership member)
        {
            
            using (VmallDbEntities entities = new VmallDbEntities())
            {
                
                string emailLogo = System.Configuration.ConfigurationManager.AppSettings.Get("EmailLogo").ToString();
                string emailTemplate = System.Configuration.ConfigurationManager.AppSettings.Get("emailTemplate").ToString();
                string loginUrl = System.Configuration.ConfigurationManager.AppSettings.Get("LoginUrl").ToString();
                string companyName = System.Configuration.ConfigurationManager.AppSettings.Get("vCOMPANY").ToString();
                string smtpServer = System.Configuration.ConfigurationManager.AppSettings.Get("SmtpServer").ToString();
                string smtpPort = System.Configuration.ConfigurationManager.AppSettings.Get("SmtpPort").ToString();
                string smtpUser = System.Configuration.ConfigurationManager.AppSettings.Get("SmtpUser").ToString();
                string smtpPassword = System.Configuration.ConfigurationManager.AppSettings.Get("SmtpPassword").ToString();
                string smtpSsl = System.Configuration.ConfigurationManager.AppSettings.Get("SmtpSsl").ToString();
                string smtpFrom = System.Configuration.ConfigurationManager.AppSettings.Get("SmtpFrom").ToString();

                var objbody = entities.tblmultilanguages.Where(x => x.fldText == "MailNewRegistrationBody").SingleOrDefault();
                var objfooter = entities.tblmultilanguages.Where(x => x.fldText == "MsgMailFootNote").SingleOrDefault();
                var objSubject = entities.tblmultilanguages.Where(x => x.fldText == "MailNewRegistrationSubject").SingleOrDefault();

                if (objbody != null)
                {
                    string emailBody = "";
                    string emailFooter = "";
                    string emailSubject = "";
                    string strLang = "";
                    IEnumerable<string> headerLangValues;

                    if (Request.Headers.TryGetValues("X-Language", out headerLangValues))
                    {
                        strLang = headerLangValues.SingleOrDefault();
                    }

                    if (strLang == "")
                    {
                        strLang = member.fldLanguage;
                    }

                    if (strLang == "ZH-CN")
                    {
                        emailBody = objbody.fldZH_CN;
                    }
                    else
                    {
                        emailBody = objbody.fldEN_US;
                    }

                    if (objfooter != null)
                    {
                        if (strLang == "ZH-CN")
                        {
                            emailFooter = objfooter.fldZH_CN;
                        }
                        else
                        {
                            emailFooter = objfooter.fldEN_US;
                        }
                    }

                    if (objSubject != null)
                    {
                        if (strLang == "ZH-CN")
                        {
                            emailSubject = objSubject.fldZH_CN;
                        }
                        else
                        {
                            emailSubject = objSubject.fldEN_US;
                        }
                    }

                    
                    emailBody = emailBody.Replace("vNAME", member.fldName);
                    emailBody = emailBody.Replace("vCOMPANY", companyName);

                    emailBody = emailBody.Replace("vLOGINID", member.fldCode);
                    emailBody = emailBody.Replace("vPASSWORD", member.fldPassword);
                    emailBody = emailBody.Replace("vLINK", loginUrl);
                    emailBody = emailBody.Replace("vLOGINID", member.fldCode);


                    emailTemplate = emailTemplate.Replace("{{img}}", emailLogo);
                    emailTemplate = emailTemplate.Replace("{{body}}", emailBody);
                    emailTemplate = emailTemplate.Replace("{{footer}}", emailFooter);

                    FuncTool.SendEmail(smtpServer, int.Parse(smtpPort), smtpUser, smtpPassword, bool.Parse(smtpSsl), smtpFrom, emailSubject, member.fldEMail, emailTemplate);

                }
            }
        }
    }
}
