﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Web.Http;
using VmallDataAccess;
using VmallService.Base;
using VmallService.Base.Filters;
using VmallService.Controllers.Base;
using VmallService.Models;
using VmallService.RequestModel;

namespace VmallService.Controllers
{
    [BasicAuthentication]
    [RoutePrefix("api/PrivateMessage")]
    public class PrivateMessageController : BaseController
    {
        [Route("{id}", Name ="GetMessageDetail")]
        public IHttpActionResult Get(long id)
        {
            long userId = base.GetUserId();

            using (VmallDbEntities entities = new VmallDbEntities())
            {
                var query = entities.tblprivatemessages.SingleOrDefault(p => p.fldID == id);
                if(query != null && query.fldToMID == userId)
                {
                    query.fldToRead = true;
                    entities.SaveChanges();
                }
                    
                return Ok(query);
            }

        }

        
        [Route("inbox")]
        [HttpPost]
        public IHttpActionResult Inbox([FromBody] PrivateMessageSearchModel request)
        {
            long userId = GetUserId();
            using (VmallDbEntities entities = new VmallDbEntities())
            {

                var query = from pm in entities.tblprivatemessages
                            join a in entities.tbladmins on pm.fldFrMID equals a.fldID
                            where pm.fldToMID == userId
                            select new
                            {
                                pm.fldID,
                                pm.fldDateTime,
                                pm.fldFrMID,
                                pm.fldFrType,
                                pm.fldToMID,
                                pm.fldToType,
                                pm.fldSubject,
                                pm.fldContent,
                                pm.fldFrRead,
                                pm.fldToRead,
                                pm.fldFrDelete,
                                pm.fldToDelete,
                                pm.fldReplyID,
                                a.fldCode
                            };


                if (request.fromDate != null && request.toDate != null)
                {
                    DateTime dtFrom;
                    DateTime dtTo;
                    
                    if(DateTime.TryParse(request.fromDate, out dtFrom) && DateTime.TryParse(request.toDate, out dtTo))
                    {
                        dtTo = dtTo.AddHours(23).AddMinutes(59).AddSeconds(59);
                        query = query.Where(p => p.fldDateTime >= dtFrom && p.fldDateTime <= dtTo);
                    }
                }

                if(request.status != null)
                {
                    if(request.status == "0")
                        query = query.Where(p => p.fldToRead == true);
                    else if (request.status == "1")
                        query = query.Where(p => p.fldToRead == false);
                }

                if (!string.IsNullOrEmpty(request.subject))
                {
                    query = query.Where(p => p.fldSubject.Contains(request.subject));
                }

                query = query.OrderByDescending(p => p.fldDateTime);

                var result = CreatePageResult(request.pageSize, request.pageNumber, query);

                return Ok(result);
            }
        }

        [HttpPost]
        [Route("outbox")]
        public IHttpActionResult Outbox([FromBody] PrivateMessageSearchModel request)
        {
            long userId = GetUserId();
            using (VmallDbEntities entities = new VmallDbEntities())
            {

                var query = entities.tblprivatemessages.Where(p => p.fldFrMID == userId);

                if (request.fromDate != null && request.toDate != null)
                {
                    DateTime dtFrom;
                    DateTime dtTo;

                    if (DateTime.TryParse(request.fromDate, out dtFrom) && DateTime.TryParse(request.toDate, out dtTo))
                    {
                        dtTo = dtTo.AddHours(23).AddMinutes(59).AddSeconds(59);
                        query = query.Where(p => p.fldDateTime >= dtFrom && p.fldDateTime <= dtTo);
                    }
                }

                if (request.status != null)
                {
                    if (request.status == "0")
                        query = query.Where(p => p.fldToRead == true);
                    else if (request.status == "1")
                        query = query.Where(p => p.fldToRead == false);
                }

                if (!string.IsNullOrEmpty(request.subject))
                {
                    query = query.Where(p => p.fldSubject.Contains(request.subject));
                }

                query = query.OrderByDescending(p => p.fldDateTime);

                var result = CreatePageResult<VmallDataAccess.tblprivatemessage>(request.pageSize, request.pageNumber, query);

                return Ok(result);
            }

        }

        [HttpPost]
        [Route("createMessage")]
        public IHttpActionResult CreateMessage([FromBody] tblprivatemessage message)
        {
            long userId = base.GetUserId();
            //if (!ModelState.IsValid)
            //{
            //    return BadRequest(ModelState);
            //}

            tblprivatemessage entity = new tblprivatemessage();
            entity.fldDateTime = DateTime.Now;
            entity.fldFrMID = userId;
            entity.fldFrType = "M";
            entity.fldToMID = 0;
            entity.fldToType = "A";
            entity.fldSubject = message.fldSubject;
            entity.fldContent = message.fldContent;
            entity.fldFrRead = true;
            entity.fldToRead = false;
            entity.fldFrDelete = false;
            entity.fldFrDelete = false;
            entity.fldToDelete = false;
            if (message.fldReplyID != null && message.fldReplyID > 0)
                entity.fldReplyID = message.fldReplyID;
            else
                entity.fldReplyID = 0;

            using (VmallDbEntities entities = new VmallDbEntities())
            {
                entities.tblprivatemessages.Add(entity);
                entities.SaveChanges();
                return CreatedAtRoute("GetMessageDetail", new { id = entity.fldID }, entity);
            }

            
        }

        [Route("admin/inbox")]
        [HttpPost]
        public IHttpActionResult AdminInbox([FromBody] PrivateMessageSearchModel request)
        {
            long userId = GetUserId();
            using (VmallDbEntities entities = new VmallDbEntities())
            {
                long toUserId = userId;
                long fromUserId = 0;

                if (isAdmin)
                    toUserId = 0;
                else
                    return BadRequest();    

                if (request.fromMemberCode != null && request.fromMemberCode != "")
                {
                    var user = entities.tblmemberships.Where(p => p.fldCode == request.fromMemberCode).SingleOrDefault();
                    if (user != null)
                        fromUserId = user.fldID;
                    else
                        fromUserId = -1;
                }

                var query = from pm in entities.tblprivatemessages
                            where pm.fldToMID == toUserId
                            select new
                            {
                                pm.fldID,
                                pm.fldDateTime,
                                pm.fldFrMID,
                                pm.fldFrType,
                                pm.fldToMID,
                                pm.fldToType,
                                pm.fldSubject,
                                pm.fldContent,
                                pm.fldFrRead,
                                pm.fldToRead,
                                pm.fldFrDelete,
                                pm.fldToDelete,
                                pm.fldReplyID
                            };

                if (fromUserId != 0)
                    query = query.Where(x => x.fldFrMID == fromUserId);

                if (request.fromDate != null && request.toDate != null)
                {
                    DateTime dtFrom;
                    DateTime dtTo;

                    if (DateTime.TryParse(request.fromDate, out dtFrom) && DateTime.TryParse(request.toDate, out dtTo))
                    {
                        dtTo = dtTo.AddHours(23).AddMinutes(59).AddSeconds(59);
                        query = query.Where(p => p.fldDateTime >= dtFrom && p.fldDateTime <= dtTo);
                    }
                }

                if (request.status != null)
                {
                    if (request.status == "0")
                        query = query.Where(p => p.fldToRead == true);
                    else if (request.status == "1")
                        query = query.Where(p => p.fldToRead == false);
                }

                if (!string.IsNullOrEmpty(request.subject))
                {
                    query = query.Where(p => p.fldSubject.Contains(request.subject));
                }

                query = query.OrderByDescending(p => p.fldDateTime);

                var result = CreatePageResult(request.pageSize, request.pageNumber, query);

                return Ok(result);
            }
        }

        [HttpPost]
        [Route("admin/createMessage")]
        public IHttpActionResult AdminCreateMessage([FromBody] tblprivatemessage message)
        {
            long userId = base.GetUserId();
            long toUserId;

            if (!isAdmin)
                return BadRequest();

            if(message.toMember == null)
            {
                ModelState.AddModelError("", "Invalid Member ID");
                return BadRequest(ModelState);
            }
            else
            {
                using (VmallDbEntities entities = new VmallDbEntities())
                {
                    var user = entities.tblmemberships.SingleOrDefault(x => x.fldCode.Equals(message.toMember, StringComparison.OrdinalIgnoreCase));
                    if(user == null)
                    {
                        ModelState.AddModelError("", "Invalid Member ID");
                        return BadRequest(ModelState);
                    }
                    else
                    {
                        toUserId = user.fldID;
                    }

                }
            }

            tblprivatemessage entity = new tblprivatemessage();
            entity.fldDateTime = DateTime.Now;
            entity.fldFrMID = userId;
            entity.fldFrType = "A";
            entity.fldToMID = toUserId;
            entity.fldToType = "M";
            entity.fldSubject = message.fldSubject;
            entity.fldContent = message.fldContent;
            entity.fldFrRead = true;
            entity.fldToRead = false;
            entity.fldFrDelete = false;
            entity.fldFrDelete = false;
            entity.fldToDelete = false;
            if (message.fldReplyID != null && message.fldReplyID > 0)
                entity.fldReplyID = message.fldReplyID;
            else
                entity.fldReplyID = 0;

            using (VmallDbEntities entities = new VmallDbEntities())
            {
                entities.tblprivatemessages.Add(entity);
                entities.SaveChanges();
                return CreatedAtRoute("GetMessageDetail", new { id = entity.fldID }, entity);
            }


        }

        [Route("admin/history")]
        [HttpPost]
        public IHttpActionResult AdminMessageHistory([FromBody] PrivateMessageSearchModel request)
        {
            long userId = GetUserId();

            if (!isAdmin)
                return BadRequest();


            using (VmallDbEntities entities = new VmallDbEntities())
            {

                var query = from pm in entities.tblprivatemessages
                            from a in entities.tbladmins.Where(x=>x.fldID == pm.fldFrMID).DefaultIfEmpty()
                            from m in entities.tblmemberships.Where(x => x.fldID == pm.fldToMID)
                            where pm.fldFrType == "A"
                            select new
                            {
                                pm.fldID,
                                pm.fldDateTime,
                                pm.fldFrMID,
                                pm.fldFrType,
                                pm.fldToMID,
                                pm.fldToType,
                                pm.fldSubject,
                                pm.fldContent,
                                pm.fldFrRead,
                                pm.fldToRead,
                                pm.fldFrDelete,
                                pm.fldToDelete,
                                pm.fldReplyID,
                                fromAdmin = a.fldCode,
                                toMember = m.fldMemberCode
                            };


                if (request.fromDate != null && request.toDate != null)
                {
                    DateTime dtFrom;
                    DateTime dtTo;
                    
                    if(DateTime.TryParse(request.fromDate, out dtFrom) && DateTime.TryParse(request.toDate, out dtTo))
                    {
                        dtTo = dtTo.AddHours(23).AddMinutes(59).AddSeconds(59);
                        query = query.Where(p => p.fldDateTime >= dtFrom && p.fldDateTime <= dtTo);
                    }
                }

                
                if(!string.IsNullOrEmpty(request.fromMemberCode))
                {
                    var admin = entities.tbladmins.SingleOrDefault(x => x.fldCode == request.fromMemberCode);
                    if (admin != null)
                        query = query.Where(p => p.fldFrMID == admin.fldID);
                }

                if (!string.IsNullOrEmpty(request.toMemberCode))
                {
                    var user = entities.tblmemberships.SingleOrDefault(x => x.fldCode == request.toMemberCode);
                    if (user != null)
                        query = query.Where(p => p.fldToMID == user.fldID);
                }

                if (!string.IsNullOrEmpty(request.subject))
                {
                    query = query.Where(p => p.fldSubject.Contains(request.subject));
                }

                query = query.OrderByDescending(p => p.fldDateTime);

                var result = CreatePageResult(request.pageSize, request.pageNumber, query);

                return Ok(result);
            }
        }

    }
}
