﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using VmallDataAccess;
using VmallService.Controllers.Base;
using VmallService.ResponseModel;

namespace VmallService.Controllers
{
    [RoutePrefix("api/parameter")]
    public class ParameterController : BaseController
    {
        [HttpGet]
        [Route("")]
        public IHttpActionResult GetParameter()
        {
            using (VmallDbEntities entities = new VmallDbEntities())
            {
                var bank = entities.tblbanks.Where(x => x.fldStatus == "Y").ToArray();
                var country = entities.tblcountries.Where(x => x.fldStatus == "Y").ToArray();
                var setting = entities.tblsettings.ToArray();
                var package = entities.tblpackages.ToArray();
                var trxType = entities.tbltransactions.Select(x => x.fldType).Distinct().ToArray();
                var serviceCenterType = entities.tblservicecentertypes.ToArray();
                ParameterModel param = new ParameterModel();
                param.Bank = bank;
                param.Country = country;
                param.Setting = setting;
                param.Package = package;
                param.transactionType = trxType;
                param.serviceCenterType = serviceCenterType;
                
                //return Request.CreateResponse(HttpStatusCode.OK, param);
                return Ok(param);
            }
        }
    }
}
