﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using VmallDataAccess;
using VmallService.Base.Filters;
using VmallService.Controllers.Base;

namespace VmallService.Controllers
{
    [BasicAuthentication]
    [RoutePrefix("api/MultiLanguage")]
    public class MultiLanguageController : BaseController
    {
        [HttpGet]
        [Route("")]
        public IHttpActionResult Get()
        {
            using (VmallDbEntities entities = new VmallDbEntities())
            {
                var query = entities.tblmultilanguages
                                    .Select(x => new
                                    {
                                        fldEN_US = x.fldEN_US,
                                        fldZH_CN = x.fldZH_CN
                                    })
                                    .ToList();
                return Ok(query);
            }
        }

        [HttpGet]
        [Route("search/{text=text}")]
        public IHttpActionResult Search(string text)
        {
            using (VmallDbEntities entities = new VmallDbEntities())
            {
                var query = entities.tblmultilanguages
                                    .Where(p => p.fldEN_US.Equals(text, StringComparison.OrdinalIgnoreCase))
                                    .ToList();


                dynamic lang = null;

                if (query != null && query.Count > 0)
                {
                    lang = query.Select(x => new
                    {
                        fldEN_US = x.fldEN_US,
                        fldZH_CN = (x.fldZH_CN.Equals("") || x.fldZH_CN == null) ? x.fldEN_US : x.fldZH_CN
                    })
                    .SingleOrDefault();
                }
                else
                {
                    tblmultilanguage objLang = new tblmultilanguage();
                    objLang.fldDefault = text;
                    objLang.fldEN_US = text;
                    objLang.fldText = text;
                    objLang.fldZH_CN = "";

                    entities.tblmultilanguages.Add(objLang);
                    entities.SaveChanges();

                    lang = new
                    {
                        fldEN_US = text,
                        fldZH_CN = text
                    };

                }



                return Ok(lang);
            }

        }

        [HttpGet]
        [Route("en")]
        public IHttpActionResult GetEnDictionary()
        {
            using (VmallDbEntities entities = new VmallDbEntities())
            {
                var query = from lang in entities.tblmultilanguages
                            orderby lang.fldDefault
                            select new
                            {
                                lang.fldDefault,
                                lang.fldEN_US
                            };

                Dictionary<string, string> dict = new Dictionary<string, string>();
                foreach (var item in query)
                {
                    if(!dict.ContainsKey(item.fldDefault))
                    {
                        dict.Add(item.fldDefault, item.fldEN_US);
                    }
                }

                return Ok(dict.ToDictionary(x=>x.Key, x=>x.Value));
            }
        }

        [HttpGet]
        [Route("zh")]
        public IHttpActionResult GetZhDictionary()
        {
            using (VmallDbEntities entities = new VmallDbEntities())
            {
                var query = from lang in entities.tblmultilanguages
                            orderby lang.fldDefault
                            select new
                            {
                                lang.fldDefault,
                                lang.fldZH_CN
                            };

                Dictionary<string, string> dict = new Dictionary<string, string>();
                foreach (var item in query)
                {
                    if (!dict.ContainsKey(item.fldDefault))
                    {
                        dict.Add(item.fldDefault, item.fldZH_CN);
                    }
                }

                return Ok(dict.ToDictionary(x => x.Key, x => x.Value));
            }
        }

    }
}
