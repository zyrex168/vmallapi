﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Web.Http;
using VmallDataAccess;
using VmallService.Base;
using VmallService.Base.Filters;
using VmallService.Controllers.Base;

namespace VmallService.Controllers
{
    [BasicAuthentication]
    [RoutePrefix("api/sharerate")]
    public class ShareRateController : BaseController
    {
        [HttpGet]
        [Route("")]
        public IHttpActionResult GetChart()
        {
            long userId = GetUserId();

            using (VcoinEntities entities = new VcoinEntities())
            {
                List<object[]> lst = new List<object[]>();


                var query = entities.tblsharerates.OrderBy(p => p.fldDateTime);

                foreach (var item in query.ToList())
                {
                    
                    if(item.fldDateTime != null)
                    {
                        DateTime dt = (DateTime)item.fldDateTime;
                        //dt = new DateTime(dt.Year, dt.Month, dt.Day);

                        object[] data = new object[2];
                        data[0] = ((DateTimeOffset) dt).ToUnixTimeMilliseconds(); //Convert.ToInt64(dt.Ticks.ToString().Substring(0,13));
                        data[1] = item.fldRate;
                        lst.Add(data);
                    }
                }

                return Ok(lst);
            }
        }
    }
}
