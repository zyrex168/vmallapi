﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Web.Http;
using VmallDataAccess;
using VmallService.Base.Filters;
using VmallService.Controllers.Base;
using VmallService.RequestModel;

namespace VmallService.Controllers
{
    [BasicAuthentication]
    [RoutePrefix("api/SalesHistory")]
    public class SalesHistoryController : BaseController
    {
        [HttpPost]
        [Route("search")]
        public IHttpActionResult Search([FromBody] SalesHistorySearchModel request)
        {
            long userId = GetUserId();
            using (VmallDbEntities entities = new VmallDbEntities())
            {
                DateTime dtFrom = DateTime.Now;
                DateTime dtTo = DateTime.Now;

                //if (request.memberCode != null && request.memberCode != "")
                //{
                //    var user = entities.tblmemberships.Where(p => p.fldCode == request.memberCode).SingleOrDefault();
                //    userId = user.fldID;
                //}

                if (request.fromDate != null && request.toDate != null)
                {

                    if (DateTime.TryParse(request.fromDate, out dtFrom) && DateTime.TryParse(request.toDate, out dtTo))
                    {
                        dtTo = dtTo.AddHours(23).AddMinutes(59).AddSeconds(59);
                    }
                }

                var query = from trx in entities.tbltransactions
                            where trx.fldMID == userId &&
                                  trx.fldDateTime >= dtFrom && trx.fldDateTime <= dtTo
                            select trx;

                if(request.package != null && request.package != "")
                {
                    long packageId = long.Parse(request.package);
                    query = query.Where(x => x.fldPackageID == packageId);
                }

                if (request.payType != null && request.payType != "")
                {
                    query = query.Where(x => x.fldPaymentType == request.payType);
                }

                if (request.tranType != null && request.tranType != "")
                {
                    query = query.Where(x => x.fldType == request.tranType);
                }

                query = query.OrderByDescending(p => p.fldDateTime);

                var result = CreatePageResult<VmallDataAccess.tbltransaction>(request.pageSize, request.pageNumber, query);

                return Ok(result);
            }

        }

        [HttpPost]
        [Route("admin/search")]
        public IHttpActionResult AdminSearch([FromBody] SalesHistorySearchModel request)
        {
            long userId = base.GetUserId();

            if (!isAdmin)
                return BadRequest();

            using (VmallDbEntities entities = new VmallDbEntities())
            {
                DateTime dtFrom = DateTime.Now;
                DateTime dtTo = DateTime.Now;

                

                if (request.fromDate != null && request.toDate != null)
                {

                    if (DateTime.TryParse(request.fromDate, out dtFrom) && DateTime.TryParse(request.toDate, out dtTo))
                    {
                        dtTo = dtTo.AddHours(23).AddMinutes(59).AddSeconds(59);
                    }
                }

                var query = from trx in entities.tbltransactions
                            from member in entities.tblmemberships.Where(x=>x.fldID == trx.fldMID)
                            from creator in entities.tblmemberships.Where(x => x.fldID == trx.fldCreator)
                            where trx.fldCreatorType == "M" &&
                                  trx.fldDateTime >= dtFrom && trx.fldDateTime <= dtTo
                            select new
                            {
                                trx.fldDateTime,
                                trx.fldReferenceID,
                                memberId = member.fldCode,
                                trx.fldType,
                                trx.fldPackageID,
                                trx.fldAmount,
                                trx.fldPaymentType,
                                creator = creator.fldCode,
                                trx.fldRemark,
                                trx.fldMID
                            };

                if (request.memberCode != null && request.memberCode != "")
                {
                    var user = entities.tblmemberships.Where(p => p.fldCode == request.memberCode).SingleOrDefault();
                    query = query.Where(x => x.fldMID == user.fldID);
                }

                if (request.package != null && request.package != "")
                {
                    long packageId = long.Parse(request.package);
                    query = query.Where(x => x.fldPackageID == packageId);
                }

                if (request.payType != null && request.payType != "")
                {
                    query = query.Where(x => x.fldPaymentType == request.payType);
                }

                if (request.tranType != null && request.tranType != "")
                {
                    query = query.Where(x => x.fldType == request.tranType);
                }

                query = query.OrderByDescending(p => p.fldDateTime);

                var result = CreatePageResult(request.pageSize, request.pageNumber, query);

                return Ok(result);
            }

        }
    }
}
