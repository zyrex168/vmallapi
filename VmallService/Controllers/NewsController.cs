﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Web.Http;
using VmallDataAccess;
using VmallService.Base;
using VmallService.Base.Filters;
using VmallService.Controllers.Base;
using VmallService.RequestModel;
using VmallService.ResponseModel;

namespace VmallService.Controllers
{
    [BasicAuthentication]
    [RoutePrefix("api/news")]
    public class NewsController : BaseController
    {
        [HttpGet]
        [Route("{id}", Name = "GetNews")]
        public IHttpActionResult GetNews(long id)
        {
            long userId = GetUserId();
            using (VmallDbEntities entities = new VmallDbEntities())
            {

                var query = entities.tblnews.SingleOrDefault(x => x.fldID == id);
                if (query == null)
                    return NotFound();
                

                return Ok(query);
            }
        }


        [HttpGet]
        [Route("{pageNumber=pageNumber}/{pageSize=pageSize}")]
        public IHttpActionResult GetNewsList(int pageNumber, int pageSize)
        {
            long userId = GetUserId();
            using (VmallDbEntities entities = new VmallDbEntities())
            {
                
                var query = entities.tblnews.Where(p => p.fldStatus == "Y")
                                            .OrderByDescending(p => p.fldDateTime);
                
                var result = CreatePageResult<VmallDataAccess.tblnew>(pageSize, pageNumber, query);

                return Ok(result);
            }
        }

        [HttpPost]
        [Route("admin/create")]
        public IHttpActionResult AdminCreateNews([FromBody] tblnew request)
        {
            long userId = base.GetUserId();

            if (!isAdmin)
                return BadRequest();

            using (VmallDbEntities entities = new VmallDbEntities())
            {
                tblnew news = new tblnew();
                news.fldSubject = request.fldSubject;
                news.fldContent = request.fldContent;
                news.fldCreatorID = (int)userId;
                news.fldCreatorName = base.userName;
                news.fldStatus = request.fldStatus;
                news.fldDateTime = DateTime.Now;

                entities.tblnews.Add(news);
                entities.SaveChanges();
                return CreatedAtRoute("GetNews", new { id = news.fldID }, news);
            }
        }

        [HttpPut]
        [Route("admin/edit")]
        public IHttpActionResult AdminEditNews([FromBody] tblnew request)
        {
            long userId = base.GetUserId();

            if (!isAdmin)
                return BadRequest();

            using (VmallDbEntities entities = new VmallDbEntities())
            {
                var news = entities.tblnews.SingleOrDefault(x => x.fldID == request.fldID);
                if (news == null)
                    return NotFound();

                news.fldSubject = request.fldSubject;
                news.fldContent = request.fldContent;
                news.fldCreatorID = (int)userId;
                news.fldCreatorName = base.userName;
                news.fldStatus = request.fldStatus;
                news.fldDateTime = DateTime.Now;

                entities.SaveChanges();

                return Ok();
            }
        }

        [HttpDelete]
        [Route("admin/delete/{id}")]
        public IHttpActionResult AdminEditDelete(long id)
        {
            long userId = base.GetUserId();

            if (!isAdmin)
                return BadRequest();

            using (VmallDbEntities entities = new VmallDbEntities())
            {
                var news = entities.tblnews.SingleOrDefault(x => x.fldID == id);
                if (news == null)
                    return NotFound();
                news.fldStatus = "D";
                entities.SaveChanges();

                return Ok();
            }
        }

        [Route("admin/search")]
        [HttpPost]
        public IHttpActionResult AdminMessageHistory([FromBody] NewsSearchModel request)
        {
            long userId = GetUserId();

            if (!isAdmin)
                return BadRequest();


            using (VmallDbEntities entities = new VmallDbEntities())
            {

                var query = from news in entities.tblnews
                            where news.fldStatus != "D"
                            select news;


                if (request.fromDate != null && request.toDate != null)
                {
                    DateTime dtFrom;
                    DateTime dtTo;

                    if (DateTime.TryParse(request.fromDate, out dtFrom) && DateTime.TryParse(request.toDate, out dtTo))
                    {
                        dtTo = dtTo.AddHours(23).AddMinutes(59).AddSeconds(59);
                        query = query.Where(p => p.fldDateTime >= dtFrom && p.fldDateTime <= dtTo);
                    }
                }


                if (!string.IsNullOrEmpty(request.creatorName))
                {
                    var admin = entities.tbladmins.SingleOrDefault(x => x.fldCode == request.creatorName);
                    if (admin != null)
                        query = query.Where(p => p.fldCreatorID == admin.fldID);
                }

                if (!string.IsNullOrEmpty(request.status))
                {
                    query = query.Where(p => p.fldStatus == request.status);
                }

                if (!String.IsNullOrEmpty(request.subject))
                {
                    query = query.Where(p => p.fldSubject.Contains(request.subject));
                }

                query = query.OrderByDescending(p => p.fldDateTime);

                var result = CreatePageResult(request.pageSize, request.pageNumber, query);

                return Ok(result);
            }
        }
    }
}
