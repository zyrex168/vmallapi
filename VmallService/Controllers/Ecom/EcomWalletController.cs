﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using VmallDataAccess;
using VmallService.Base.Filters;
using VmallService.Controllers.Base;
using VmallService.RequestModel.Ecom;

namespace VmallService.Controllers.Ecom
{
    [BasicAuthentication]
    [RoutePrefix("api/Ecom")]
    public class EcomWalletController : BaseController
    {
        [HttpGet]
        [Route("walletSummary")]
        public IHttpActionResult WalletSummary()
        {
            long userId = GetUserId();

            using (VmallDbEntities entities = new VmallDbEntities())
            {
                var user = from m in entities.tblmemberships
                           where m.fldID == userId
                           select new
                           {
                               wWallet = m.fldWWallet
                           };

                return Ok(user.SingleOrDefault());

            }
        }

        [Route("redemption")]
        [HttpPost]
        public IHttpActionResult Redemption([FromBody] WalletRedemptionModel request)
        {
            long userId = GetUserId();
            using (VmallDbEntities entities = new VmallDbEntities())
            {
                var user = entities.tblmemberships.SingleOrDefault(x => x.fldID == userId);
                if(user.fldWWallet < request.walletAmt)
                {
                    ModelState.AddModelError("", "Insufficient wallet balance");
                    return BadRequest(ModelState);
                }

                decimal dmUserBf = 0;
                decimal dmUserNewBal;
                string strRef = "";

                dmUserBf = (decimal)user.fldWWallet;
                dmUserNewBal = (decimal)user.fldWWallet - request.walletAmt;
                strRef = request.referenceId;

                //update balance
                user.fldWWallet = dmUserNewBal;
                
                // trf out from user
                tblwwallettransaction walletTrx = new tblwwallettransaction();
                walletTrx.fldMID = userId;
                walletTrx.fldType = "REDEEM";
                walletTrx.fldReferenceID = strRef;
                walletTrx.fldCreator = userId;
                walletTrx.fldDateTime = DateTime.Now;
                walletTrx.fldCreatorType = "M";
                walletTrx.fldBF = dmUserBf;
                walletTrx.fldIn = 0;
                walletTrx.fldOut = (decimal)request.walletAmt;
                walletTrx.fldCF = dmUserNewBal;
                walletTrx.fldRemark = "Redeem " + string.Format("{0:N}", request.walletAmt) + " for Ecom. Ref " + strRef;
                entities.tblwwallettransactions.Add(walletTrx);

                entities.SaveChanges();
                return Ok();
            }
        }
    }
}