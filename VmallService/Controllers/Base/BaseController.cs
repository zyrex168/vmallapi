﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Headers;
using System.Threading;
using System.Web;
using System.Web.Http;
using VmallDataAccess;
using VmallService.Base;
using VmallService.ResponseModel;

namespace VmallService.Controllers.Base
{
    public class BaseController: ApiController
    {
        public string userLanguage { get; set; }
        public bool isAdmin = false;
        public string userName = "";
        public string userMemberCode = "";

        public BaseController()
        {
            //userName = Thread.CurrentPrincipal.Identity.Name;
        }

        public long GetUserId()
        {
            //long userId = long.Parse(Thread.CurrentPrincipal.Identity.Name);
            string userId = Thread.CurrentPrincipal.Identity.Name;
            long id = 0;
            using (VmallDbEntities entities = new VmallDbEntities())
            {

                var member = entities.tblmemberships.SingleOrDefault(x => x.fldCode == userId);

                if (GetIsAdmin(base.Request.Headers))
                {
                    var admin = entities.tbladmins.SingleOrDefault(x => x.fldCode == userId);
                    if (admin != null)
                    {
                        id = admin.fldID;
                        isAdmin = true;
                        userName = admin.fldName;
                    }
                }
                else
                {
                    userName = member.fldName;
                    userMemberCode = member.fldCode;
                    id = member.fldID;

                }
            }

            return id;
        }

        public string GetLanguage(HttpHeaders header)
        {
            IEnumerable<string> result;
            if (header.TryGetValues("X-Language" , out result))
            {
                return result.ToList()[0];
            }
            else
            {
                return "EN_US";
            }
            
        }

        public PagedResults<dynamic> CreatePageResult<dynamic>(int pageSize, int pageNumber, IQueryable<dynamic> queryable)
        {
            if (pageSize == 0)
                pageSize = SystemConfig.pageSize;

            if (pageNumber == 0)
                pageNumber = 1;

            var totalNumberOfRecords = queryable.Count();
            var skipAmount = pageSize * (pageNumber - 1);
            var mod = totalNumberOfRecords % pageSize;
            var totalPageCount = (totalNumberOfRecords / pageSize) + (mod == 0 ? 0 : 1);
            

            var queryPage = queryable.Skip((int)(skipAmount))
                                 .Take(pageSize).ToList();

            var nextPageUrl = "";

            if (pageNumber == totalPageCount)
            {
                nextPageUrl = "";
            }
            else
            {
                nextPageUrl = Url?.Link("DefaultApi", new
                {
                    pageNumber = pageNumber + 1,
                    pageSize
                });
            }


            var result = new PagedResults<dynamic>
            {
                Results = queryPage,
                PageNumber = pageNumber,
                PageSize = pageSize,
                TotalNumberOfRecords = totalNumberOfRecords,
                TotalNumberOfPages = totalPageCount,
                NextPageUrl = nextPageUrl

            };

            return result;
        }

        public static bool GetIsAdmin(HttpHeaders header)
        {
            bool blnFlag = false;
            IEnumerable<string> result;
            if (header.TryGetValues("X-Admin", out result))
            {
                if (result.ToList().Count > 0 && result.ToList()[0] == "1")
                    blnFlag = true;
            }

            return blnFlag;
        }
    }
}