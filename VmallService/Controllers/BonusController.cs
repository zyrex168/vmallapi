﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Web.Http;
using VmallDataAccess;
using VmallService.Base;
using VmallService.Base.Filters;
using VmallService.Controllers.Base;
using VmallService.RequestModel;

namespace VmallService.Controllers
{
    [BasicAuthentication]
    public class BonusController : BaseController
    {
        
        [Route("api/bonusincome")]
        public IHttpActionResult Get()
        {
            long userId = GetUserId();
            using (VmallDbEntities entities = new VmallDbEntities())
            {

                var query = from b in entities.tblbonus
                            where b.fldMID == userId
                            group b by b.fldMID into g
                            select new
                            {
                                fldMatching = g.Where(p=>p.fldType == 1).Sum(s=>s.fldBonus) ?? 0,
                                fldPairing = g.Where(p => p.fldType == 2).Sum(s => s.fldBonus) ?? 0,
                                fldLeadership = g.Where(p => p.fldType == 3).Sum(s => s.fldBonus) ?? 0,
                                fldUnilevel = g.Where(p => p.fldType == 5).Sum(s => s.fldBonus) ?? 0,
                                fldSponsorReward = g.Where(p => p.fldType == 6).Sum(s => s.fldBonus) ?? 0
                            };

                return Ok(query.ToList());
            }
        }

        [HttpPost]
        [Route("api/BonusSearch")]
        public IHttpActionResult BonusSearch([FromBody] BonusSearchModel request)
        {
            long userId = GetUserId();

            

            using (VmallDbEntities entities = new VmallDbEntities())
            {
                if (isAdmin)
                {
                    if(string.IsNullOrEmpty(request.memberId))
                    {
                        ModelState.AddModelError("", "*Member ID cannot be blank");
                        return BadRequest(ModelState);
                    }

                    var member = entities.tblmemberships.SingleOrDefault(x => x.fldCode == request.memberId);

                    if(member == null)
                    {
                        ModelState.AddModelError("", "*Member ID not found");
                        return BadRequest(ModelState);
                    }

                    userId = member.fldID;
                }


                var user = entities.tblmemberships.SingleOrDefault(x => x.fldID == userId);

                if(request.bonusType > 0)
                {
                    var query = from bonus in entities.tblbonus
                                from member in entities.tblmemberships.Where(x => x.fldID == userId)
                                from fromMember in entities.tblmemberships.Where(x => x.fldID == bonus.fldFMID).DefaultIfEmpty()
                                where bonus.fldMID == userId && bonus.fldType == request.bonusType
                                select new
                                {
                                    fldID = bonus.fldID,
                                    fldDate = bonus.fldDate,
                                    fldMID = bonus.fldMID,
                                    fldAc = bonus.fldAc ?? 0,
                                    fldFMID = bonus.fldFMID,
                                    fldFAc = bonus.fldFAc ?? 0,
                                    fldTransID = bonus.fldTransID,
                                    fldReferenceID = bonus.fldReferenceID ?? string.Empty,
                                    fldType = bonus.fldType,
                                    fldLevel = bonus.fldLevel,
                                    fldPV = bonus.fldPV,
                                    fldPercent = bonus.fldPercent,
                                    fldBonus = bonus.fldBonus,
                                    fldProcess = bonus.fldProcess,
                                    fldCode = member.fldCode ?? string.Empty,
                                    fldName = member.fldName ?? string.Empty,
                                    fldFrCode = fromMember.fldCode ?? string.Empty,
                                    fldFrName = fromMember.fldName ?? string.Empty
                                };



                    if (request.fromDate != null && request.toDate != null)
                    {
                        DateTime dtFrom;
                        DateTime dtTo;

                        if (DateTime.TryParse(request.fromDate, out dtFrom) && DateTime.TryParse(request.toDate, out dtTo))
                        {
                            dtTo = dtTo.AddHours(23).AddMinutes(59).AddSeconds(59);
                            query = query.Where(p => p.fldDate >= dtFrom && p.fldDate <= dtTo);
                        }
                    }

                    query = query.OrderByDescending(p => p.fldDate).ThenByDescending(p=>p.fldID);
                    var result = CreatePageResult(request.pageSize, request.pageNumber, query);
                    return Ok(result);
                }
                else
                {
                    DateTime dtFrom = DateTime.Now;
                    DateTime dtTo = DateTime.Now;

                    if (DateTime.TryParse(request.fromDate, out dtFrom) && DateTime.TryParse(request.toDate, out dtTo))
                    {
                        dtTo = dtTo.AddHours(23).AddMinutes(59).AddSeconds(59);
                    }

                    var matchingBonus = from bonus in entities.tblbonus
                             where bonus.fldType == 1 && bonus.fldMID == userId && bonus.fldDate >= dtFrom && bonus.fldDate <= dtTo
                             group bonus by new { bonus.fldMID, bonus.fldDate } into g1
                             select new
                             {
                                 fldDate = g1.Key.fldDate,
                                 fldMID = g1.Key.fldMID,
                                 fldMatchingBonus = g1.Sum(p => p.fldBonus)
                             };

                    var pairingBonus = from bonus in entities.tblbonus
                                        where bonus.fldType == 2 && bonus.fldMID == userId && bonus.fldDate >= dtFrom && bonus.fldDate <= dtTo
                                        group bonus by new { bonus.fldMID, bonus.fldDate } into g1
                                        select new
                                        {
                                            fldDate = g1.Key.fldDate,
                                            fldMID = g1.Key.fldMID,
                                            fldPairingBonus = g1.Sum(p => p.fldBonus)
                                        };

                    var leadershipBonus = from bonus in entities.tblbonus
                                       where bonus.fldType == 3 && bonus.fldMID == userId && bonus.fldDate >= dtFrom && bonus.fldDate <= dtTo
                                       group bonus by new { bonus.fldMID, bonus.fldDate } into g1
                                       select new
                                       {
                                           fldDate = g1.Key.fldDate,
                                           fldMID = g1.Key.fldMID,
                                           fldLeadershipBonus = g1.Sum(p => p.fldBonus)
                                       };

                    var unilevelBonus = from bonus in entities.tblbonus
                                          where bonus.fldType == 5 && bonus.fldMID == userId && bonus.fldDate >= dtFrom && bonus.fldDate <= dtTo
                                          group bonus by new { bonus.fldMID, bonus.fldDate } into g1
                                          select new
                                          {
                                              fldDate = g1.Key.fldDate,
                                              fldMID = g1.Key.fldMID,
                                              fldUnilevelBonus = g1.Sum(p => p.fldBonus)
                                          };

                    var query = from bonus in entities.tblbonus.GroupBy(x=> new {x.fldMID, x.fldDate }).Select(g=> new { g.Key.fldMID, g.Key.fldDate})
                                from member in entities.tblmemberships.Where(x => x.fldID == bonus.fldMID).DefaultIfEmpty()
                                from match in matchingBonus.Where(x => x.fldMID == bonus.fldMID && x.fldDate == bonus.fldDate).DefaultIfEmpty()
                                from pair in pairingBonus.Where(x => x.fldMID == bonus.fldMID && x.fldDate == bonus.fldDate).DefaultIfEmpty()
                                from leader in leadershipBonus.Where(x => x.fldMID == bonus.fldMID && x.fldDate == bonus.fldDate).DefaultIfEmpty()
                                from unilevel in unilevelBonus.Where(x => x.fldMID == bonus.fldMID && x.fldDate == bonus.fldDate).DefaultIfEmpty()
                                where bonus.fldMID == userId && bonus.fldDate >= dtFrom && bonus.fldDate <= dtTo
                                select new
                                {
                                    bonus.fldMID,
                                    bonus.fldDate,
                                    member.fldCode,
                                    member.fldName,
                                    fldMatchingBonus = match.fldMatchingBonus ?? 0,
                                    fldPairingBonus = pair.fldPairingBonus ?? 0,
                                    fldLeadershipBonus = leader.fldLeadershipBonus ?? 0,
                                    fldUnilevelBonus = unilevel.fldUnilevelBonus ?? 0,
                                    fldSubTotal = (match.fldMatchingBonus ?? 0) + (pair.fldPairingBonus ?? 0) 
                                                  + (leader.fldLeadershipBonus ?? 0) + (unilevel.fldUnilevelBonus ?? 0)
                                };
                    query = query.OrderByDescending(p => p.fldDate);
                    var result = CreatePageResult(request.pageSize, request.pageNumber, query);
                    return Ok(result);
                }

                
            }
        }


    }
}
