﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Web.Http;
using VmallDataAccess;
using VmallService.Base;
using VmallService.Base.Filters;
using VmallService.CommonFunction;
using VmallService.Controllers.Base;
using VmallService.RequestModel;
using VmallService.ResponseModel;

namespace VmallService.Controllers
{
    [BasicAuthentication]
    [RoutePrefix("api/Wallet")]
    public class WalletController : BaseController
    {
        [Route("transfer")]
        [HttpPost]
        public IHttpActionResult Transfer([FromBody] WalletTransferModel request)
        {
            long userId = GetUserId();
            string strRefPrefix = "TF";

            using (VmallDbEntities entities = new VmallDbEntities())
            {
                //get setting
                
                if (!ValidateTransfer(request))
                {
                    return BadRequest(ModelState);
                }

                
                var user = entities.tblmemberships.SingleOrDefault(x => x.fldID == userId);
                var member = entities.tblmemberships.SingleOrDefault(x => x.fldCode == request.toMemberId);

                decimal dmUserBf = 0; // (decimal)user.fldCWallet;
                decimal dmMemberBf = 0; // (decimal)member.fldCWallet;

                decimal dmUserNewBal;
                decimal dmMemberNewBal;
                string strRef = "";

                
                decimal dmFee = 0;
                var fee = entities.tblsettings.SingleOrDefault(x => x.fldSetting == "WalletTransferFee"); //WalletTransferFee


                if (fee != null)
                {
                    dmFee = decimal.Parse(fee.fldValue);
                }

               
                #region CP(Cash Point)
                if (request.transferType == "C") //CP(Cash Point)
                {
                    dmUserBf = (decimal)user.fldCWallet;
                    dmMemberBf = (decimal)member.fldCWallet;

                    dmUserNewBal = (decimal)user.fldCWallet - (decimal)request.amount;
                    dmMemberNewBal = (decimal)member.fldCWallet + (decimal)request.amount - dmFee;

                    if(dmUserNewBal < 0)
                    {
                        ModelState.AddModelError("", "*Insufficient CP(Cash Point) balance");
                        return BadRequest(ModelState);
                    }
                    //update balance
                    user.fldCWallet = dmUserNewBal;
                    member.fldCWallet = dmMemberNewBal;
                    

                    strRef = FuncTool.GenerateRef(strRefPrefix);

                    // trf out from user
                    tblcwallettransaction walletTrx = new tblcwallettransaction();
                    walletTrx.fldMID = userId;
                    walletTrx.fldType = "TRANSFER";
                    walletTrx.fldReferenceID = strRef;
                    walletTrx.fldCreator = userId;
                    walletTrx.fldDateTime = DateTime.Now;
                    walletTrx.fldCreatorType = "M";
                    walletTrx.fldBF = dmUserBf;
                    walletTrx.fldIn = 0;
                    walletTrx.fldOut = (decimal)request.amount;
                    walletTrx.fldCF = dmUserNewBal;
                    walletTrx.fldRemark = "Transfer " + String.Format("{0:N}", request.amount) + " CP to " + member.fldCode + " CP, Fees: " + dmFee.ToString();
                    entities.tblcwallettransactions.Add(walletTrx);

                    // trf into member

                    walletTrx = new tblcwallettransaction();
                    walletTrx.fldMID = member.fldID;
                    walletTrx.fldType = "TRANSFER";
                    walletTrx.fldReferenceID = strRef;
                    walletTrx.fldCreator = userId;
                    walletTrx.fldDateTime = DateTime.Now;
                    walletTrx.fldCreatorType = "M";
                    walletTrx.fldBF = dmMemberBf;
                    walletTrx.fldIn = (decimal)request.amount - dmFee;
                    walletTrx.fldOut = 0;
                    walletTrx.fldCF = dmMemberNewBal;
                    walletTrx.fldRemark = "Receive " + String.Format("{0:N}", request.amount - dmFee) + " CP from " + user.fldCode + " CP";
                    entities.tblcwallettransactions.Add(walletTrx);
                    
                }
                #endregion

                #region EP(Register Point)
                else if (request.transferType == "E") //EP(Register Point)
                {
                    dmUserBf = (decimal)user.fldEWallet;
                    dmMemberBf = (decimal)member.fldEWallet;

                    dmUserNewBal = (decimal)user.fldEWallet - (decimal)request.amount;
                    dmMemberNewBal = (decimal)member.fldEWallet + (decimal)request.amount - dmFee;

                    if (dmUserNewBal < 0)
                    {
                        ModelState.AddModelError("", "*Insufficient EP(Register Point) balance");
                        return BadRequest(ModelState);
                    }

                    //update balance
                    user.fldEWallet = dmUserNewBal;
                    member.fldEWallet = dmMemberNewBal;
                    strRef = FuncTool.GenerateRef(strRefPrefix);

                    // trf out from user
                    tblewallettransaction walletTrx = new tblewallettransaction();
                    walletTrx.fldMID = userId;
                    walletTrx.fldType = "TRANSFER";
                    walletTrx.fldReferenceID = strRef;
                    walletTrx.fldCreator = userId;
                    walletTrx.fldDateTime = DateTime.Now;
                    walletTrx.fldCreatorType = "M";
                    walletTrx.fldBF = dmUserBf;
                    walletTrx.fldIn = 0;
                    walletTrx.fldOut = (decimal)request.amount;
                    walletTrx.fldCF = dmUserNewBal;
                    walletTrx.fldRemark = "Transfer " + String.Format("{0:N}", request.amount) + " EP to " + member.fldCode + " EP, Fees: " + dmFee.ToString();
                    entities.tblewallettransactions.Add(walletTrx);

                    // trf into member
                    walletTrx = new tblewallettransaction();
                    walletTrx.fldMID = member.fldID;
                    walletTrx.fldType = "TRANSFER";
                    walletTrx.fldReferenceID = strRef;
                    walletTrx.fldCreator = userId;
                    walletTrx.fldDateTime = DateTime.Now;
                    walletTrx.fldCreatorType = "M";
                    walletTrx.fldBF = dmMemberBf;
                    walletTrx.fldIn = (decimal)request.amount - dmFee;
                    walletTrx.fldOut = 0;
                    walletTrx.fldCF = dmMemberNewBal;
                    walletTrx.fldRemark = "Receive " + String.Format("{0:N}", request.amount - dmFee) + " EP from " + user.fldCode + " EP";
                    entities.tblewallettransactions.Add(walletTrx);
                }
                #endregion

                #region SP(Sold Point)
                else if (request.transferType == "O") //SP(Sold Point)
                {
                    dmUserBf = (decimal)user.fldOWallet;
                    dmMemberBf = (decimal)member.fldOWallet;
                    dmUserNewBal = (decimal)user.fldOWallet - (decimal)request.amount;
                    dmMemberNewBal = (decimal)member.fldOWallet + (decimal)request.amount - dmFee;

                    if (dmUserNewBal < 0)
                    {
                        ModelState.AddModelError("", "*Insufficient wallet balance");
                        return BadRequest(ModelState);
                    }

                    //update balance
                    user.fldOWallet = dmUserNewBal;
                    member.fldOWallet = dmMemberNewBal;
                    strRef = FuncTool.GenerateRef(strRefPrefix);

                    // trf out from user
                    tblowallettransaction walletTrx = new tblowallettransaction();
                    walletTrx.fldMID = userId;
                    walletTrx.fldType = "TRANSFER";
                    walletTrx.fldReferenceID = strRef;
                    walletTrx.fldCreator = userId;
                    walletTrx.fldDateTime = DateTime.Now;
                    walletTrx.fldCreatorType = "M";
                    walletTrx.fldBF = dmUserBf;
                    walletTrx.fldIn = 0;
                    walletTrx.fldOut = (decimal)request.amount;
                    walletTrx.fldCF = dmUserNewBal;
                    walletTrx.fldRemark = "Transfer " + String.Format("{0:N}", request.amount) + " SP to " + member.fldCode + " SP, Fees: " + dmFee.ToString();
                    entities.tblowallettransactions.Add(walletTrx);

                    // trf into member
                    walletTrx = new tblowallettransaction();
                    walletTrx.fldMID = member.fldID;
                    walletTrx.fldType = "TRANSFER";
                    walletTrx.fldReferenceID = strRef;
                    walletTrx.fldCreator = userId;
                    walletTrx.fldDateTime = DateTime.Now;
                    walletTrx.fldCreatorType = "M";
                    walletTrx.fldBF = dmMemberBf;
                    walletTrx.fldIn = (decimal)request.amount - dmFee;
                    walletTrx.fldOut = 0;
                    walletTrx.fldCF = dmMemberNewBal;
                    walletTrx.fldRemark = "Receive " + String.Format("{0:N}", request.amount - dmFee) + " SP from " + user.fldCode + " SP";
                    entities.tblowallettransactions.Add(walletTrx);
                }
                #endregion

                //update tblwallettransfer
                tblwallettransfer objTblwallettransfer = new tblwallettransfer();
                objTblwallettransfer.fldFrMID = userId;
                objTblwallettransfer.fldFrWallet = request.transferType;
                objTblwallettransfer.fldToMID = member.fldID;
                objTblwallettransfer.fldToWallet = request.transferType;
                objTblwallettransfer.fldAmount = (decimal)request.amount;
                objTblwallettransfer.fldFees = dmFee;
                objTblwallettransfer.fldReference = strRef;
                objTblwallettransfer.fldDateTime = DateTime.Now;
                objTblwallettransfer.fldCreator = userId;
                objTblwallettransfer.fldCreatorType = "M";
                entities.tblwallettransfers.Add(objTblwallettransfer);
                entities.SaveChanges();
                return Ok();
            }
        }

        [Route("transferhistory")]
        [HttpPost]
        public IHttpActionResult TransferHistoryList([FromBody] WalletTransferSearchModel request)
        {
            long userId = GetUserId();
            
            using (VmallDbEntities entities = new VmallDbEntities())
            {
                var user = entities.tblmemberships.SingleOrDefault(x => x.fldID == userId);

                var query = from wallet in entities.tblwallettransfers
                            join member in entities.tblmemberships on wallet.fldToMID equals member.fldID
                            where wallet.fldFrMID == userId
                            select new { wallet, member};

                if (request.fromDate != null && request.toDate != null)
                {
                    DateTime dtFrom;
                    DateTime dtTo;

                    if (DateTime.TryParse(request.fromDate, out dtFrom) && DateTime.TryParse(request.toDate, out dtTo))
                    {
                        dtTo = dtTo.AddHours(23).AddMinutes(59).AddSeconds(59);
                        query = query.Where(p => p.wallet.fldDateTime >= dtFrom && p.wallet.fldDateTime <= dtTo);
                    }
                }

                if(request.walletType != null)
                {
                    query = query.Where(p => p.wallet.fldFrWallet == request.walletType);
                }

                var queryOut = query.Select(p => new
                {
                    fldID = p.wallet.fldID,
                    fldFrMID = p.wallet.fldFrMID,
                    fldFrWallet = p.wallet.fldFrWallet,
                    fldToMID = p.wallet.fldToMID,
                    fldToWallet = p.wallet.fldToWallet,
                    fldAmount = p.wallet.fldAmount,
                    fldFees = p.wallet.fldFees,
                    fldReference = p.wallet.fldReference,
                    fldDateTime = p.wallet.fldDateTime,
                    fldCreator = p.wallet.fldCreator,
                    fldCreatorType = p.wallet.fldCreatorType,
                    fldFrCode = user.fldCode,
                    fldFrName = user.fldName,
                    fldICNo = user.fldICNo,
                    fldToCode = p.member.fldCode
                });

                queryOut = queryOut.OrderByDescending(p => p.fldDateTime);

                var result = CreatePageResult(request.pageSize, request.pageNumber, queryOut);

                return Ok(result);
            }
        }

        [Route("withdraw")]
        [HttpPost]
        public IHttpActionResult Withdraw([FromBody] WalletWithdrawModel request)
        {
            long userId = GetUserId();
            string strRefPrefix = "WD";

            using (VmallDbEntities entities = new VmallDbEntities())
            {
                var user = entities.tblmemberships.SingleOrDefault(x => x.fldID == userId);
                
                decimal dmUserBf = (decimal)user.fldCWallet;
                
                decimal dmUserNewBal;
                string strRef = "";


                decimal dmFee = 0;
                var fee = entities.tblsettings.SingleOrDefault(x => x.fldSetting == "WalletWithdrawFee"); //


                if (fee != null)
                {
                    dmFee = decimal.Parse(fee.fldValue) * request.amount;
                }


                #region CP(Cash Point)
                if (request.walletType == "C") //CP(Cash Point)
                {

                    dmUserNewBal = (decimal)user.fldCWallet - (decimal)request.amount;

                    if (dmUserNewBal < 0)
                    {
                        ModelState.AddModelError("", "*Insufficient CP(Cash Point) balance");
                        return BadRequest(ModelState);
                    }

                    //update balance
                    user.fldCWallet = dmUserNewBal;
                    
                    strRef = FuncTool.GenerateRef(strRefPrefix);

                    // trf out from user
                    tblcwallettransaction walletTrx = new tblcwallettransaction();
                    walletTrx.fldMID = userId;
                    walletTrx.fldType = "WITHDRAW";
                    walletTrx.fldReferenceID = strRef;
                    walletTrx.fldCreator = userId;
                    walletTrx.fldDateTime = DateTime.Now;
                    walletTrx.fldCreatorType = "M";
                    walletTrx.fldBF = dmUserBf;
                    walletTrx.fldIn = 0;
                    walletTrx.fldOut = (decimal)request.amount;
                    walletTrx.fldCF = dmUserNewBal;
                    walletTrx.fldRemark = "Withdraw " + string.Format("{0:N}", request.amount) + " CP from " + user.fldCode + ", Fees: " + dmFee.ToString();
                    entities.tblcwallettransactions.Add(walletTrx);

                }
                #endregion

                #region SP(Sold Point)
                else if (request.walletType == "O") //SP(Sold Point)
                {
                    dmUserNewBal = (decimal)user.fldOWallet - (decimal)request.amount;

                    if (dmUserNewBal < 0)
                    {
                        ModelState.AddModelError("", "*Insufficient wallet balance");
                        return BadRequest(ModelState);
                    }

                    //update balance
                    user.fldOWallet = dmUserNewBal;
                    strRef = FuncTool.GenerateRef(strRefPrefix);

                    // trf out from user
                    tblowallettransaction walletTrx = new tblowallettransaction();
                    walletTrx.fldMID = userId;
                    walletTrx.fldType = "WITHDRAW";
                    walletTrx.fldReferenceID = strRef;
                    walletTrx.fldCreator = userId;
                    walletTrx.fldDateTime = DateTime.Now;
                    walletTrx.fldCreatorType = "M";
                    walletTrx.fldBF = dmUserBf;
                    walletTrx.fldIn = 0;
                    walletTrx.fldOut = (decimal)request.amount;
                    walletTrx.fldCF = dmUserNewBal;
                    walletTrx.fldRemark = "Withdraw " + String.Format("{0:N}", request.amount) + " SP from " + user.fldCode + ", Fees: " + dmFee.ToString();
                    entities.tblowallettransactions.Add(walletTrx);
                }
                #endregion

                //update tblwalletwithdraw
                tblwalletwithdraw objTblwalletWithdraw = new tblwalletwithdraw();
                objTblwalletWithdraw.fldDateTime = DateTime.Now;
                objTblwalletWithdraw.fldMID = userId;
                objTblwalletWithdraw.fldWallet = request.walletType;
                objTblwalletWithdraw.fldBankID = (long)user.fldBankID;
                objTblwalletWithdraw.fldBankBranch = user.fldBankBranch;
                objTblwalletWithdraw.fldAccHolder = user.fldAccHolder;
                objTblwalletWithdraw.fldAccNo = user.fldAccNo;
                objTblwalletWithdraw.fldAmount = (decimal)request.amount;
                objTblwalletWithdraw.fldFees = dmFee;
                objTblwalletWithdraw.fldReference = strRef;
                objTblwalletWithdraw.fldStatus = "P";
                objTblwalletWithdraw.fldCreator = userId;
                objTblwalletWithdraw.fldCreatorType = "M";
                objTblwalletWithdraw.fldProcessComment = "";
                objTblwalletWithdraw.fldProcessBy = 0;
                objTblwalletWithdraw.fldProcessDateTime = DateTime.MinValue;

                entities.tblwalletwithdraws.Add(objTblwalletWithdraw);
                entities.SaveChanges();
                return Ok();
            }
        }

        [Route("withdrawhistory")]
        [HttpPost]
        public IHttpActionResult WithdrawHistoryList([FromBody] WalletWithdrawSearchModel request)
        {
            long userId = GetUserId();

            using (VmallDbEntities entities = new VmallDbEntities())
            {
                var user = entities.tblmemberships.SingleOrDefault(x => x.fldID == userId);

                var query = from wallet in entities.tblwalletwithdraws
                            from member in entities.tblmemberships.Where(p=>p.fldID == wallet.fldMID)
                            from bank in entities.tblbanks.Where(p=>p.fldID == member.fldBankID).DefaultIfEmpty()
                            where wallet.fldMID == userId
                            select new
                            {
                                fldID = wallet.fldID,
                                fldDateTime = wallet.fldDateTime,
                                fldMID = wallet.fldMID,
                                fldWallet = wallet.fldWallet,
                                fldBankID = wallet.fldBankID,
                                fldBankBranch = wallet.fldBankBranch,
                                fldAccHolder = wallet.fldAccHolder,
                                fldAccNo = wallet.fldAccNo,
                                fldAmount = wallet.fldAmount,
                                fldFees = wallet.fldFees,
                                fldReference = wallet.fldReference,
                                fldStatus = wallet.fldStatus,
                                fldCreator = wallet.fldCreator,
                                fldCreatorType = wallet.fldCreatorType,
                                fldProcessComment = wallet.fldProcessComment,
                                fldProcessBy = wallet.fldProcessBy,
                                fldProcessDateTime = wallet.fldProcessDateTime,
                                fldName = member.fldName,
                                fldICNo = member.fldICNo,
                                fldCode = member.fldCode,
                                fldBankName = bank.fldName
                                
                            };

                if (request.fromDate != null && request.toDate != null)
                {
                    DateTime dtFrom;
                    DateTime dtTo;

                    if (DateTime.TryParse(request.fromDate, out dtFrom) && DateTime.TryParse(request.toDate, out dtTo))
                    {
                        dtTo = dtTo.AddHours(23).AddMinutes(59).AddSeconds(59);
                        query = query.Where(p => p.fldDateTime >= dtFrom && p.fldDateTime <= dtTo);
                    }
                }

                if (request.walletType != null)
                {
                    query = query.Where(p => p.fldWallet == request.walletType);
                }

                if (!string.IsNullOrEmpty(request.status))
                {
                    query = query.Where(p => p.fldStatus == request.status);
                }

                query = query.OrderByDescending(p => p.fldDateTime);

                var result = CreatePageResult(request.pageSize, request.pageNumber, query);

                return Ok(result);
            }
        }

        [Route("trantype")]
        [HttpGet]
        public IHttpActionResult TransactionTypeList()
        {
            using (VmallDbEntities entities = new VmallDbEntities())
            {
                WalletTranType objList = new WalletTranType();

                //CP(Cash Point)
                var cp = entities.tblcwallettransactions.Select(x =>x.fldType).Distinct().ToList();
                objList.C = cp;

                //EP(Register Point)
                var ep = entities.tblewallettransactions.Select(x => x.fldType).Distinct().ToList();
                objList.E = ep;

                //MP(Mall Point)
                var mp = entities.tblbwallettransactions.Select(x => x.fldType).Distinct().ToList();
                objList.B = mp;

                //PP(Purchase Point)
                var pp = entities.tblwwallettransactions.Select(x => x.fldType).Distinct().ToList();
                objList.W = pp;

                //SP(Sold Point)
                var sp= entities.tblowallettransactions.Select(x => x.fldType).Distinct().ToList();
                objList.O = sp;

                //AP(Ads Point)
                var ap = entities.tblawallettransactions.Select(x => x.fldType).Distinct().ToList();
                objList.A = ap;

                //Tradable V-Coin
                var vcoin = entities.tblsharetransactions.Select(x => x.fldType).Distinct().ToList();
                objList.S = vcoin;

                //Locked V-Coin
                var lockVCoin = entities.tblshare2transaction.Select(x => x.fldType).Distinct().ToList();
                objList.L = lockVCoin;

                
                return Ok(objList);
            }
        }

        [Route("tranhistory")]
        [HttpPost]
        public IHttpActionResult TransactionHistoryList([FromBody] WalletTranHistSearchModel request)
        {
            long userId = GetUserId();
            
            DateTime dtFrom = DateTime.Now;
            DateTime dtTo = DateTime.Now;

            if (request.fromDate != null && request.toDate != null)
            {
                if (DateTime.TryParse(request.fromDate, out dtFrom) && DateTime.TryParse(request.toDate, out dtTo))
                {
                    dtTo = dtTo.AddHours(23).AddMinutes(59).AddSeconds(59);
                }
            }


            using (VmallDbEntities entities = new VmallDbEntities())
            {
                switch (request.walletType.ToUpper())
                {
                    case "C":
                        var queryCP = from wallet in entities.tblcwallettransactions
                                where wallet.fldMID == userId && wallet.fldDateTime >= dtFrom && wallet.fldDateTime <= dtTo
                                select wallet;
                        if(!string.IsNullOrEmpty(request.tranType))
                        {
                            queryCP = queryCP.Where(x => x.fldType.Equals(request.tranType,StringComparison.OrdinalIgnoreCase));
                        }
                        queryCP = queryCP.OrderByDescending(p => p.fldDateTime);

                        var resultCP = CreatePageResult(request.pageSize, request.pageNumber, queryCP);
                        return Ok(resultCP);
                        break;

                    case "E":
                        var queryEP = from wallet in entities.tblewallettransactions
                                    where wallet.fldMID == userId && wallet.fldDateTime >= dtFrom && wallet.fldDateTime <= dtTo
                                    select wallet;
                        if (!string.IsNullOrEmpty(request.tranType))
                        {
                            queryEP = queryEP.Where(x => x.fldType.Equals(request.tranType, StringComparison.OrdinalIgnoreCase));
                        }
                        queryEP = queryEP.OrderByDescending(p => p.fldDateTime);

                        var resultEP = CreatePageResult(request.pageSize, request.pageNumber, queryEP);
                        return Ok(resultEP);
                        break;

                    case "B":
                        var queryMP = from wallet in entities.tblbwallettransactions
                                      where wallet.fldMID == userId && wallet.fldDateTime >= dtFrom && wallet.fldDateTime <= dtTo
                                      select wallet;
                        if (!string.IsNullOrEmpty(request.tranType))
                        {
                            queryMP = queryMP.Where(x => x.fldType.Equals(request.tranType, StringComparison.OrdinalIgnoreCase));
                        }
                        queryMP = queryMP.OrderByDescending(p => p.fldDateTime);

                        var resultMP = CreatePageResult(request.pageSize, request.pageNumber, queryMP);
                        return Ok(resultMP);
                        break;

                    case "W":
                        var queryPP = from wallet in entities.tblwwallettransactions
                                      where wallet.fldMID == userId && wallet.fldDateTime >= dtFrom && wallet.fldDateTime <= dtTo
                                      select wallet;
                        if (!string.IsNullOrEmpty(request.tranType))
                        {
                            queryPP = queryPP.Where(x => x.fldType.Equals(request.tranType, StringComparison.OrdinalIgnoreCase));
                        }
                        queryPP = queryPP.OrderByDescending(p => p.fldDateTime);

                        var resultPP = CreatePageResult(request.pageSize, request.pageNumber, queryPP);
                        return Ok(resultPP);
                        break;

                    case "O":
                        var querySP = from wallet in entities.tblowallettransactions
                                      where wallet.fldMID == userId && wallet.fldDateTime >= dtFrom && wallet.fldDateTime <= dtTo
                                      select wallet;
                        if (!string.IsNullOrEmpty(request.tranType))
                        {
                            querySP = querySP.Where(x => x.fldType.Equals(request.tranType, StringComparison.OrdinalIgnoreCase));
                        }
                        querySP = querySP.OrderByDescending(p => p.fldDateTime);

                        var resultSP = CreatePageResult(request.pageSize, request.pageNumber, querySP);
                        return Ok(resultSP);
                        break;

                    case "A":
                        var queryAP = from wallet in entities.tblawallettransactions
                                      where wallet.fldMID == userId && wallet.fldDateTime >= dtFrom && wallet.fldDateTime <= dtTo
                                      select wallet;
                        if (!string.IsNullOrEmpty(request.tranType))
                        {
                            queryAP = queryAP.Where(x => x.fldType.Equals(request.tranType, StringComparison.OrdinalIgnoreCase));
                        }
                        queryAP = queryAP.OrderByDescending(p => p.fldDateTime);

                        var resultAP = CreatePageResult(request.pageSize, request.pageNumber, queryAP);
                        return Ok(resultAP);
                        break;

                    case "S":
                        var queryVC = from wallet in entities.tblsharetransactions
                                      where wallet.fldMID == userId && wallet.fldDateTime >= dtFrom && wallet.fldDateTime <= dtTo
                                      select wallet;
                        if (!string.IsNullOrEmpty(request.tranType))
                        {
                            queryVC = queryVC.Where(x => x.fldType.Equals(request.tranType, StringComparison.OrdinalIgnoreCase));
                        }
                        queryVC = queryVC.OrderByDescending(p => p.fldDateTime);

                        var resultVC = CreatePageResult(request.pageSize, request.pageNumber, queryVC);
                        return Ok(resultVC);
                        break;

                    case "L":
                        var queryLC = from wallet in entities.tblshare2transaction
                                      where wallet.fldMID == userId && wallet.fldDateTime >= dtFrom && wallet.fldDateTime <= dtTo
                                      select wallet;
                        if (!string.IsNullOrEmpty(request.tranType))
                        {
                            queryLC = queryLC.Where(x => x.fldType.Equals(request.tranType, StringComparison.OrdinalIgnoreCase));
                        }
                        queryLC = queryLC.OrderByDescending(p => p.fldDateTime);

                        var resultLC = CreatePageResult(request.pageSize, request.pageNumber, queryLC);
                        return Ok(resultLC);
                        break;
                    default:
                        return Ok();
                        break;
                }

                
            }
        }

        [Route("admin/transfer")]
        [HttpPost]
        public IHttpActionResult AdminTransfer([FromBody] AdminWalletTransferModel request)
        {
            long userId = GetUserId();
            string strRefPrefix = "TF";

            if (!isAdmin)
                return BadRequest();

            using (VmallDbEntities entities = new VmallDbEntities())
            {
                //get setting

                if (!AdminTransferValidate(request))
                {
                    return BadRequest(ModelState);
                }


                var fromMember = entities.tblmemberships.SingleOrDefault(x => x.fldCode == request.fromMemberId);
                var toMember = entities.tblmemberships.SingleOrDefault(x => x.fldCode == request.toMemberId);

                decimal dcFromMemberBf = 0;
                decimal dcToMemberBf = 0;

                decimal dmFromMemberNewBal;
                decimal dcToMemberNewBal;
                string strRef = FuncTool.GenerateRef(strRefPrefix);


                decimal dmFee = 0;

                //var fee = entities.tblsettings.SingleOrDefault(x => x.fldSetting == "WalletTransferFee"); //WalletTransferFee


                //if (fee != null)
                //{
                //    dmFee = decimal.Parse(fee.fldValue);
                //}

                #region Transfer From Process

                #region CP(Cash Point)
                if (request.fromWalletype == "C") //CP(Cash Point)
                {
                    dcFromMemberBf = (decimal)fromMember.fldCWallet;
                    dmFromMemberNewBal = (decimal)fromMember.fldCWallet - (decimal)request.amount;
                    
                    //update balance
                    fromMember.fldCWallet = dmFromMemberNewBal;
                    
                    // trf out from user
                    tblcwallettransaction walletTrx = new tblcwallettransaction();
                    walletTrx.fldMID = fromMember.fldID;
                    walletTrx.fldType = "TRANSFER";
                    walletTrx.fldReferenceID = strRef;
                    walletTrx.fldCreator = userId;
                    walletTrx.fldDateTime = DateTime.Now;
                    walletTrx.fldCreatorType = "A";
                    walletTrx.fldBF = dcFromMemberBf;
                    walletTrx.fldIn = 0;
                    walletTrx.fldOut = (decimal)request.amount;
                    walletTrx.fldCF = dmFromMemberNewBal;
                    walletTrx.fldRemark = "Transfer " + String.Format("{0:N}", request.amount) + " " + GetTrfType(request.fromWalletype) + " to " + toMember.fldCode + " " + GetTrfType(request.toWalletType) + ", Fees: " + dmFee.ToString();
                    if (!string.IsNullOrEmpty(request.remark))
                        walletTrx.fldRemark = walletTrx.fldRemark + ", " + request.remark;

                    entities.tblcwallettransactions.Add(walletTrx);
                }
                #endregion

                #region EP(Register Point)
                else if (request.fromWalletype == "E") //EP(Register Point)
                {
                    dcFromMemberBf = (decimal)fromMember.fldEWallet;
                    dmFromMemberNewBal = (decimal)fromMember.fldEWallet - (decimal)request.amount;
                    
                    //update balance
                    fromMember.fldEWallet = dmFromMemberNewBal;
                    
                    // trf out from user
                    tblewallettransaction walletTrx = new tblewallettransaction();
                    walletTrx.fldMID = fromMember.fldID;
                    walletTrx.fldType = "TRANSFER";
                    walletTrx.fldReferenceID = strRef;
                    walletTrx.fldCreator = userId;
                    walletTrx.fldDateTime = DateTime.Now;
                    walletTrx.fldCreatorType = "A";
                    walletTrx.fldBF = dcFromMemberBf;
                    walletTrx.fldIn = 0;
                    walletTrx.fldOut = (decimal)request.amount;
                    walletTrx.fldCF = dmFromMemberNewBal;
                    walletTrx.fldRemark = "Transfer " + String.Format("{0:N}", request.amount) + " " + GetTrfType(request.fromWalletype) + " to " + toMember.fldCode + " " + GetTrfType(request.toWalletType) + ", Fees: " + dmFee.ToString();
                    if (!string.IsNullOrEmpty(request.remark))
                        walletTrx.fldRemark = walletTrx.fldRemark + ", " + request.remark;
                    entities.tblewallettransactions.Add(walletTrx);
                }
                #endregion

                #region SP(Sold Point)
                else if (request.fromWalletype == "O") //SP(Sold Point)
                {
                    dcFromMemberBf = (decimal)fromMember.fldOWallet;
                    dmFromMemberNewBal = (decimal)fromMember.fldOWallet - (decimal)request.amount;
                    
                    //update balance
                    fromMember.fldOWallet = dmFromMemberNewBal;
                    
                    // trf out from user
                    tblowallettransaction walletTrx = new tblowallettransaction();
                    walletTrx.fldMID = fromMember.fldID;
                    walletTrx.fldType = "TRANSFER";
                    walletTrx.fldReferenceID = strRef;
                    walletTrx.fldCreator = userId;
                    walletTrx.fldDateTime = DateTime.Now;
                    walletTrx.fldCreatorType = "A";
                    walletTrx.fldBF = dcFromMemberBf;
                    walletTrx.fldIn = 0;
                    walletTrx.fldOut = (decimal)request.amount;
                    walletTrx.fldCF = dmFromMemberNewBal;
                    walletTrx.fldRemark = "Transfer " + String.Format("{0:N}", request.amount) + " " + GetTrfType(request.fromWalletype) + " to " + toMember.fldCode + " " + GetTrfType(request.toWalletType) + ", Fees: " + dmFee.ToString();
                    if (!string.IsNullOrEmpty(request.remark))
                        walletTrx.fldRemark = walletTrx.fldRemark + ", " + request.remark;
                    entities.tblowallettransactions.Add(walletTrx);

                }
                #endregion

                #region MP(Mall Point)
                else if (request.fromWalletype == "B")
                {
                    dcFromMemberBf = (decimal)fromMember.fldBWallet;
                    dmFromMemberNewBal = (decimal)fromMember.fldBWallet - (decimal)request.amount;
                    
                    //update balance
                    fromMember.fldBWallet = dmFromMemberNewBal;
                    
                    // trf out from user
                    tblbwallettransaction walletTrx = new tblbwallettransaction();
                    walletTrx.fldMID = fromMember.fldID;
                    walletTrx.fldType = "TRANSFER";
                    walletTrx.fldReferenceID = strRef;
                    walletTrx.fldCreator = userId;
                    walletTrx.fldDateTime = DateTime.Now;
                    walletTrx.fldCreatorType = "A";
                    walletTrx.fldBF = dcFromMemberBf;
                    walletTrx.fldIn = 0;
                    walletTrx.fldOut = (decimal)request.amount;
                    walletTrx.fldCF = dmFromMemberNewBal;
                    walletTrx.fldRemark = "Transfer " + String.Format("{0:N}", request.amount) + " " + GetTrfType(request.fromWalletype) + " to " + toMember.fldCode + " " + GetTrfType(request.toWalletType) + ", Fees: " + dmFee.ToString();
                    if (!string.IsNullOrEmpty(request.remark))
                        walletTrx.fldRemark = walletTrx.fldRemark + ", " + request.remark;
                    entities.tblbwallettransactions.Add(walletTrx);
                }
                #endregion

                #region PP(Purchase Point)
                else if (request.fromWalletype == "W")
                {
                    dcFromMemberBf = (decimal)fromMember.fldWWallet;
                    dmFromMemberNewBal = (decimal)fromMember.fldWWallet - (decimal)request.amount;
                    
                    //update balance
                    fromMember.fldWWallet = dmFromMemberNewBal;
                    
                    // trf out from user
                    tblwwallettransaction walletTrx = new tblwwallettransaction();
                    walletTrx.fldMID = fromMember.fldID;
                    walletTrx.fldType = "TRANSFER";
                    walletTrx.fldReferenceID = strRef;
                    walletTrx.fldCreator = userId;
                    walletTrx.fldDateTime = DateTime.Now;
                    walletTrx.fldCreatorType = "A";
                    walletTrx.fldBF = dcFromMemberBf;
                    walletTrx.fldIn = 0;
                    walletTrx.fldOut = (decimal)request.amount;
                    walletTrx.fldCF = dmFromMemberNewBal;
                    walletTrx.fldRemark = "Transfer " + String.Format("{0:N}", request.amount) + " " + GetTrfType(request.fromWalletype) + " to " + toMember.fldCode + " " + GetTrfType(request.toWalletType) + ", Fees: " + dmFee.ToString();
                    if (!string.IsNullOrEmpty(request.remark))
                        walletTrx.fldRemark = walletTrx.fldRemark + ", " + request.remark;
                    entities.tblwwallettransactions.Add(walletTrx);
                }
                #endregion

                #region AP(Ads Point)
                else if (request.fromWalletype == "A")
                {
                    dcFromMemberBf = (decimal)fromMember.fldAWallet;
                    dmFromMemberNewBal = (decimal)fromMember.fldAWallet - (decimal)request.amount;
                    //update balance
                    fromMember.fldAWallet = dmFromMemberNewBal;
                    
                    // trf out from user
                    tblawallettransaction walletTrx = new tblawallettransaction();
                    walletTrx.fldMID = fromMember.fldID;
                    walletTrx.fldType = "TRANSFER";
                    walletTrx.fldReferenceID = strRef;
                    walletTrx.fldCreator = userId;
                    walletTrx.fldDateTime = DateTime.Now;
                    walletTrx.fldCreatorType = "A";
                    walletTrx.fldBF = dcFromMemberBf;
                    walletTrx.fldIn = 0;
                    walletTrx.fldOut = (decimal)request.amount;
                    walletTrx.fldCF = dmFromMemberNewBal;
                    walletTrx.fldRemark = "Transfer " + String.Format("{0:N}", request.amount) + " " + GetTrfType(request.fromWalletype) + " to " + toMember.fldCode + " " + GetTrfType(request.toWalletType) + ", Fees: " + dmFee.ToString();
                    if (!string.IsNullOrEmpty(request.remark))
                        walletTrx.fldRemark = walletTrx.fldRemark + ", " + request.remark;
                    entities.tblawallettransactions.Add(walletTrx);
                }
                #endregion

                #endregion

                #region Transfer To Process
                #region CP(Cash Point)
                if (request.toWalletType == "C") //CP(Cash Point)
                {
                    dcToMemberBf = (decimal)toMember.fldCWallet;
                    dcToMemberNewBal = (decimal)toMember.fldCWallet + (decimal)request.amount - dmFee;

                    //update balance
                    toMember.fldCWallet = dcToMemberNewBal;
                    
                    // trf out from user
                    tblcwallettransaction walletTrx = new tblcwallettransaction();
                    walletTrx.fldMID = toMember.fldID;
                    walletTrx.fldType = "TRANSFER";
                    walletTrx.fldReferenceID = strRef;
                    walletTrx.fldCreator = userId;
                    walletTrx.fldDateTime = DateTime.Now;
                    walletTrx.fldCreatorType = "A";
                    walletTrx.fldBF = dcToMemberBf;
                    walletTrx.fldIn = 0;
                    walletTrx.fldOut = (decimal)request.amount;
                    walletTrx.fldCF = dcToMemberNewBal;
                    walletTrx.fldRemark = "Receive " + String.Format("{0:N}", request.amount) + " " + GetTrfType(request.toWalletType) + " from " + fromMember.fldCode + " " + GetTrfType(request.fromWalletype);
                    if (!string.IsNullOrEmpty(request.remark))
                        walletTrx.fldRemark = walletTrx.fldRemark + ", " + request.remark;

                    entities.tblcwallettransactions.Add(walletTrx);
                }
                #endregion

                #region EP(Register Point)
                else if (request.toWalletType == "E") //EP(Register Point)
                {
                    dcToMemberBf = (decimal)toMember.fldEWallet;
                    dcToMemberNewBal = (decimal)toMember.fldEWallet + (decimal)request.amount - dmFee;

                    //update balance
                    toMember.fldEWallet = dcToMemberNewBal;

                    // trf out from user
                    tblewallettransaction walletTrx = new tblewallettransaction();
                    walletTrx.fldMID = toMember.fldID;
                    walletTrx.fldType = "TRANSFER";
                    walletTrx.fldReferenceID = strRef;
                    walletTrx.fldCreator = userId;
                    walletTrx.fldDateTime = DateTime.Now;
                    walletTrx.fldCreatorType = "A";
                    walletTrx.fldBF = dcToMemberBf;
                    walletTrx.fldIn = 0;
                    walletTrx.fldOut = (decimal)request.amount;
                    walletTrx.fldCF = dcToMemberNewBal;
                    walletTrx.fldRemark = "Receive " + String.Format("{0:N}", request.amount) + " " + GetTrfType(request.toWalletType) + " from " + fromMember.fldCode + " " + GetTrfType(request.fromWalletype);
                    if (!string.IsNullOrEmpty(request.remark))
                        walletTrx.fldRemark = walletTrx.fldRemark + ", " + request.remark;

                    entities.tblewallettransactions.Add(walletTrx);
                }
                #endregion

                #region SP(Sold Point)
                else if (request.toWalletType == "O") //SP(Sold Point)
                {
                    dcToMemberBf = (decimal)toMember.fldOWallet;
                    dcToMemberNewBal = (decimal)toMember.fldOWallet + (decimal)request.amount - dmFee;

                    //update balance
                    toMember.fldOWallet = dcToMemberNewBal;

                    // trf out from user
                    tblowallettransaction walletTrx = new tblowallettransaction();
                    walletTrx.fldMID = toMember.fldID;
                    walletTrx.fldType = "TRANSFER";
                    walletTrx.fldReferenceID = strRef;
                    walletTrx.fldCreator = userId;
                    walletTrx.fldDateTime = DateTime.Now;
                    walletTrx.fldCreatorType = "A";
                    walletTrx.fldBF = dcToMemberBf;
                    walletTrx.fldIn = 0;
                    walletTrx.fldOut = (decimal)request.amount;
                    walletTrx.fldCF = dcToMemberNewBal;
                    walletTrx.fldRemark = "Receive " + String.Format("{0:N}", request.amount) + " " + GetTrfType(request.toWalletType) + " from " + fromMember.fldCode + " " + GetTrfType(request.fromWalletype);
                    if (!string.IsNullOrEmpty(request.remark))
                        walletTrx.fldRemark = walletTrx.fldRemark + ", " + request.remark;

                    entities.tblowallettransactions.Add(walletTrx);

                }
                #endregion

                #region MP(Mall Point)
                else if (request.toWalletType == "B")
                {
                    dcToMemberBf = (decimal)toMember.fldBWallet;
                    dcToMemberNewBal = (decimal)toMember.fldBWallet + (decimal)request.amount - dmFee;

                    //update balance
                    toMember.fldBWallet = dcToMemberNewBal;

                    // trf out from user
                    tblbwallettransaction walletTrx = new tblbwallettransaction();
                    walletTrx.fldMID = toMember.fldID;
                    walletTrx.fldType = "TRANSFER";
                    walletTrx.fldReferenceID = strRef;
                    walletTrx.fldCreator = userId;
                    walletTrx.fldDateTime = DateTime.Now;
                    walletTrx.fldCreatorType = "A";
                    walletTrx.fldBF = dcToMemberBf;
                    walletTrx.fldIn = 0;
                    walletTrx.fldOut = (decimal)request.amount;
                    walletTrx.fldCF = dcToMemberNewBal;
                    walletTrx.fldRemark = "Receive " + String.Format("{0:N}", request.amount) + " " + GetTrfType(request.toWalletType) + " from " + fromMember.fldCode + " " + GetTrfType(request.fromWalletype);
                    if (!string.IsNullOrEmpty(request.remark))
                        walletTrx.fldRemark = walletTrx.fldRemark + ", " + request.remark;

                    entities.tblbwallettransactions.Add(walletTrx);
                }
                #endregion

                #region PP(Purchase Point)
                else if (request.toWalletType == "W")
                {
                    dcToMemberBf = (decimal)toMember.fldWWallet;
                    dcToMemberNewBal = (decimal)toMember.fldWWallet + (decimal)request.amount - dmFee;

                    //update balance
                    toMember.fldWWallet = dcToMemberNewBal;

                    // trf out from user
                    tblwwallettransaction walletTrx = new tblwwallettransaction();
                    walletTrx.fldMID = toMember.fldID;
                    walletTrx.fldType = "TRANSFER";
                    walletTrx.fldReferenceID = strRef;
                    walletTrx.fldCreator = userId;
                    walletTrx.fldDateTime = DateTime.Now;
                    walletTrx.fldCreatorType = "A";
                    walletTrx.fldBF = dcToMemberBf;
                    walletTrx.fldIn = 0;
                    walletTrx.fldOut = (decimal)request.amount;
                    walletTrx.fldCF = dcToMemberNewBal;
                    walletTrx.fldRemark = "Receive " + String.Format("{0:N}", request.amount) + " " + GetTrfType(request.toWalletType) + " from " + fromMember.fldCode + " " + GetTrfType(request.fromWalletype);
                    if (!string.IsNullOrEmpty(request.remark))
                        walletTrx.fldRemark = walletTrx.fldRemark + ", " + request.remark;

                    entities.tblwwallettransactions.Add(walletTrx);
                }
                #endregion

                #region AP(Ads Point)
                else if (request.toWalletType == "A")
                {
                    dcToMemberBf = (decimal)toMember.fldAWallet;
                    dcToMemberNewBal = (decimal)toMember.fldAWallet + (decimal)request.amount - dmFee;

                    //update balance
                    toMember.fldAWallet = dcToMemberNewBal;

                    // trf out from user
                    tblawallettransaction walletTrx = new tblawallettransaction();
                    walletTrx.fldMID = toMember.fldID;
                    walletTrx.fldType = "TRANSFER";
                    walletTrx.fldReferenceID = strRef;
                    walletTrx.fldCreator = userId;
                    walletTrx.fldDateTime = DateTime.Now;
                    walletTrx.fldCreatorType = "A";
                    walletTrx.fldBF = dcToMemberBf;
                    walletTrx.fldIn = 0;
                    walletTrx.fldOut = (decimal)request.amount;
                    walletTrx.fldCF = dcToMemberNewBal;
                    walletTrx.fldRemark = "Receive " + String.Format("{0:N}", request.amount) + " " + GetTrfType(request.toWalletType) + " from " + fromMember.fldCode + " " + GetTrfType(request.fromWalletype);
                    if (!string.IsNullOrEmpty(request.remark))
                        walletTrx.fldRemark = walletTrx.fldRemark + ", " + request.remark;

                    entities.tblawallettransactions.Add(walletTrx);
                }
                #endregion
                #endregion


                //update tblwallettransfer
                tblwallettransfer objTblwallettransfer = new tblwallettransfer();
                objTblwallettransfer.fldFrMID = fromMember.fldID;
                objTblwallettransfer.fldFrWallet = request.fromWalletype;
                objTblwallettransfer.fldToMID = toMember.fldID;
                objTblwallettransfer.fldToWallet = request.toWalletType;
                objTblwallettransfer.fldAmount = (decimal)request.amount;
                objTblwallettransfer.fldFees = dmFee;
                objTblwallettransfer.fldReference = strRef;
                objTblwallettransfer.fldDateTime = DateTime.Now;
                objTblwallettransfer.fldCreator = userId;
                objTblwallettransfer.fldCreatorType = "A";
                entities.tblwallettransfers.Add(objTblwallettransfer);
                entities.SaveChanges();
                return Ok();
            }
        }

        [Route("admin/topup")]
        [HttpPost]
        public IHttpActionResult AdminTopUp([FromBody] WalletTopupModel request)
        {
            long userId = GetUserId();
            string strRefPrefix = "TW";
            string strType = "TOPUP WALLET";
            string strCreator = "A";

            if (!isAdmin)
                return BadRequest();

            if (!AdminTopUpValidate(request))
            {
                return BadRequest(ModelState);
            }

            decimal dcMemberBf = 0;
            decimal dcMemberNewBal;
            string strRef = FuncTool.GenerateRef(strRefPrefix);

            using (VmallDbEntities entities = new VmallDbEntities())
            {
                var member = entities.tblmemberships.SingleOrDefault(x => x.fldCode == request.memberId);

                #region CP(Cash Point)
                if (request.walletType == "C")
                {
                    dcMemberBf = (decimal)member.fldCWallet;
                    dcMemberNewBal = (decimal)member.fldCWallet + (decimal)request.amount;
                    member.fldCWallet = dcMemberNewBal;

                    tblcwallettransaction walletTrx = new tblcwallettransaction();
                    walletTrx.fldMID = member.fldID;
                    walletTrx.fldType = strType;
                    walletTrx.fldReferenceID = strRef;
                    walletTrx.fldCreator = userId;
                    walletTrx.fldDateTime = DateTime.Now;
                    walletTrx.fldCreatorType = strCreator;
                    walletTrx.fldBF = dcMemberBf;
                    walletTrx.fldOut = 0;
                    walletTrx.fldIn = (decimal)request.amount;
                    walletTrx.fldCF = dcMemberNewBal;
                    walletTrx.fldRemark = "Topup Wallet: " + String.Format("{0:N}", request.amount) + ", Payment Type: " + request.payType;
                    if (!string.IsNullOrEmpty(request.remark))
                        walletTrx.fldRemark = walletTrx.fldRemark + ", " + request.remark;

                    entities.tblcwallettransactions.Add(walletTrx);
                }
                #endregion

                #region EP(Register Point)
                else if (request.walletType == "E")
                {
                    dcMemberBf = (decimal)member.fldEWallet;
                    dcMemberNewBal = (decimal)member.fldEWallet + (decimal)request.amount;
                    member.fldEWallet = dcMemberNewBal;

                    tblewallettransaction walletTrx = new tblewallettransaction();
                    walletTrx.fldMID = member.fldID;
                    walletTrx.fldType = strType;
                    walletTrx.fldReferenceID = strRef;
                    walletTrx.fldCreator = userId;
                    walletTrx.fldDateTime = DateTime.Now;
                    walletTrx.fldCreatorType = strCreator;
                    walletTrx.fldBF = dcMemberBf;
                    walletTrx.fldOut = 0;
                    walletTrx.fldIn = (decimal)request.amount;
                    walletTrx.fldCF = dcMemberNewBal;
                    walletTrx.fldRemark = "Topup Wallet: " + String.Format("{0:N}", request.amount) + ", Payment Type: " + request.payType;
                    if (!string.IsNullOrEmpty(request.remark))
                        walletTrx.fldRemark = walletTrx.fldRemark + ", " + request.remark;

                    entities.tblewallettransactions.Add(walletTrx);
                }
                #endregion

                #region SP(Sold Point)
                else if (request.walletType == "O")
                {
                    dcMemberBf = (decimal)member.fldOWallet;
                    dcMemberNewBal = (decimal)member.fldOWallet + (decimal)request.amount;
                    member.fldOWallet = dcMemberNewBal;

                    tblowallettransaction walletTrx = new tblowallettransaction();
                    walletTrx.fldMID = member.fldID;
                    walletTrx.fldType = strType;
                    walletTrx.fldReferenceID = strRef;
                    walletTrx.fldCreator = userId;
                    walletTrx.fldDateTime = DateTime.Now;
                    walletTrx.fldCreatorType = strCreator;
                    walletTrx.fldBF = dcMemberBf;
                    walletTrx.fldOut = 0;
                    walletTrx.fldIn = (decimal)request.amount;
                    walletTrx.fldCF = dcMemberNewBal;
                    walletTrx.fldRemark = "Topup Wallet: " + String.Format("{0:N}", request.amount) + ", Payment Type: " + request.payType;
                    if (!string.IsNullOrEmpty(request.remark))
                        walletTrx.fldRemark = walletTrx.fldRemark + ", " + request.remark;

                    entities.tblowallettransactions.Add(walletTrx);
                }
                #endregion

                #region MP(Mall Point)
                else if (request.walletType == "B")
                {
                    dcMemberBf = (decimal)member.fldBWallet;
                    dcMemberNewBal = (decimal)member.fldBWallet + (decimal)request.amount;
                    member.fldBWallet = dcMemberNewBal;

                    tblbwallettransaction walletTrx = new tblbwallettransaction();
                    walletTrx.fldMID = member.fldID;
                    walletTrx.fldType = strType;
                    walletTrx.fldReferenceID = strRef;
                    walletTrx.fldCreator = userId;
                    walletTrx.fldDateTime = DateTime.Now;
                    walletTrx.fldCreatorType = strCreator;
                    walletTrx.fldBF = dcMemberBf;
                    walletTrx.fldOut = 0;
                    walletTrx.fldIn = (decimal)request.amount;
                    walletTrx.fldCF = dcMemberNewBal;
                    walletTrx.fldRemark = "Topup Wallet: " + String.Format("{0:N}", request.amount) + ", Payment Type: " + request.payType;
                    if (!string.IsNullOrEmpty(request.remark))
                        walletTrx.fldRemark = walletTrx.fldRemark + ", " + request.remark;

                    entities.tblbwallettransactions.Add(walletTrx);
                }
                #endregion

                #region PP(Purchase Point)
                else if (request.walletType == "W")
                {
                    dcMemberBf = (decimal)member.fldWWallet;
                    dcMemberNewBal = (decimal)member.fldWWallet + (decimal)request.amount;
                    member.fldWWallet = dcMemberNewBal;

                    tblwwallettransaction walletTrx = new tblwwallettransaction();
                    walletTrx.fldMID = member.fldID;
                    walletTrx.fldType = strType;
                    walletTrx.fldReferenceID = strRef;
                    walletTrx.fldCreator = userId;
                    walletTrx.fldDateTime = DateTime.Now;
                    walletTrx.fldCreatorType = strCreator;
                    walletTrx.fldBF = dcMemberBf;
                    walletTrx.fldOut = 0;
                    walletTrx.fldIn = (decimal)request.amount;
                    walletTrx.fldCF = dcMemberNewBal;
                    walletTrx.fldRemark = "Topup Wallet: " + String.Format("{0:N}", request.amount) + ", Payment Type: " + request.payType;
                    if (!string.IsNullOrEmpty(request.remark))
                        walletTrx.fldRemark = walletTrx.fldRemark + ", " + request.remark;

                    entities.tblwwallettransactions.Add(walletTrx);
                }
                #endregion

                #region AP(Ads Point)
                else if (request.walletType == "A")
                {
                    dcMemberBf = (decimal)member.fldAWallet;
                    dcMemberNewBal = (decimal)member.fldAWallet + (decimal)request.amount;
                    member.fldAWallet = dcMemberNewBal;

                    tblawallettransaction walletTrx = new tblawallettransaction();
                    walletTrx.fldMID = member.fldID;
                    walletTrx.fldType = strType;
                    walletTrx.fldReferenceID = strRef;
                    walletTrx.fldCreator = userId;
                    walletTrx.fldDateTime = DateTime.Now;
                    walletTrx.fldCreatorType = strCreator;
                    walletTrx.fldBF = dcMemberBf;
                    walletTrx.fldIn = 0;
                    walletTrx.fldOut = (decimal)request.amount;
                    walletTrx.fldCF = dcMemberNewBal;
                    walletTrx.fldRemark = "Topup Wallet: " + String.Format("{0:N}", request.amount) + ", Payment Type: " + request.payType;
                    if (!string.IsNullOrEmpty(request.remark))
                        walletTrx.fldRemark = walletTrx.fldRemark + ", " + request.remark;

                    entities.tblawallettransactions.Add(walletTrx);
                }
                #endregion

                //update tblwallettopup
                tblwallettopup objTblwalletTopup = new tblwallettopup();
                objTblwalletTopup.fldMID = member.fldID;
                objTblwalletTopup.fldWallet = request.walletType;
                objTblwalletTopup.fldAmount = (decimal)request.amount;
                objTblwalletTopup.fldReference = strRef;
                objTblwalletTopup.fldDateTime = DateTime.Now;
                objTblwalletTopup.fldCreator = userId;
                objTblwalletTopup.fldCreatorType = strCreator;
                objTblwalletTopup.fldBankID = 0;
                objTblwalletTopup.fldPaymentMode = request.payType;
                objTblwalletTopup.fldActive = 0;

                entities.tblwallettopups.Add(objTblwalletTopup);
                entities.SaveChanges();
                return Ok();
            }
        }

        [Route("admin/adjust")]
        [HttpPost]
        public IHttpActionResult AdminAdjust([FromBody] WalletAdjustModel request)
        {
            long userId = GetUserId();
            string strRefPrefix = "AJ";
            string strType = "ADJUST";
            string strCreator = "A";

            if (!isAdmin)
                return BadRequest();

            if (!AdminAdjustValidate(request))
            {
                return BadRequest(ModelState);
            }

            decimal dcMemberBf = 0;
            decimal dcMemberNewBal;
            string strRef = FuncTool.GenerateRef(strRefPrefix);

            using (VmallDbEntities entities = new VmallDbEntities())
            {
                var member = entities.tblmemberships.SingleOrDefault(x => x.fldCode == request.memberId);
                decimal dcAmount = (decimal)request.amount;
                #region CP(Cash Point)
                if (request.walletType == "C")
                {
                    dcMemberBf = (decimal)member.fldCWallet;
                    dcMemberNewBal = (decimal)member.fldCWallet + dcAmount;
                    member.fldCWallet = dcMemberNewBal;

                    tblcwallettransaction walletTrx = new tblcwallettransaction();
                    walletTrx.fldMID = member.fldID;
                    walletTrx.fldType = strType;
                    walletTrx.fldReferenceID = strRef;
                    walletTrx.fldCreator = userId;
                    walletTrx.fldDateTime = DateTime.Now;
                    walletTrx.fldCreatorType = strCreator;
                    walletTrx.fldBF = dcMemberBf;
                    walletTrx.fldOut = dcAmount < 0? dcAmount * -1 : 0;
                    walletTrx.fldIn = dcAmount > 0 ? dcAmount : 0;
                    walletTrx.fldCF = dcMemberNewBal;
                    if (!string.IsNullOrEmpty(request.remark))
                        walletTrx.fldRemark = request.remark;

                    entities.tblcwallettransactions.Add(walletTrx);
                }
                #endregion

                #region EP(Register Point)
                else if (request.walletType == "E")
                {
                    dcMemberBf = (decimal)member.fldEWallet;
                    dcMemberNewBal = (decimal)member.fldEWallet + (decimal)request.amount;
                    member.fldEWallet = dcMemberNewBal;

                    tblewallettransaction walletTrx = new tblewallettransaction();
                    walletTrx.fldMID = member.fldID;
                    walletTrx.fldType = strType;
                    walletTrx.fldReferenceID = strRef;
                    walletTrx.fldCreator = userId;
                    walletTrx.fldDateTime = DateTime.Now;
                    walletTrx.fldCreatorType = strCreator;
                    walletTrx.fldBF = dcMemberBf;
                    walletTrx.fldOut = dcAmount < 0 ? dcAmount * -1 : 0;
                    walletTrx.fldIn = dcAmount > 0 ? dcAmount : 0;
                    walletTrx.fldCF = dcMemberNewBal;
                    if (!string.IsNullOrEmpty(request.remark))
                        walletTrx.fldRemark = request.remark;

                    entities.tblewallettransactions.Add(walletTrx);
                }
                #endregion

                #region SP(Sold Point)
                else if (request.walletType == "O")
                {
                    dcMemberBf = (decimal)member.fldOWallet;
                    dcMemberNewBal = (decimal)member.fldOWallet + (decimal)request.amount;
                    member.fldOWallet = dcMemberNewBal;

                    tblowallettransaction walletTrx = new tblowallettransaction();
                    walletTrx.fldMID = member.fldID;
                    walletTrx.fldType = strType;
                    walletTrx.fldReferenceID = strRef;
                    walletTrx.fldCreator = userId;
                    walletTrx.fldDateTime = DateTime.Now;
                    walletTrx.fldCreatorType = strCreator;
                    walletTrx.fldBF = dcMemberBf;
                    walletTrx.fldOut = dcAmount < 0 ? dcAmount * -1 : 0;
                    walletTrx.fldIn = dcAmount > 0 ? dcAmount : 0;
                    walletTrx.fldCF = dcMemberNewBal;
                    if (!string.IsNullOrEmpty(request.remark))
                        walletTrx.fldRemark = request.remark;

                    entities.tblowallettransactions.Add(walletTrx);
                }
                #endregion

                #region MP(Mall Point)
                else if (request.walletType == "B")
                {
                    dcMemberBf = (decimal)member.fldBWallet;
                    dcMemberNewBal = (decimal)member.fldBWallet + (decimal)request.amount;
                    member.fldBWallet = dcMemberNewBal;

                    tblbwallettransaction walletTrx = new tblbwallettransaction();
                    walletTrx.fldMID = member.fldID;
                    walletTrx.fldType = strType;
                    walletTrx.fldReferenceID = strRef;
                    walletTrx.fldCreator = userId;
                    walletTrx.fldDateTime = DateTime.Now;
                    walletTrx.fldCreatorType = strCreator;
                    walletTrx.fldBF = dcMemberBf;
                    walletTrx.fldOut = dcAmount < 0 ? dcAmount * -1 : 0;
                    walletTrx.fldIn = dcAmount > 0 ? dcAmount : 0;
                    walletTrx.fldCF = dcMemberNewBal;
                    if (!string.IsNullOrEmpty(request.remark))
                        walletTrx.fldRemark = request.remark;

                    entities.tblbwallettransactions.Add(walletTrx);
                }
                #endregion

                #region PP(Purchase Point)
                else if (request.walletType == "W")
                {
                    dcMemberBf = (decimal)member.fldWWallet;
                    dcMemberNewBal = (decimal)member.fldWWallet + (decimal)request.amount;
                    member.fldWWallet = dcMemberNewBal;

                    tblwwallettransaction walletTrx = new tblwwallettransaction();
                    walletTrx.fldMID = member.fldID;
                    walletTrx.fldType = strType;
                    walletTrx.fldReferenceID = strRef;
                    walletTrx.fldCreator = userId;
                    walletTrx.fldDateTime = DateTime.Now;
                    walletTrx.fldCreatorType = strCreator;
                    walletTrx.fldBF = dcMemberBf;
                    walletTrx.fldOut = dcAmount < 0 ? dcAmount * -1 : 0;
                    walletTrx.fldIn = dcAmount > 0 ? dcAmount : 0;
                    walletTrx.fldCF = dcMemberNewBal;
                    if (!string.IsNullOrEmpty(request.remark))
                        walletTrx.fldRemark = request.remark;

                    entities.tblwwallettransactions.Add(walletTrx);
                }
                #endregion

                #region AP(Ads Point)
                else if (request.walletType == "A")
                {
                    dcMemberBf = (decimal)member.fldAWallet;
                    dcMemberNewBal = (decimal)member.fldAWallet + (decimal)request.amount;
                    member.fldAWallet = dcMemberNewBal;

                    tblawallettransaction walletTrx = new tblawallettransaction();
                    walletTrx.fldMID = member.fldID;
                    walletTrx.fldType = strType;
                    walletTrx.fldReferenceID = strRef;
                    walletTrx.fldCreator = userId;
                    walletTrx.fldDateTime = DateTime.Now;
                    walletTrx.fldCreatorType = strCreator;
                    walletTrx.fldBF = dcMemberBf;
                    walletTrx.fldOut = dcAmount < 0 ? dcAmount * -1 : 0;
                    walletTrx.fldIn = dcAmount > 0 ? dcAmount : 0;
                    walletTrx.fldCF = dcMemberNewBal;
                    if (!string.IsNullOrEmpty(request.remark))
                        walletTrx.fldRemark = request.remark;

                    entities.tblawallettransactions.Add(walletTrx);
                }
                #endregion
                #region Tradable V-Coin
                else if (request.walletType == "S")
                {
                    dcMemberBf = (decimal)member.fldShare;
                    dcMemberNewBal = (decimal)member.fldShare + (decimal)request.amount;
                    member.fldShare = dcMemberNewBal;

                    tblsharetransaction walletTrx = new tblsharetransaction();
                    walletTrx.fldMID = member.fldID;
                    walletTrx.fldType = strType;
                    walletTrx.fldReferenceID = strRef;
                    walletTrx.fldCreator = userId;
                    walletTrx.fldDateTime = DateTime.Now;
                    walletTrx.fldCreatorType = strCreator;
                    walletTrx.fldBF = dcMemberBf;
                    walletTrx.fldOut = dcAmount < 0 ? dcAmount * -1 : 0;
                    walletTrx.fldIn = dcAmount > 0 ? dcAmount : 0;
                    walletTrx.fldCF = dcMemberNewBal;
                    if (!string.IsNullOrEmpty(request.remark))
                        walletTrx.fldRemark = request.remark;

                    entities.tblsharetransactions.Add(walletTrx);
                }
                #endregion
                #region Locked V-Coin
                else if (request.walletType == "L")
                {
                    dcMemberBf = (decimal)member.fldShare2;
                    dcMemberNewBal = (decimal)member.fldShare2 + (decimal)request.amount;
                    member.fldShare2 = dcMemberNewBal;

                    tblshare2transaction walletTrx = new tblshare2transaction();
                    walletTrx.fldMID = member.fldID;
                    walletTrx.fldType = strType;
                    walletTrx.fldReferenceID = strRef;
                    walletTrx.fldCreator = userId;
                    walletTrx.fldDateTime = DateTime.Now;
                    walletTrx.fldCreatorType = strCreator;
                    walletTrx.fldBF = dcMemberBf;
                    walletTrx.fldOut = dcAmount < 0 ? dcAmount * -1 : 0;
                    walletTrx.fldIn = dcAmount > 0 ? dcAmount : 0;
                    walletTrx.fldCF = dcMemberNewBal;
                    if (!string.IsNullOrEmpty(request.remark))
                        walletTrx.fldRemark = request.remark;

                    entities.tblshare2transaction.Add(walletTrx);
                }
                #endregion
                entities.SaveChanges();
                return Ok();
            }
        }

        [Route("admin/transferhistory")]
        [HttpPost]
        public IHttpActionResult AdminTransferHistoryList([FromBody] AdminWalletTransferSearchModel request)
        {
            long userId = GetUserId();
            DateTime dtFrom = DateTime.Now;
            DateTime dtTo = DateTime.Now;

            if (!isAdmin)
                return BadRequest();

            using (VmallDbEntities entities = new VmallDbEntities())
            {
                long strMemberId = 0;

                var member = entities.tblmemberships.SingleOrDefault(x => x.fldCode == request.memberId);

                if (member != null)
                    strMemberId = member.fldID;
                else
                {
                    ModelState.AddModelError("", "Member ID not found");
                    return BadRequest(ModelState);
                }

                var query = from wallet in entities.tblwallettransfers
                            join fromMember in entities.tblmemberships on wallet.fldFrMID equals fromMember.fldID
                            join toMember in entities.tblmemberships on wallet.fldToMID equals toMember.fldID
                            where wallet.fldFrMID == strMemberId
                            select new
                            {
                                wallet,
                                fromMember,
                                toMember
                            };

                if (request.fromDate != null && request.toDate != null)
                {
                    if (DateTime.TryParse(request.fromDate, out dtFrom) && DateTime.TryParse(request.toDate, out dtTo))
                    {
                        dtTo = dtTo.AddHours(23).AddMinutes(59).AddSeconds(59);
                    }
                }

                query = query.Where(p => p.wallet.fldDateTime >= dtFrom && p.wallet.fldDateTime <= dtTo);
                
                if (request.walletType != null)
                {
                    query = query.Where(p => p.wallet.fldFrWallet == request.walletType);
                }

                
                var queryOut = query.Select(p => new
                {
                    fldID = p.wallet.fldID,
                    fldFrMID = p.wallet.fldFrMID,
                    fldFrWallet = p.wallet.fldFrWallet,
                    fldToMID = p.wallet.fldToMID,
                    fldToWallet = p.wallet.fldToWallet,
                    fldAmount = p.wallet.fldAmount,
                    fldFees = p.wallet.fldFees,
                    fldReference = p.wallet.fldReference,
                    fldDateTime = p.wallet.fldDateTime,
                    fldCreator = p.wallet.fldCreator,
                    fldCreatorType = p.wallet.fldCreatorType,
                    fldFrCode = p.fromMember.fldCode,
                    fldToCode = p.toMember.fldCode,

                });

                queryOut = queryOut.OrderByDescending(p => p.fldDateTime);

                var result = CreatePageResult(request.pageSize, request.pageNumber, queryOut);

                return Ok(result);
            }
        }

        [Route("admin/withdrawhistory")]
        [HttpPost]
        public IHttpActionResult AdminWithdrawHistoryList([FromBody] WalletWithdrawSearchModel request)
        {
            long userId = GetUserId();

            if (!isAdmin)
                return BadRequest();

            //if(string.IsNullOrEmpty(request.memberId))
            //{
            //    ModelState.AddModelError("", "Member ID cannot be blank");
            //    return BadRequest(ModelState);
            //}

            //if (!isMemberExist(request.memberId))
            //{
            //    ModelState.AddModelError("", "Member ID not found");
            //    return BadRequest(ModelState);
            //}


            using (VmallDbEntities entities = new VmallDbEntities())
            {
                var user = entities.tblmemberships.SingleOrDefault(x => x.fldID == userId);



                var query = from wallet in entities.tblwalletwithdraws
                            from member in entities.tblmemberships.Where(p => p.fldID == wallet.fldMID)
                            from bank in entities.tblbanks.Where(p => p.fldID == member.fldBankID).DefaultIfEmpty()
                            where member.fldCode.Contains(request.memberId)
                            select new
                            {
                                fldID = wallet.fldID,
                                fldDateTime = wallet.fldDateTime,
                                fldMID = wallet.fldMID,
                                fldWallet = wallet.fldWallet,
                                fldBankID = wallet.fldBankID,
                                fldBankBranch = wallet.fldBankBranch,
                                fldAccHolder = wallet.fldAccHolder,
                                fldAccNo = wallet.fldAccNo,
                                fldAmount = wallet.fldAmount,
                                fldFees = wallet.fldFees,
                                fldReference = wallet.fldReference,
                                fldStatus = wallet.fldStatus,
                                fldCreator = wallet.fldCreator,
                                fldCreatorType = wallet.fldCreatorType,
                                fldProcessComment = wallet.fldProcessComment,
                                fldProcessBy = wallet.fldProcessBy,
                                fldProcessDateTime = wallet.fldProcessDateTime,
                                fldName = member.fldName,
                                fldICNo = member.fldICNo,
                                fldCode = member.fldCode,
                                fldBankName = bank.fldName

                            };

                if (request.fromDate != null && request.toDate != null)
                {
                    DateTime dtFrom;
                    DateTime dtTo;

                    if (DateTime.TryParse(request.fromDate, out dtFrom) && DateTime.TryParse(request.toDate, out dtTo))
                    {
                        dtTo = dtTo.AddHours(23).AddMinutes(59).AddSeconds(59);
                        query = query.Where(p => p.fldDateTime >= dtFrom && p.fldDateTime <= dtTo);
                    }
                }

                if (!string.IsNullOrEmpty(request.walletType))
                {
                    query = query.Where(p => p.fldWallet == request.walletType);
                }

                if (!string.IsNullOrEmpty(request.status))
                {
                    query = query.Where(p => p.fldStatus == request.status);
                }

                query = query.OrderByDescending(p => p.fldDateTime);

                var result = CreatePageResult(request.pageSize, request.pageNumber, query);

                return Ok(result);
            }
        }

        [Route("admin/tranhistory")]
        [HttpPost]
        public IHttpActionResult AdminTransactionHistoryList([FromBody] WalletTranHistSearchModel request)
        {
            long userId = GetUserId();

            if (!isAdmin)
                return BadRequest();

            if(string.IsNullOrEmpty(request.memberId))
            {
                ModelState.AddModelError("", "*Member ID cannot be blank");
                return BadRequest(ModelState);
            }

            DateTime dtFrom = DateTime.Now;
            DateTime dtTo = DateTime.Now;

            if (request.fromDate != null && request.toDate != null)
            {
                if (DateTime.TryParse(request.fromDate, out dtFrom) && DateTime.TryParse(request.toDate, out dtTo))
                {
                    dtTo = dtTo.AddHours(23).AddMinutes(59).AddSeconds(59);
                }
            }


            using (VmallDbEntities entities = new VmallDbEntities())
            {
                if (string.IsNullOrEmpty(request.memberId))
                {
                    ModelState.AddModelError("", "Member ID cannot be blank");
                    return BadRequest(ModelState);
                }

                var member = entities.tblmemberships.SingleOrDefault(x => x.fldCode == request.memberId);

                if (member == null)
                {
                    ModelState.AddModelError("", "Member ID not found");
                    return BadRequest(ModelState);
                }

                long lMemberId = member.fldID;

                switch (request.walletType.ToUpper())
                {
                    case "C":
                        var queryCP = from wallet in entities.tblcwallettransactions
                                      where wallet.fldMID == lMemberId && wallet.fldDateTime >= dtFrom && wallet.fldDateTime <= dtTo
                                      select wallet;
                        if (!string.IsNullOrEmpty(request.tranType))
                        {
                            queryCP = queryCP.Where(x => x.fldType.Equals(request.tranType, StringComparison.OrdinalIgnoreCase));
                        }
                        queryCP = queryCP.OrderByDescending(p => p.fldDateTime);

                        var resultCP = CreatePageResult(request.pageSize, request.pageNumber, queryCP);
                        return Ok(resultCP);
                        break;

                    case "E":
                        var queryEP = from wallet in entities.tblewallettransactions
                                      where wallet.fldMID == lMemberId && wallet.fldDateTime >= dtFrom && wallet.fldDateTime <= dtTo
                                      select wallet;
                        if (!string.IsNullOrEmpty(request.tranType))
                        {
                            queryEP = queryEP.Where(x => x.fldType.Equals(request.tranType, StringComparison.OrdinalIgnoreCase));
                        }
                        queryEP = queryEP.OrderByDescending(p => p.fldDateTime);

                        var resultEP = CreatePageResult(request.pageSize, request.pageNumber, queryEP);
                        return Ok(resultEP);
                        break;

                    case "B":
                        var queryMP = from wallet in entities.tblbwallettransactions
                                      where wallet.fldMID == lMemberId && wallet.fldDateTime >= dtFrom && wallet.fldDateTime <= dtTo
                                      select wallet;
                        if (!string.IsNullOrEmpty(request.tranType))
                        {
                            queryMP = queryMP.Where(x => x.fldType.Equals(request.tranType, StringComparison.OrdinalIgnoreCase));
                        }
                        queryMP = queryMP.OrderByDescending(p => p.fldDateTime);

                        var resultMP = CreatePageResult(request.pageSize, request.pageNumber, queryMP);
                        return Ok(resultMP);
                        break;

                    case "W":
                        var queryPP = from wallet in entities.tblwwallettransactions
                                      where wallet.fldMID == lMemberId && wallet.fldDateTime >= dtFrom && wallet.fldDateTime <= dtTo
                                      select wallet;
                        if (!string.IsNullOrEmpty(request.tranType))
                        {
                            queryPP = queryPP.Where(x => x.fldType.Equals(request.tranType, StringComparison.OrdinalIgnoreCase));
                        }
                        queryPP = queryPP.OrderByDescending(p => p.fldDateTime);

                        var resultPP = CreatePageResult(request.pageSize, request.pageNumber, queryPP);
                        return Ok(resultPP);
                        break;

                    case "O":
                        var querySP = from wallet in entities.tblowallettransactions
                                      where wallet.fldMID == lMemberId && wallet.fldDateTime >= dtFrom && wallet.fldDateTime <= dtTo
                                      select wallet;
                        if (!string.IsNullOrEmpty(request.tranType))
                        {
                            querySP = querySP.Where(x => x.fldType.Equals(request.tranType, StringComparison.OrdinalIgnoreCase));
                        }
                        querySP = querySP.OrderByDescending(p => p.fldDateTime);

                        var resultSP = CreatePageResult(request.pageSize, request.pageNumber, querySP);
                        return Ok(resultSP);
                        break;

                    case "A":
                        var queryAP = from wallet in entities.tblawallettransactions
                                      where wallet.fldMID == lMemberId && wallet.fldDateTime >= dtFrom && wallet.fldDateTime <= dtTo
                                      select wallet;
                        if (!string.IsNullOrEmpty(request.tranType))
                        {
                            queryAP = queryAP.Where(x => x.fldType.Equals(request.tranType, StringComparison.OrdinalIgnoreCase));
                        }
                        queryAP = queryAP.OrderByDescending(p => p.fldDateTime);

                        var resultAP = CreatePageResult(request.pageSize, request.pageNumber, queryAP);
                        return Ok(resultAP);
                        break;

                    case "S":
                        var queryVC = from wallet in entities.tblsharetransactions
                                      where wallet.fldMID == lMemberId && wallet.fldDateTime >= dtFrom && wallet.fldDateTime <= dtTo
                                      select wallet;
                        if (!string.IsNullOrEmpty(request.tranType))
                        {
                            queryVC = queryVC.Where(x => x.fldType.Equals(request.tranType, StringComparison.OrdinalIgnoreCase));
                        }
                        queryVC = queryVC.OrderByDescending(p => p.fldDateTime);

                        var resultVC = CreatePageResult(request.pageSize, request.pageNumber, queryVC);
                        return Ok(resultVC);
                        break;

                    case "L":
                        var queryLC = from wallet in entities.tblshare2transaction
                                      where wallet.fldMID == lMemberId && wallet.fldDateTime >= dtFrom && wallet.fldDateTime <= dtTo
                                      select wallet;
                        if (!string.IsNullOrEmpty(request.tranType))
                        {
                            queryLC = queryLC.Where(x => x.fldType.Equals(request.tranType, StringComparison.OrdinalIgnoreCase));
                        }
                        queryLC = queryLC.OrderByDescending(p => p.fldDateTime);

                        var resultLC = CreatePageResult(request.pageSize, request.pageNumber, queryLC);
                        return Ok(resultLC);
                        break;
                    default:
                        return Ok();
                        break;
                }


            }
        }

        [Route("admin/balance")]
        [HttpPost]
        public IHttpActionResult AdminBalanceList([FromBody]BalanceSearchModel request)
        {
            long userId = GetUserId();

            if (!isAdmin)
                return BadRequest();

            BalanceSearchValidator validator = new BalanceSearchValidator();
            if(!validator.ValidInput(request))
            {
                ModelState.AddModelError("", "Member ID or name or IC number cannot be blank");
                return BadRequest(ModelState);
            }

            using (VmallDbEntities entities = new VmallDbEntities())
            {
                var query = from m in entities.tblmemberships
                            select new
                            {
                                m.fldCode,
                                m.fldName,
                                m.fldICNo,
                                m.fldCWallet,
                                m.fldEWallet,
                                m.fldBWallet,
                                m.fldWWallet,
                                m.fldOWallet,
                                m.fldAWallet,
                                m.fldShare,
                                m.fldShare2
                            };

                if (!string.IsNullOrEmpty(request.idNumber))
                {
                    query = query.Where(x => x.fldICNo.Contains(request.idNumber));
                }

                if (!string.IsNullOrEmpty(request.name))
                {
                    query = query.Where(x => x.fldName.Contains(request.name));
                }

                if (!string.IsNullOrEmpty(request.memberId))
                {
                    query = query.Where(x => x.fldCode.Contains(request.memberId));
                }

                query = query.OrderBy(p => p.fldCode);

                var result = CreatePageResult(request.pageSize, request.pageNumber, query);
                return Ok(result);
            }
        }

        private bool ValidateTransfer(WalletTransferModel request)
        {
            bool valid = true;
            
            using (VmallDbEntities entities = new VmallDbEntities())
            {
                long userId = GetUserId();
                var user = entities.tblmemberships.SingleOrDefault(x => x.fldID == userId);
                var member = entities.tblmemberships.Where(x => x.fldCode == request.toMemberId).SingleOrDefault();

                
                if (member == null)
                {
                    ModelState.AddModelError("memberID", "Invalid member ID");
                    valid = false;
                }

                if (valid)
                {
                    if(userId == member.fldID)
                    {
                        ModelState.AddModelError("", "*Invalid transfer to member ID");
                        valid = false;
                    }
                }

                if (valid)
                {
                    if(request.transferType == "E")
                    {
                        if(user.fldEWallet < (decimal)request.amount)
                        {
                            ModelState.AddModelError("", "*Insufficient wallet balance");
                            valid = false;
                        }
                    }
                }
                if (valid)
                {
                    if (request.transferType == "C")
                    {
                        if (user.fldCWallet < (decimal)request.amount)
                        {
                            ModelState.AddModelError("", "*Insufficient wallet balance");
                            valid = false;
                        }
                    }
                }
                if (valid)
                {
                    if (request.transferType == "O")
                    {
                        if (user.fldOWallet < (decimal)request.amount)
                        {
                            ModelState.AddModelError("", "*Insufficient wallet balance");
                            valid = false;
                        }
                    }
                }

                if(valid)
                {
                    var minAmt = entities.tblsettings.SingleOrDefault(x => x.fldSetting == "WalletTransferFee"); //MinTransferAmount
                    decimal dmMinAmt = 0;
                    if (minAmt != null)
                    {
                        dmMinAmt = decimal.Parse(minAmt.fldValue);
                    }

                    if(request.amount < dmMinAmt)
                    {
                        ModelState.AddModelError("", "*Transfer amount cannot less then vAMOUNT".Replace("vAMOUNT", minAmt.fldValue));
                        valid = false;
                    }

                }
            }
            

            return valid;
        }

        private bool AdminTransferValidate(AdminWalletTransferModel request)
        {
            bool valid = true;

            using (VmallDbEntities entities = new VmallDbEntities())
            {
                var fromMember = entities.tblmemberships.SingleOrDefault(x => x.fldCode == request.toMemberId);


                if (fromMember == null)
                {
                    ModelState.AddModelError("memberID", "From Member ID not found");
                    valid = false;
                }

                
                if (valid)
                {
                    var toMember = entities.tblmemberships.SingleOrDefault(x => x.fldCode == request.toMemberId);


                    if (toMember == null)
                    {
                        ModelState.AddModelError("memberID", "To Member ID not found");
                        valid = false;
                    }
                }

                if (valid)
                {
                    if (request.fromWalletype == "E")
                    {
                        if (fromMember.fldEWallet < (decimal)request.amount)
                        {
                            ModelState.AddModelError("", "*Insufficient wallet balance");
                            valid = false;
                        }
                    }
                }
                if (valid)
                {
                    if (request.fromWalletype == "C")
                    {
                        if (fromMember.fldCWallet < (decimal)request.amount)
                        {
                            ModelState.AddModelError("", "*Insufficient wallet balance");
                            valid = false;
                        }
                    }
                }
                if (valid)
                {
                    if (request.fromWalletype == "O")
                    {
                        if (fromMember.fldOWallet < (decimal)request.amount)
                        {
                            ModelState.AddModelError("", "*Insufficient wallet balance");
                            valid = false;
                        }
                    }
                }

                if (valid)
                {
                    if (request.fromWalletype == "B")
                    {
                        if (fromMember.fldBWallet < (decimal)request.amount)
                        {
                            ModelState.AddModelError("", "*Insufficient wallet balance");
                            valid = false;
                        }
                    }
                }

                if (valid)
                {
                    if (request.fromWalletype == "W")
                    {
                        if (fromMember.fldWWallet < (decimal)request.amount)
                        {
                            ModelState.AddModelError("", "*Insufficient wallet balance");
                            valid = false;
                        }
                    }
                }

                if (valid)
                {
                    if (request.fromWalletype == "A")
                    {
                        if (fromMember.fldAWallet < (decimal)request.amount)
                        {
                            ModelState.AddModelError("", "*Insufficient wallet balance");
                            valid = false;
                        }
                    }
                }

                if (valid)
                {
                    var minAmt = entities.tblsettings.SingleOrDefault(x => x.fldSetting == "WalletTransferFee"); //MinTransferAmount
                    decimal dmMinAmt = 0;
                    if (minAmt != null)
                    {
                        dmMinAmt = decimal.Parse(minAmt.fldValue);
                    }

                    if (request.amount < dmMinAmt)
                    {
                        ModelState.AddModelError("", "Transfer amount cannot less then " + minAmt.fldValue);
                        valid = false;
                    }

                }
            }


            return valid;
        }

        private bool AdminTopUpValidate(WalletTopupModel request)
        {
            bool valid = true;

            if (!isMemberExist(request.memberId))
            {
                ModelState.AddModelError("", "Member ID not found");
                valid = false;
            }


            return valid;
        }

        private bool AdminAdjustValidate(WalletAdjustModel request)
        {
            bool valid = true;

            if (!isMemberExist(request.memberId))
            {
                ModelState.AddModelError("", "Member ID not found");
                valid = false;
            }

            return valid;
        }

        private bool isMemberExist(string memberId)
        {
            bool valid = true;

            using (VmallDbEntities entities = new VmallDbEntities())
            {
                var member = entities.tblmemberships.SingleOrDefault(x => x.fldCode == memberId);


                if (member == null)
                {
                    valid = false;
                }
            }
            return valid;
        }

        private string GetTrfType(string strType)
        {
            string strOut = "";
            //C - CP(Cash Point)
            //E - EP(Register Point)
            //B - MP(Mall Point)
            //W - PP(Purchase Point)
            //A - AP(Ads Point)
            //O - SP(Sold Point)

            if (strType == "C")
                strOut = "CP";
            else if (strType == "E")
                strOut = "EP";
            else if (strType == "B")
                strOut = "MP";
            else if (strType == "W")
                strOut = "PP";
            else if (strType == "A")
                strOut = "AP";
            else if (strType == "O")
                strOut = "SP";
            else 
                strOut = strType;

            return strOut;
        }

        private T GenerateTransferFromEntity<T>() where T : class, new()
        {
            return new T();
        } 
    }
}
