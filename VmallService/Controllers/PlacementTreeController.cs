﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using VmallDataAccess;
using VmallService.Base.Filters;
using VmallService.Controllers.Base;
using VmallService.ResponseModel;

namespace VmallService.Controllers
{
    [BasicAuthentication]
    [RoutePrefix("api/PlacementTree")]
    public class PlacementTreeController : BaseController
    {
        int iCount = 0;
        
        [HttpGet]
        [Route("search/{id=id}/{ac=ac}")]
        public IHttpActionResult Search(string id, int ac)
        {
            long userId = GetUserId();
            string strMsg = "";

            if (string.IsNullOrEmpty(id))
            {
                strMsg = "*Invalid placement ID";
            }

            if (strMsg == "" && ac == 0)
            {
                strMsg = "Invalid placement Account";
            }

            using (VmallDbEntities entities = new VmallDbEntities())
            {
                var member = entities.tblmemberships.SingleOrDefault(x => x.fldCode == id);


                if (strMsg == "" && member == null)
                {
                    strMsg = "Invalid Member ID";
                    
                }

                if(strMsg != "")
                {
                    ModelState.AddModelError("", strMsg);
                    return BadRequest(ModelState);
                }

                PlacementMatrixModel placement = new PlacementMatrixModel();

                var gpv = from b in entities.tblbinaryhists.Where(x => x.fldMID == member.fldID && x.fldPAc == ac)
                          group b by new { b.fldMID, b.fldPAc } into g
                          select new
                          {
                              sumGpv = g.Sum(x => x.fld1) + g.Sum(x => x.fld2)
                          };

                var binary = (from b in entities.tblbinaries.Where(x => x.fldMID == member.fldID && x.fldPAc == ac)
                             select b).FirstOrDefault();

                
                placement.totalGPV = gpv.SingleOrDefault().sumGpv;
                placement.totalBfLeftGPV = binary.fldBF1 ?? 0;
                placement.totalBfRightGPV = binary.fldBF2 ?? 0;
                placement.totalCurrentLeftGPV = binary.fld1;
                placement.totalCurrentRightGPV = binary.fld2;
                
                placement.tree = GetTree(id, ac, 0);
                //System.Diagnostics.Debug.WriteLine("count: " + iCount);
                return Ok(placement);
            }
        }

        private PlacementMatrixModel.Node GetTree(string id, int ac, int iLevel)
        {
            iCount++;
            int levelCount = iLevel;
               
            using (VmallDbEntities entities = new VmallDbEntities())
            {

                PlacementMatrixModel.Node parent = new PlacementMatrixModel.Node();
                var member = entities.tblmemberships.SingleOrDefault(x => x.fldCode == id);
                var root = (from u in entities.tblunittrees
                            from m in entities.tblmemberships.Where(x => x.fldID == u.fldMID)
                            where u.fldMID == member.fldID && u.fldAc == ac
                            select new
                            {
                                unitree = u,
                                member = m
                            }).SingleOrDefault();


                if (root != null)
                {
                    levelCount++;
                    parent.fldCode = root.member.fldCode;
                    parent.fldAc = root.unitree.fldAc;
                    parent.fldMid = root.unitree.fldMID;
                    parent.fldName = root.member.fldName;
                    parent.fldRank = root.unitree.fldRank ?? 0;
                    parent.fldSales = root.member.fldSales ?? 0;
                    parent.fldSponsorCode = root.member.fldSponsorCode;
                    parent.fldSponsorID = root.member.fldSponsorID ?? 0;
                    parent.fldStatus = root.unitree.fldStatus;
                    parent.fldPlacementAc = root.unitree.fldPlacementAc;
                    parent.fldPlacementGroup = root.unitree.fldPlacementGroup;
                    parent.fldPlacementMID = root.unitree.fldPlacementMID;


                    var child = (from u in entities.tblunittrees
                               from m in entities.tblmemberships.Where(x => x.fldID == u.fldMID)
                               where u.fldPlacementMID == root.member.fldID && u.fldPlacementAc == ac
                               orderby u.fldPlacementGroup
                               select new
                               {
                                   unitree = u,
                                   member = m
                               }).ToList();

                    if (levelCount < 5 && child != null)
                    {
                        foreach (var item in child)
                        {
                            PlacementMatrixModel.Node node = new PlacementMatrixModel.Node();

                            if (item.unitree.fldPlacementGroup == 1)
                            {   
                                parent.Left = GetTree(item.member.fldCode, item.unitree.fldAc, levelCount);
                            }
                            else
                            {
                                parent.Right = GetTree(item.member.fldCode, item.unitree.fldAc, levelCount) ;
                            }
                        }
                    }
                }

                return parent;
            }
        }
    }
}
