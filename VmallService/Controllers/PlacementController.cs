﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.SqlServer;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Http;
using VmallDataAccess;
using VmallService.Base.Filters;
using VmallService.CommonFunction;
using VmallService.Controllers.Base;
using VmallService.ResponseModel;

namespace VmallService.Controllers
{
    [BasicAuthentication]
    [RoutePrefix("api/placement")]
    public class PlacementController : BaseController
    {
        [HttpGet]
        [Route("")]
        public IHttpActionResult GetPlacement()
        {
            long userId = GetUserId();
            DateTime dtNow = DateTime.Now.AddMonths(-1);
            DataSet dsPlace = new DataSet("placement");

            //getPlacementCount
            DataTable dt = GetPlacementCount(userId, int.Parse(dtNow.ToString("yyyyMM")));
            dt.TableName = dtNow.ToString("yyyyMM");
            dsPlace.Tables.Add(dt);

            dtNow = DateTime.Now;
            dt = GetPlacementCount(userId, int.Parse(dtNow.ToString("yyyyMM")));
            dt.TableName = dtNow.ToString("yyyyMM");
            dsPlace.Tables.Add(dt);

            //getSponsorCount
            DataSet dsSponsor = new DataSet("sponsor");
            dtNow = DateTime.Now.AddMonths(-1);
            dt = GetSponsorCount(userId, int.Parse(dtNow.ToString("yyyyMM")));
            dt.TableName = dtNow.ToString("yyyyMM");
            dsSponsor.Tables.Add(dt);

            dtNow = DateTime.Now;
            dt = GetSponsorCount(userId, int.Parse(dtNow.ToString("yyyyMM")));
            dt.TableName = dtNow.ToString("yyyyMM");
            dsSponsor.Tables.Add(dt);

            List<DataSet> list = new List<DataSet>();
            list.Add(dsPlace);
            list.Add(dsSponsor);

            return Ok(list);
            
        }

        [HttpGet]
        [Route("matrix/{id=id}/{ac=ac}")]
        public IHttpActionResult Matrix(string id, int ac)
        {
            long userId = GetUserId();
            string strMsg = "";

            if (string.IsNullOrEmpty(id))
            {
                strMsg = "*Invalid placement ID";
            }

            if (strMsg == "" && ac == 0)
            {
                strMsg = "Invalid placement Account";
            }

            using (VmallDbEntities entities = new VmallDbEntities())
            {
                var member = entities.tblmemberships.SingleOrDefault(x => x.fldCode == id);


                if (strMsg == "" && member == null)
                {
                    strMsg = "*Invalid Member ID";

                }

                if (strMsg != "")
                {
                    ModelState.AddModelError("", strMsg);
                    return BadRequest(ModelState);
                }

                PlacementMatrixModel placement = new PlacementMatrixModel();

                var gpv = (from b in entities.tblbinaryhists.Where(x => x.fldMID == member.fldID && x.fldAc == ac)
                          group b by new { b.fldMID, b.fldPAc } into g
                          select new
                          {
                              sumGpv = g.Sum(x => x.fld1) + g.Sum(x => x.fld2)
                          }).SingleOrDefault();

                var binary = (from b in entities.tblbinaries.Where(x => x.fldMID == member.fldID && x.fldAc == ac)
                              select b).FirstOrDefault();

                

                if(gpv != null)
                    placement.totalGPV = gpv.sumGpv;
                if(binary != null)
                { 
                    placement.totalBfLeftGPV = binary.fldBF1 ?? 0;
                    placement.totalBfRightGPV = binary.fldBF2 ?? 0;
                    placement.totalCurrentLeftGPV = binary.fld1;
                    placement.totalCurrentRightGPV = binary.fld2;
                }

                placement.tree = GetTree(id, ac, 0);
                
                return Ok(placement);
            }
        }

        [HttpGet]
        [Route("sponsor/{id=id}")]
        public IHttpActionResult Sponsor(string id)
        {
            string strMsg = "";

            if (string.IsNullOrEmpty(id))
            {
                strMsg = "*Invalid placement ID";
            }


            using (VmallDbEntities entities = new VmallDbEntities())
            {
                entities.Database.Log = Console.Write;
                entities.Database.CommandTimeout = 180;
                var member = entities.tblmemberships.SingleOrDefault(x => x.fldCode == id);


                if (strMsg == "" && member == null)
                {
                    strMsg = "*Invalid Member ID";

                }

                if (strMsg != "")
                {
                    ModelState.AddModelError("", strMsg);
                    return BadRequest(ModelState);
                }

                SponsorModel sponsorModel = new SponsorModel();

                var grpSales = (from s in entities.tblgroupsales
                                where s.fldMID == member.fldID
                                group s by s.fldMID into g
                                select new
                                {
                                    sumSP = g.Sum(x => x.fldSP),
                                    sumGSP = g.Sum(x => x.fldGSP)
                                }).SingleOrDefault();

                //var sponsorTree = (from m in entities.tblmemberships
                //                   where m.fldSponsorID == member.fldID
                //                   select new
                //                   {
                //                       m.fldID,
                //                       m.fldName,
                //                       m.fldCode,
                //                       m.fldDateJoin,
                //                       m.fldSponsorID,
                //                       m.fldSales,
                //                       m.fldRank,
                //                       personalSales= (from d in entities.tblgroupsales.Where(x => x.fldMID == m.fldID)
                //                                       select d.fldSP).Sum() ?? 0,
                //                       groupSales = (from d in entities.tblgroupsales.Where(x => x.fldMID == m.fldID)
                //                                        select d.fldGSP).Sum() ?? 0,
                //                       organization = (from d in entities.tblmemberships.Where(x => x.fldSponsorID == m.fldID)
                //                                   select d.fldID).Count()
                //                   }).ToList();

                var sponsorTree = (from m in entities.tblmemberships
                                   join s in entities.tblgroupsales on m.fldID equals s.fldMID into gs
                                   where m.fldSponsorID == member.fldID
                                   //group grp by new { m.fldID, m.fldName, m.fldCode, m.fldDateJoin, m.fldSponsorID, m.fldSales, m.fldRank, grp.fldGSP, grp.fldSP } into gs
                                   select new
                                   {
                                       m.fldID,
                                       m.fldName,
                                       m.fldCode,
                                       m.fldDateJoin,
                                       m.fldSponsorID,
                                       m.fldSales,
                                       m.fldRank,
                                       personalSales = gs.Sum(x => x.fldSP) ?? 0,
                                       groupSales = gs.Sum(x => x.fldGSP) ?? 0,
                                       organization = (from d in entities.tblmemberships.Where(x => x.fldSponsorID == m.fldID)
                                                       select d.fldID).Count()
                                   }).ToList();


                sponsorModel.fldCode = member.fldCode;
                sponsorModel.fldName = member.fldName;
                sponsorModel.fldRank = member.fldRank ?? 0;
                sponsorModel.organization = 0;
                sponsorModel.fldSales = member.fldSales??0;
                sponsorModel.fldDateJoin = (DateTime)member.fldDateJoin;

                if (grpSales != null)
                {
                    //sponsorModel.personalSales = grpSales .Sum(x=>x.fldSP) ?? 0;
                    //sponsorModel.groupSales = grpSales.Sum(x => x.fldGSP) ?? 0;
                    sponsorModel.personalSales = grpSales.sumSP ?? 0;
                    sponsorModel.groupSales = grpSales.sumGSP ?? 0;

                }

                if (sponsorTree != null)
                {
                    sponsorModel.tree = sponsorTree;
                    sponsorModel.organization = sponsorTree.Count();
                }
                return Ok(sponsorModel);
            }
        }

        [HttpGet]
        [Route("validateSponsor/{sponsorId=sponsorId}/{memberId=memberId}")]
        public IHttpActionResult ValidateSponsor(string sponsorId, string memberId)
        {
            long userId = GetUserId();
            string strMsg = "";

            if (string.IsNullOrEmpty(sponsorId))
            {
                strMsg = "*Invalid sponsor ID";
            }

            if(strMsg == "")
            {
                if (string.IsNullOrEmpty(memberId))
                {
                    strMsg = "*Invalid member ID";
                }
            }

            if (strMsg != "")
            {
                ModelState.AddModelError("", strMsg);
                return BadRequest(ModelState);
            }

            using (VmallDbEntities entities = new VmallDbEntities())
            {
                var member = entities.tblmemberships.SingleOrDefault(x => x.fldCode == memberId);
                var sponsor = entities.tblmemberships.SingleOrDefault(x => x.fldCode == sponsorId);

                if (strMsg == "" && sponsor == null)
                {
                    strMsg = "*Invalid sponsor ID";

                }

                if (strMsg == "" && member == null)
                {
                    strMsg = "*Invalid Member ID";

                }

                if (strMsg != "")
                {
                    ModelState.AddModelError("", strMsg);
                    return BadRequest(ModelState);
                }

                
                bool isValid = false;



                //var query = entities.tblsponsorgroups.Where(x => x.fldMID == sponsor.fldID && x.fldDownlineMID == member.fldID);

                //if (query != null && query.Count() > 0)
                //    isValid = true;

                isValid = isValidSponsor(sponsor.fldID, member.fldID);

                return Ok(new { isValid = isValid });
            }
        }

        private bool isValidSponsor(long sponsorId, long memberId)
        {
            bool blnStatus = false;
            using (VmallDbEntities entities = new VmallDbEntities())
            {
                var member = entities.tblmemberships.SingleOrDefault(x => x.fldID == memberId);
                if(member != null)
                {
                    if (member.fldSponsorID != sponsorId)
                        blnStatus = isValidSponsor(sponsorId, (long)member.fldSponsorID);
                    else
                        blnStatus = true;
                }
            }

            return blnStatus;
        }

        [HttpGet]
        //[Route("validatePlacement/{sponsorId=sponsorId}/{placementId=placementId}/{placementAc=placementAc}")]
        [Route("validatePlacement/{uplineId=uplineId}/{uplineIdPlacementAc=uplineIdPlacementAc}/{memberId=memberId}/{memberPlacementAc=memberPlacementAc}")]
        public IHttpActionResult ValidatePlacement(string uplineId, int uplineIdPlacementAc, string memberId, int memberPlacementAc)
        {
            long userId = GetUserId();
            string strMsg = "";

            if (string.IsNullOrEmpty(uplineId))
            {
                strMsg = "*Invalid sponsor ID";
            }

            if (string.IsNullOrEmpty(memberId))
            {
                strMsg = "*Invalid placement ID";
            }


            if (strMsg == "")
            {
                if (uplineIdPlacementAc == 0 || memberPlacementAc == 0)
                {
                    strMsg = "*Placement ID cannot be blank";
                }
            }

            if (strMsg != "")
            {
                ModelState.AddModelError("", strMsg);
                return BadRequest(ModelState);
            }

            using (VmallDbEntities entities = new VmallDbEntities())
            {
                var placement = entities.tblmemberships.SingleOrDefault(x => x.fldCode == memberId);
                var sponsor = entities.tblmemberships.SingleOrDefault(x => x.fldCode == uplineId);

                if (strMsg == "" && sponsor == null)
                {
                    strMsg = "*Invalid sponsor ID";

                }

                if (strMsg == "" && placement == null)
                {
                    strMsg = "*Invalid placement ID";

                }

                if (strMsg != "")
                {
                    ModelState.AddModelError("", strMsg);
                    return BadRequest(ModelState);
                }


                bool isValid = false;



                //var query = entities.tblplacementgroups.Where(x => x.fldMID == sponsor.fldID && x.fldDownlineMID == placement.fldID && x.fldDownlineAc == placementAc);
                //var query = entities.tblplacementgroups.Where(x => x.fldMID == sponsor.fldID &&
                //                                              x.fldAc == uplineIdPlacementAc &&
                //                                              x.fldDownlineMID == placement.fldID && x.fldDownlineAc == memberPlacementAc);

                //if (query != null && query.Count() > 0)
                //    isValid = true;

                isValid = isValidPlacement(sponsor.fldID, uplineIdPlacementAc, placement.fldID, memberPlacementAc);
                return Ok(new { isValid = isValid });
            }
        }

        private bool isValidPlacement(long uplineId, int uplinePlacementAc, long memberId, int memberPlacementAc)
        {
            bool blnStatus = false;
            using (VmallDbEntities entities = new VmallDbEntities())
            {

                var member = entities.tblunittrees.SingleOrDefault(x => x.fldMID == memberId && x.fldAc == memberPlacementAc);

                while(member != null && member.fldPlacementMID != uplineId && member.fldPlacementAc != uplinePlacementAc && member.fldPlacementMID > 0)
                {
                    member = entities.tblunittrees.SingleOrDefault(x => x.fldMID == member.fldPlacementMID && x.fldAc == member.fldPlacementAc);
                }

                if (member == null || member.fldPlacementMID == 0)
                    return false;
                else
                    return true;

                //if (member != null)
                //{
                //    if (member.fldPlacementMID != uplineId && member.fldPlacementAc != uplinePlacementAc && member.fldPlacementMID > 0)
                //        blnStatus = isValidPlacement(uplineId, uplinePlacementAc, member.fldPlacementMID, member.fldPlacementAc);
                //    else
                //        blnStatus = true;

                //}
            }

            //return blnStatus;
        }

        [HttpGet]
        [Route("tree/{id=id}/{ac=ac}")]
        public IHttpActionResult Tree(string id, int ac)
        {
            long userId = GetUserId();
            string strMsg = "";

            if (string.IsNullOrEmpty(id))
            {
                strMsg = "*Invalid Member ID";
            }

            if (strMsg == "" && ac == 0)
            {
                strMsg = "Invalid placement Account";
            }

            using (VmallDbEntities entities = new VmallDbEntities())
            {
                var member = entities.tblmemberships.SingleOrDefault(x => x.fldCode == id);   

                if (strMsg == "" && member == null)
                {
                    strMsg = "*Invalid Member ID";

                }

                if (strMsg != "")
                {
                    ModelState.AddModelError("", strMsg);
                    return BadRequest(ModelState);
                }

                PlacementTreeModel treeModel = new PlacementTreeModel();

                var memberUnitree = (from unitree in entities.tblunittrees
                                    from m in entities.tblmemberships.Where(x => x.fldID == unitree.fldMID)
                                    where unitree.fldMID == member.fldID && unitree.fldAc == ac
                                    select new
                                    {
                                        unitree,
                                        m
                                    }).FirstOrDefault();

                var placeTree = (from u in entities.tblunittrees
                                 from m in entities.tblmemberships.Where(x=>x.fldID == u.fldMID)
                                   where u.fldPlacementMID == member.fldID && u.fldPlacementAc == ac
                                   select new
                                   {
                                       u.fldMID,
                                       u.fldAc,
                                       u.fldPlacementGroup,
                                       m.fldName,
                                       m.fldCode,
                                       m.fldDateJoin,
                                       m.fldSponsorID,
                                       m.fldSales,
                                       u.fldRank,
                                       organization = (from d in entities.tblunittrees.Where(x => x.fldPlacementMID == u.fldMID && x.fldPlacementAc == u.fldAc)
                                                       select d.fldMID).Count()
                                   }).ToList();

                treeModel.fldCode = member.fldCode;
                treeModel.fldName = member.fldName;
                treeModel.fldRank = memberUnitree.unitree.fldRank ?? 0;
                treeModel.organization = 0;
                treeModel.fldSales = member.fldSales ?? 0;
                treeModel.fldDateJoin = (DateTime)member.fldDateJoin;


                if (placeTree != null)
                {
                    treeModel.tree = placeTree;
                    treeModel.organization = placeTree.Count();
                }
                return Ok(treeModel);
            }
        }


        private PlacementMatrixModel.Node GetTree(string id, int ac, int iLevel)
        {
            int levelCount = iLevel;

            using (VmallDbEntities entities = new VmallDbEntities())
            {

                PlacementMatrixModel.Node parent = new PlacementMatrixModel.Node();
                var member = entities.tblmemberships.SingleOrDefault(x => x.fldCode == id);
                var root = (from u in entities.tblunittrees
                            from m in entities.tblmemberships.Where(x => x.fldID == u.fldMID)
                            where u.fldMID == member.fldID && u.fldAc == ac
                            select new
                            {
                                unitree = u,
                                member = m
                            }).SingleOrDefault();


                if (root != null)
                {
                    levelCount++;
                    parent.fldCode = root.member.fldCode;
                    parent.fldAc = root.unitree.fldAc;
                    parent.fldMid = root.unitree.fldMID;
                    parent.fldName = root.member.fldName;
                    parent.fldRank = root.unitree.fldRank ?? 0;
                    parent.fldSales = root.member.fldSales ?? 0;
                    parent.fldSponsorCode = root.member.fldSponsorCode;
                    parent.fldSponsorID = root.member.fldSponsorID ?? 0;
                    parent.fldStatus = root.unitree.fldStatus;
                    parent.fldPlacementAc = root.unitree.fldPlacementAc;
                    parent.fldPlacementGroup = root.unitree.fldPlacementGroup;
                    parent.fldPlacementMID = root.unitree.fldPlacementMID;


                    var child = (from u in entities.tblunittrees
                                 from m in entities.tblmemberships.Where(x => x.fldID == u.fldMID)
                                 where u.fldPlacementMID == root.member.fldID && u.fldPlacementAc == ac
                                 orderby u.fldPlacementGroup
                                 select new
                                 {
                                     unitree = u,
                                     member = m
                                 }).ToList();

                    if (levelCount < 5 && child != null)
                    {
                        foreach (var item in child)
                        {
                            PlacementMatrixModel.Node node = new PlacementMatrixModel.Node();

                            if (item.unitree.fldPlacementGroup == 1)
                            {
                                parent.Left = GetTree(item.member.fldCode, item.unitree.fldAc, levelCount);
                            }
                            else
                            {
                                parent.Right = GetTree(item.member.fldCode, item.unitree.fldAc, levelCount);
                            }
                        }
                    }
                }

                return parent;
            }
        }

        private DataTable GetPlacementCount(long id, int yearMonth)
        {
            StringBuilder sb = new StringBuilder();

            sb.AppendLine("SELECT T.fldPackageID, P.fldAc, COUNT(*) as count");
            sb.AppendLine("FROM tblplacementgroup AS P");
            sb.AppendLine("INNER JOIN tbltransaction AS T ON T.fldMID = P.fldDownlineMID");
            sb.AppendLine("INNER JOIN tblsponsorgroup AS S ON S.fldDownlineMID = T.fldMID");
            sb.AppendLine("WHERE P.fldMID = " + id);
            sb.AppendLine("AND P.fldAc > 1");
            sb.AppendLine("AND S.fldMID = " + id);
            sb.AppendLine("AND S.fldLevel >= 1");
            sb.AppendLine("AND DATE_FORMAT(T.fldDateTime, '%Y%m') = " + yearMonth);
            sb.AppendLine("AND P.fldDownlineAc = 1");
            sb.AppendLine("AND T.fldPackageID > 2");
            sb.AppendLine("group by T.fldPackageID, P.fldAc");

            using (VmallDbEntities entities = new VmallDbEntities())
            {
                return FuncTool.ExceuteSql(entities, sb.ToString());

            }

        }

        private DataTable GetSponsorCount(long id, int yearMonth)
        {
            StringBuilder sb = new StringBuilder();

            sb.AppendLine("SELECT t.fldPackageID, COUNT(*) as count");
            sb.AppendLine("FROM tbltransaction t");
            sb.AppendLine("LEFT JOIN tblmembership m ON m.fldID = t.fldMID");
            sb.AppendLine("WHERE m.fldSponsorID = " + id);
            sb.AppendLine("AND t.fldPackageID > 2");
            sb.AppendLine("AND DATE_FORMAT(fldDateTime,'%Y%m') = " + yearMonth);
            sb.AppendLine("group by t.fldPackageID");


            using (VmallDbEntities entities = new VmallDbEntities())
            {
                return FuncTool.ExceuteSql(entities, sb.ToString());

            }

        }

        //private object getPlacementCount(long id, int year, int month)
        //{
        //    using (VmallDbEntities entities = new VmallDbEntities())
        //    {
        //        var query = from p in entities.tblplacementgroups
        //                    from t in entities.tbltransactions.Where(x => x.fldMID == p.fldDownlineMID)
        //                    from s in entities.tblsponsorgroups.Where(x => x.fldDownlineMID == t.fldMID)
        //                    where p.fldMID == id && p.fldAc > 1 && s.fldMID == id && s.fldLevel >= 1
        //                          && p.fldDownlineAc == 1 && t.fldPackageID > 2
        //                          && t.fldDateTime.Value.Year == year && t.fldDateTime.Value.Month == month
        //                    group new { t.fldPackageID, p.fldAc } by new { t.fldPackageID, p.fldAc } into g
        //                    select new
        //                    {
        //                        g.Key.fldPackageID,
        //                        g.Key.fldAc,
        //                        count = g.Count()
        //                    };

        //        return query.ToList();
        //    }

        //}
    }

    
}
