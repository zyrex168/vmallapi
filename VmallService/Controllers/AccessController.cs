﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using VmallDataAccess;
using VmallService.Base.Filters;
using VmallService.Controllers.Base;
using VmallService.RequestModel;

namespace VmallService.Controllers
{
    [BasicAuthentication]
    [RoutePrefix("api/access")]
    public class AccessController : BaseController
    {
        [Route("admin/menu")]
        [HttpGet]
        public IHttpActionResult AdminMenuList()
        {
            using (VmallDbEntities entities = new VmallDbEntities())
            {
                dynamic result = new System.Dynamic.ExpandoObject();

                //get level
                var level = (from admin in entities.tbladmins
                             orderby admin.fldLevel descending
                             select new { admin.fldLevel }).FirstOrDefault();

                List<int> listLevel = new List<int>();

                int iLevel = (int)level.fldLevel;

                for (int i = 1; i <= iLevel; i++)
                {
                    listLevel.Add(i);
                }

                result.level = listLevel.ToArray();

                // menu
                string strLang = base.GetLanguage(Request.Headers);

                var adminMenu = from menu in entities.tbladminmenus
                                join lang in entities.tblmultilanguages on menu.fldMenuTitleText equals lang.fldText
                                into joined
                                from all in joined.DefaultIfEmpty()
                                where menu.fldDisplay == true && menu.fldStatus == true && menu.fldParentMenuID == 0
                                orderby menu.fldMenuOrder
                                select new
                                {
                                    menu.fldMenuID,
                                    menu_text = strLang == "EN-US" ? all.fldEN_US : all.fldZH_CN
                                };

                result.menu = adminMenu.ToList();

                return Ok(result);

            }
        }

        [Route("admin/menuitem/{level=level}/{menuid=menuid}")]
        [HttpGet]
        public IHttpActionResult AdminMenuItems(int level, int menuid)
        {
            string strLang = base.GetLanguage(Request.Headers);

            using (VmallDbEntities entities = new VmallDbEntities())
            {
                ///dynamic result = new System.Dynamic.ExpandoObject();

                var adminRole = from role in entities.tblrolemenus
                                where role.fldRoleID == level && role.fldUserType == "A"
                                select role;

                var query = from menu in entities.tbladminmenus
                            where menu.fldDisplay == true && menu.fldStatus == true
                            join role in adminRole on menu.fldMenuID equals role.fldMenuID
                            into menu_roles
                            from menu_role in menu_roles.DefaultIfEmpty()
                            join lang in entities.tblmultilanguages on menu.fldMenuTitleText equals lang.fldText
                            into joined
                            from all in joined.DefaultIfEmpty()
                            where menu.fldParentMenuID == menuid
                            orderby menu.fldParentMenuID, menu.fldMenuOrder
                            select new
                            {
                                menu.fldParentMenuID,
                                selected = ((long?)menu_role.fldRoleID ?? 0) == 0 ? false : true,
                                menu.fldMenuID,
                                menu.fldMenuOrder,
                                itemText = strLang == "EN-US" ? all.fldEN_US : all.fldZH_CN


                            };

                return Ok(query.ToList());
            }
        }

        [Route("admin/savemenuitem/{level=level}")]
        [HttpPost]
        public IHttpActionResult SaveAdminMenuItems([FromUri] int level, [FromBody] AdminAccessModel[] request)
        {
            bool blnStatus = SaveAccess(level, "A", request);
            return Ok();
        }

        [Route("member/menu")]
        [HttpGet]
        public IHttpActionResult MemberMenuList()
        {
            using (VmallDbEntities entities = new VmallDbEntities())
            {
                dynamic result = new System.Dynamic.ExpandoObject();
                string strLang = base.GetLanguage(Request.Headers);

                //get level
                var level = (from member in entities.tblmembertypes
                             join lang in entities.tblmultilanguages on member.fldRoleName equals lang.fldText
                             orderby member.fldRoleID descending
                             select new
                             {
                                 member.fldRoleID,
                                 menu_text = strLang == "EN-US" ? lang.fldEN_US : lang.fldZH_CN
                             }).ToList();


                result.memberType = level;

                // menu
                

                var memberMenu = from menu in entities.tblmembermenus
                                join lang in entities.tblmultilanguages on menu.fldMenuTitleText equals lang.fldText
                                into joined
                                from all in joined.DefaultIfEmpty()
                                where menu.fldDisplay == true && menu.fldStatus == true && menu.fldParentMenuID == 0
                                orderby menu.fldMenuOrder
                                select new
                                {
                                    menu.fldMenuID,
                                    menu_text = strLang == "EN-US" ? all.fldEN_US : all.fldZH_CN
                                };

                result.menu = memberMenu.ToList();

                return Ok(result);

            }
        }

        [Route("member/menuitem/{roleid=roleid}/{menuid=menuid}")]
        [HttpGet]
        public IHttpActionResult MemebrMenuItems(int roleid, int menuid)
        {
            string strLang = base.GetLanguage(Request.Headers);

            using (VmallDbEntities entities = new VmallDbEntities())
            {
                ///dynamic result = new System.Dynamic.ExpandoObject();

                var adminRole = from role in entities.tblrolemenus
                                where role.fldRoleID == roleid && role.fldUserType == "M"
                                select role;

                var query = from menu in entities.tblmembermenus
                            where menu.fldDisplay == true && menu.fldStatus == true
                            join role in adminRole on menu.fldMenuID equals role.fldMenuID
                            into menu_roles
                            from menu_role in menu_roles.DefaultIfEmpty()
                            join lang in entities.tblmultilanguages on menu.fldMenuTitleText equals lang.fldText
                            into joined
                            from all in joined.DefaultIfEmpty()
                            where menu.fldParentMenuID == menuid
                            orderby menu.fldParentMenuID, menu.fldMenuOrder
                            select new
                            {
                                menu.fldParentMenuID,
                                selected = ((long?)menu_role.fldRoleID ?? 0) == 0 ? false : true,
                                menu.fldMenuID,
                                menu.fldMenuOrder,
                                itemText = strLang == "EN-US" ? all.fldEN_US : all.fldZH_CN


                            };

                return Ok(query.ToList());
            }
        }

        [Route("member/savemenuitem/{roleid=roleid}")]
        [HttpPost]
        public IHttpActionResult SaveMemberMenuItems([FromUri] int roleid, [FromBody] AdminAccessModel[] request)
        {
            bool blnStatus = SaveAccess(roleid, "M", request);
            return Ok();
        }


        private bool SaveAccess(int level, string userType, AdminAccessModel[] request)
        {
            bool blnStatus = true;

            using (VmallDbEntities entities = new VmallDbEntities())
            {
                DbContextTransaction transaction = entities.Database.BeginTransaction();
                try
                {
                    foreach (var item in request)
                    {
                        tblrolemenu oldRole = entities.tblrolemenus.Where(x => x.fldRoleID == level && x.fldMenuID == item.fldMenuID && x.fldUserType == userType).SingleOrDefault();

                        if (oldRole != null)
                            entities.tblrolemenus.Remove(oldRole);

                        if (item.selected)
                        {
                            tblrolemenu newRole = new tblrolemenu();
                            newRole.fldRoleID = level;
                            newRole.fldMenuID = item.fldMenuID;
                            newRole.fldUserType = userType;
                            entities.tblrolemenus.Add(newRole);
                        }
                    }

                    entities.SaveChanges();

                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    blnStatus = false;
                    transaction.Rollback();
                    throw ex;
                }

                return blnStatus;
            }
        }
    }
}

//var r = from role in Tblrolemenus
//        where role.FldRoleID == 6 && role.FldUserType == "A"
//        select role;

//var user = from menu in Tbladminmenus
//           where menu.FldDisplay == 1 && menu.FldStatus == 1
//           join role in r on menu.FldMenuID equals role.FldMenuID
//           into menu_roles
//           from menu_role in menu_roles.DefaultIfEmpty()
//           join lang in Tblmultilanguages on menu.FldMenuTitleText equals lang.FldText
//           into joined
//           from all in joined.DefaultIfEmpty()
//           where menu.FldParentMenuID == 5
//           orderby menu.FldParentMenuID, menu.FldMenuOrder
//           select new
//           {
//               menu_role.FldRoleID,
//               menu.FldParentMenuID,
//               menu.FldMenuID,
//               menu.FldMenuOrder,
//               all.FldEN_US

//           };
//user.Dump();
