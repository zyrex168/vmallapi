﻿using FluentValidation.Results;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Web;
using System.Web.Http;
using VmallDataAccess;
using VmallService.Base;
using VmallService.Base.Filters;
using VmallService.CommonFunction;
using VmallService.Controllers.Base;
using VmallService.RequestModel;

namespace VmallService.Controllers
{
    
    [RoutePrefix("api/Profile")]
    public class ProfileController : BaseController
    {
        [Route("")]
        [HttpGet]
        [BasicAuthentication]
        public IHttpActionResult Get()
        {
            long userId = GetUserId(); // long.Parse(Thread.CurrentPrincipal.Identity.Name);
            using (VmallDbEntities entities = new VmallDbEntities())
            {

                var queryUser = entities.tblmemberships.SingleOrDefault(x => x.fldID == userId);

                //return Request.CreateResponse(HttpStatusCode.OK, queryUser);
                return Ok(queryUser);
            }
        }

        [Route("{id}")]
        [HttpGet]
        [BasicAuthentication]
        public IHttpActionResult Get(string id)
        {
            using (VmallDbEntities entities = new VmallDbEntities())
            {

                var queryUser = entities.tblmemberships.SingleOrDefault(x => x.fldCode == id);

                if (queryUser == null)
                    return NotFound();

                return Ok(queryUser);
            }
        }


        [Route("security")]
        [HttpPost]
        [BasicAuthentication]
        public IHttpActionResult ValidateSecurityPassword([FromBody]string fldTransactionPassword)
        {
            long userId = GetUserId();
            using (VmallDbEntities entities = new VmallDbEntities())
            {
                bool isValid = entities.tblmemberships.Any(user => user.fldID == userId
                                                    && user.fldTransactionPassword == fldTransactionPassword);


                if (isValid)
                {
                    return Ok();
                }
                else
                {
                    return Unauthorized();
                }
            }

        }

        [HttpPost]
        [BasicAuthentication]
        [Route("search")]
        public IHttpActionResult Search([FromBody] MemberSearchModel request)
        {
            long userId = GetUserId();
            using (VmallDbEntities entities = new VmallDbEntities())
            {
                var user = entities.tblmemberships.Where(p => p.fldID == userId).SingleOrDefault();

                var query = entities.tblmemberships.Where(p => p.fldStatus == "A" && p.fldActivate == "Y" && p.fldCountryID == user.fldCountryID);


                if (request.memberId != null && request.memberId != null)
                {
                    query = query.Where(p => p.fldCode.Contains(request.memberId));

                }

                if (request.icNumber != null && request.icNumber != null)
                {
                    query = query.Where(p => p.fldICNo.Contains(request.icNumber));

                }

                if (request.name != null && request.name != null)
                {
                    query = query.Where(p => p.fldName.Contains(request.name));

                }

                query = query.OrderBy(p => p.fldMemberCode);

                var result = CreatePageResult<VmallDataAccess.tblmembership>(request.pageSize, request.pageNumber, query);

                return Ok(result);
            }

        }

        [HttpPut]
        [BasicAuthentication]
        [Route("update")]
        public IHttpActionResult Update([FromBody] MemberInfoUpdateModel request)
        {
            long userId = GetUserId();
            using (VmallDbEntities entities = new VmallDbEntities())
            {
                var user = entities.tblmemberships.SingleOrDefault(x => x.fldID == userId);

                if (user == null)
                {
                    return NotFound();
                }
                else
                {
                    user.fldLanguage = request.fldLanguage;
                    user.fldName = request.fldName;
                    user.fldICNo = request.fldICNo;
                    user.fldEMail = request.fldEMail;
                    user.fldWeChatID = request.fldWeChatID;
                    user.fldMobileNo = request.fldMobileNo;
                    user.fldAddress1 = request.fldAddress1;
                    user.fldPostCode = request.fldPostCode;
                    user.fldCountryID = request.fldCountryID;

                    user.fldBRelationship = request.fldBRelationship;
                    user.fldBName = request.fldBName;
                    user.fldBIC = request.fldBIC;
                    user.fldBContactNo = request.fldBContactNo;
                    user.fldBAddress = request.fldBAddress;
                    user.fldBPostcode = request.fldBPostcode;
                    user.fldBCountryID = request.fldBCountryID;

                    entities.SaveChanges();

                    return Ok();
                }

            }
        }

        [HttpPut]
        [BasicAuthentication]
        [Route("ChangePassword")]
        public IHttpActionResult ChangePassword([FromBody] MemberPasswordModel request)
        {
            long userId = GetUserId();
            using (VmallDbEntities entities = new VmallDbEntities())
            {
                var user = entities.tblmemberships.SingleOrDefault(x => x.fldID == userId);

                if (user == null)
                {
                    return NotFound();
                }
                else
                {
                    if (user.fldPassword != request.oldPassword)
                    {
                        ModelState.AddModelError("", "*Incorrect current login password");
                        return BadRequest(ModelState);
                    }
                    else
                    {

                        user.fldPassword = request.newPassword;
                        entities.SaveChanges();

                        return Ok();
                    }
                }
            }
        }

        [HttpPut]
        [BasicAuthentication]
        [Route("ChangeSecurityPassword")]
        public IHttpActionResult ChangeSecurityPassword([FromBody] MemberPasswordModel request)
        {
            long userId = GetUserId();
            using (VmallDbEntities entities = new VmallDbEntities())
            {
                var user = entities.tblmemberships.SingleOrDefault(x => x.fldID == userId);

                if (user == null)
                {
                    return NotFound();
                }
                else
                {
                    if (user.fldTransactionPassword != request.oldPassword)
                    {
                        ModelState.AddModelError("", "*Incorrect current security password");
                        return BadRequest(ModelState);
                    }
                    else
                    {

                        user.fldTransactionPassword = request.newPassword;
                        entities.SaveChanges();

                        return Ok();
                    }
                }
            }
        }

        [HttpPut]
        [BasicAuthentication]
        [Route("UpdateBankInfo")]
        public IHttpActionResult UpdateBankInfo([FromBody] MemberBankInfoModel request)
        {
            long userId = GetUserId();
            using (VmallDbEntities entities = new VmallDbEntities())
            {
                var user = entities.tblmemberships.SingleOrDefault(x => x.fldID == userId);

                if (user == null)
                {
                    return NotFound();
                }
                else
                {

                    tblupdatebankhistory history = new tblupdatebankhistory();
                    history.fldMID = userId;
                    history.fldDateTime = DateTime.Now;
                    history.fldBankID = user.fldBankID;
                    history.fldBankCity = "";
                    history.fldBankState = "";
                    history.fldBankBranch = user.fldBankBranch;
                    history.fldAccHolder = user.fldAccHolder;
                    history.fldAccNo = user.fldAccNo;
                    history.fldNewBankID = request.fldBankID;
                    history.fldNewBankCity = "";
                    history.fldNewBankState = "";
                    history.fldNewBankBranch = request.fldBankBranch;
                    history.fldNewAccHolder = request.fldAccHolder;
                    history.fldNewAccNo = request.fldAccNo;
                    history.fldCreator = userId;
                    history.fldCreatorType = "M";
                    history.fldIPAddress = HttpContext.Current.Request.UserHostAddress;
                    history.fldProcessBy = 0;
                    history.fldProcessComment = "";
                    history.fldProcessDateTime = DateTime.Now;
                    history.fldStatus = "A";
                    entities.tblupdatebankhistories.Add(history);

                    user.fldAccNo = request.fldAccNo;
                    user.fldAccHolder = request.fldAccHolder;
                    user.fldBankID = request.fldBankID;
                    user.fldBankBranch = request.fldBankBranch;


                    entities.SaveChanges();

                    return Ok();

                }
            }
        }

        [AllowAnonymous]
        [Route("forgetpassword")]
        [HttpPost]
        public IHttpActionResult ForgetPassword([FromBody] ForgetPasswordModel request)
        {
            using (VmallDbEntities entities = new VmallDbEntities())
            {
                var user = entities.tblmemberships.SingleOrDefault(x => x.fldCode == request.fldCode);

                if (user == null)
                {
                    return NotFound();
                }

                string strUserEmail = user.fldEMail;

                if(request.fldEMail.ToUpper() != strUserEmail.ToUpper())
                {
                    ModelState.AddModelError("", "*Login ID and e-mail not match");
                    return BadRequest(ModelState);
                }
                else
                {
                    
                    string emailLogo = System.Configuration.ConfigurationManager.AppSettings.Get("EmailLogo").ToString();
                    string emailTemplate = System.Configuration.ConfigurationManager.AppSettings.Get("emailTemplate").ToString();
                    string loginUrl = System.Configuration.ConfigurationManager.AppSettings.Get("LoginUrl").ToString();
                    string companyName = System.Configuration.ConfigurationManager.AppSettings.Get("vCOMPANY").ToString();
                    string smtpServer = System.Configuration.ConfigurationManager.AppSettings.Get("SmtpServer").ToString();
                    string smtpPort = System.Configuration.ConfigurationManager.AppSettings.Get("SmtpPort").ToString();
                    string smtpUser = System.Configuration.ConfigurationManager.AppSettings.Get("SmtpUser").ToString();
                    string smtpPassword = System.Configuration.ConfigurationManager.AppSettings.Get("SmtpPassword").ToString();
                    string smtpSsl = System.Configuration.ConfigurationManager.AppSettings.Get("SmtpSsl").ToString();
                    string smtpFrom = System.Configuration.ConfigurationManager.AppSettings.Get("SmtpFrom").ToString();

                    var objbody = entities.tblmultilanguages.Where(x => x.fldText == "MailResetPasswordBody").SingleOrDefault();
                    var objfooter = entities.tblmultilanguages.Where(x => x.fldText == "MsgMailFootNote").SingleOrDefault();
                    var objSubject = entities.tblmultilanguages.Where(x => x.fldText == "MailResetPasswordSubject").SingleOrDefault();

                    if (objbody != null)
                    {
                        string emailBody = "";
                        string emailFooter = "";
                        string emailSubject = "Reset Password";
                        string strLang = "";
                        IEnumerable<string> headerLangValues;

                        if (Request.Headers.TryGetValues("X-Language", out headerLangValues))
                        {
                            strLang = headerLangValues.SingleOrDefault();
                        }

                        if(strLang == "")
                        {
                            strLang = user.fldLanguage;
                        }

                        if (strLang == "ZH-CN")
                        {
                            emailBody = objbody.fldZH_CN;
                        }
                        else
                        {
                            emailBody = objbody.fldEN_US;
                        }
                            
                        if(objfooter != null)
                        {
                            if (strLang == "ZH-CN")
                            {
                                emailFooter = objfooter.fldZH_CN;
                            }
                            else
                            {
                                emailFooter = objfooter.fldEN_US;
                            }
                        }

                        if(objSubject != null)
                        {
                            if (strLang == "ZH-CN")
                            {
                                emailSubject = objSubject.fldZH_CN;
                            }
                            else
                            {
                                emailSubject = objSubject.fldEN_US;
                            }
                        }

                        string path = Path.GetRandomFileName();
                        path = path.Replace(".", ""); // Remove period.
                        string strNewPassword = path.Substring(0, 6);  // Return 8 character string

                        emailBody = emailBody.Replace("vPASSWORD", strNewPassword);
                        emailBody = emailBody.Replace("vLOGINADD", loginUrl);
                        emailBody = emailBody.Replace("vCOMPANY", companyName);

                        emailTemplate = emailTemplate.Replace("{{img}}", emailLogo);
                        emailTemplate = emailTemplate.Replace("{{body}}", emailBody);
                        emailTemplate = emailTemplate.Replace("{{footer}}", emailFooter);

                        FuncTool.SendEmail(smtpServer, int.Parse(smtpPort), smtpUser, smtpPassword, bool.Parse(smtpSsl), smtpFrom, emailSubject, strUserEmail, emailTemplate);

                        user.fldPassword = strNewPassword;
                        entities.SaveChanges();
                    }
                }
                return Ok();
            }
        }

        [Route("admin/password")]
        [BasicAuthentication]
        [HttpPost]
        public IHttpActionResult AdminMemberPasswordList([FromBody] MemberSearchModel request)
        {
            long userId = GetUserId();

            if (!isAdmin)
                return BadRequest();

            MemberSearchValidator validator = new MemberSearchValidator();
            if (!validator.ValidInput(request))
            {
                ModelState.AddModelError("", "Member ID or name or IC number cannot be blank");
                return BadRequest(ModelState);
            }

            using (VmallDbEntities entities = new VmallDbEntities())
            {
                var query = from m in entities.tblmemberships
                            select new
                            {
                                m.fldCode,
                                m.fldName,
                                m.fldICNo,
                                m.fldPassword,
                                m.fldTransactionPassword
                            };

                if (!string.IsNullOrEmpty(request.icNumber))
                {
                    query = query.Where(x => x.fldICNo.Contains(request.icNumber));
                }

                if (!string.IsNullOrEmpty(request.name))
                {
                    query = query.Where(x => x.fldName.Contains(request.name));
                }

                if (!string.IsNullOrEmpty(request.memberId))
                {
                    query = query.Where(x => x.fldCode.Contains(request.memberId));
                }

                query = query.OrderBy(p => p.fldCode);

                var result = CreatePageResult(request.pageSize, request.pageNumber, query);
                return Ok(result);
            }
        }

        [HttpPut]
        [BasicAuthentication]
        [Route("admin/servicecenter")]
        public IHttpActionResult AdminUpdateServiceCenter([FromBody] MemberServiceCenterModel request)
        {
            long userId = GetUserId();

            if (!isAdmin)
                return BadRequest();

            using (VmallDbEntities entities = new VmallDbEntities())
            {
                var member = entities.tblmemberships.SingleOrDefault(x => x.fldCode == request.memberId);
                if (member != null)
                {
                    member.fldSCRank = (int)request.serviceCenter;
                    entities.SaveChanges();
                    return Ok();
                }
                else
                {
                    ModelState.AddModelError("", "Member ID not found");
                    return BadRequest(ModelState);
                }
            }
        }

        [HttpPut]
        [BasicAuthentication]
        [Route("admin/update")]
        public IHttpActionResult AdminUpdate([FromBody] MemberInfoUpdateModel request)
        {
            long userId = base.GetUserId();
            if (!isAdmin)
                return BadRequest();

            using (VmallDbEntities entities = new VmallDbEntities())
            {
                var user = entities.tblmemberships.SingleOrDefault(x => x.fldCode == request.memberId);

                if (user == null)
                {
                    ModelState.AddModelError("", "*Member ID not found");
                    return BadRequest(ModelState);
                }
                else
                {
                    user.fldLanguage = request.fldLanguage;
                    user.fldName = request.fldName;
                    user.fldICNo = request.fldICNo;
                    user.fldEMail = request.fldEMail;
                    user.fldWeChatID = request.fldWeChatID;
                    user.fldMobileNo = request.fldMobileNo;
                    user.fldAddress1 = request.fldAddress1;
                    user.fldPostCode = request.fldPostCode;
                    user.fldCountryID = request.fldCountryID;

                    user.fldBRelationship = request.fldBRelationship;
                    user.fldBName = request.fldBName;
                    user.fldBIC = request.fldBIC;
                    user.fldBContactNo = request.fldBContactNo;
                    user.fldBAddress = request.fldBAddress;
                    user.fldBPostcode = request.fldBPostcode;
                    user.fldBCountryID = request.fldBCountryID;

                    entities.SaveChanges();

                    return Ok();
                }

            }
        }

        [HttpPut]
        [BasicAuthentication]
        [Route("admin/updateBankInfo")]
        public IHttpActionResult AdminUpdateBankInfo([FromBody] MemberBankInfoModel request)
        {
            long userId = base.GetUserId();
            if (!isAdmin)
                return BadRequest();

            using (VmallDbEntities entities = new VmallDbEntities())
            {
                var user = entities.tblmemberships.SingleOrDefault(x => x.fldCode == request.memberId);

                if (user == null)
                {
                    ModelState.AddModelError("", "*Member ID not found");
                    return BadRequest(ModelState);
                }
                else
                {

                    tblupdatebankhistory history = new tblupdatebankhistory();
                    history.fldMID = userId;
                    history.fldDateTime = DateTime.Now;
                    history.fldBankID = user.fldBankID;
                    history.fldBankCity = "";
                    history.fldBankState = "";
                    history.fldBankBranch = user.fldBankBranch;
                    history.fldAccHolder = user.fldAccHolder;
                    history.fldAccNo = user.fldAccNo;
                    history.fldNewBankID = request.fldBankID;
                    history.fldNewBankCity = "";
                    history.fldNewBankState = "";
                    history.fldNewBankBranch = request.fldBankBranch;
                    history.fldNewAccHolder = request.fldAccHolder;
                    history.fldNewAccNo = request.fldAccNo;
                    history.fldCreator = userId;
                    history.fldCreatorType = "A";
                    history.fldIPAddress = HttpContext.Current.Request.UserHostAddress;
                    history.fldProcessBy = 0;
                    history.fldProcessComment = "";
                    history.fldProcessDateTime = DateTime.Now;
                    history.fldStatus = "A";
                    entities.tblupdatebankhistories.Add(history);

                    user.fldAccNo = request.fldAccNo;
                    user.fldAccHolder = request.fldAccHolder;
                    user.fldBankID = request.fldBankID;
                    user.fldBankBranch = request.fldBankBranch;


                    entities.SaveChanges();

                    return Ok();

                }
            }
        }

        [HttpPut]
        [Route("admin/ChangePassword")]
        public IHttpActionResult AdminChangePassword([FromBody] MemberPasswordModel request)
        {
            long userId = base.GetUserId();
            if (!isAdmin)
                return BadRequest();

            using (VmallDbEntities entities = new VmallDbEntities())
            {
                var user = entities.tblmemberships.SingleOrDefault(x => x.fldCode == request.memberId);

                if (user == null)
                {
                    ModelState.AddModelError("", "*Member ID not found");
                    return BadRequest(ModelState);
                }
                else
                {
                    user.fldPassword = request.newPassword;
                    entities.SaveChanges();

                    return Ok();
                }
            }
        }

        [HttpPut]
        [BasicAuthentication]
        [Route("admin/ChangeSecurityPassword")]
        public IHttpActionResult AdminChangeSecurityPassword([FromBody] MemberPasswordModel request)
        {
            long userId = base.GetUserId();
            if (!isAdmin)
                return BadRequest();
            using (VmallDbEntities entities = new VmallDbEntities())
            {
                var user = entities.tblmemberships.SingleOrDefault(x => x.fldCode == request.memberId);

                if (user == null)
                {
                    ModelState.AddModelError("", "*Member ID not found");
                    return BadRequest(ModelState);
                }
                else
                {
                    user.fldTransactionPassword = request.newPassword;
                    entities.SaveChanges();

                    return Ok();

                }
            }
        }

        [HttpPost]
        [BasicAuthentication]
        [Route("admin/memberlist")]
        public IHttpActionResult AdminMemberList([FromBody] MemberListModel request)
        {
            long userId = base.GetUserId();
            if (!isAdmin)
                return BadRequest();

            MemberLishValidator validator = new MemberLishValidator();

            if (!validator.ValidInput(request))
            {
                ModelState.AddModelError("", "Please enter value");
                return BadRequest(ModelState);
            }

            using (VmallDbEntities entities = new VmallDbEntities())
            {

                var query = from member in entities.tblmemberships
                            from sc in entities.tblservicecentertypes.Where(x => x.fldID == member.fldSCRank).DefaultIfEmpty()
                            select new
                            {
                                member,
                                sc
                            };

                if (!string.IsNullOrEmpty(request.memberId))
                {
                    query = query.Where(p => p.member.fldCode.StartsWith(request.memberId));

                }

                if (!string.IsNullOrEmpty(request.name))
                {
                    query = query.Where(p => p.member.fldName.Contains(request.name));

                }

                if (!string.IsNullOrEmpty(request.icNumber))
                {
                    query = query.Where(p => p.member.fldICNo.Contains(request.icNumber));

                }

                if (!string.IsNullOrEmpty(request.package))
                {
                    int value = int.Parse(request.package);
                    query = query.Where(p => p.member.fldRank == value);

                }

                if (!string.IsNullOrEmpty(request.serviceCenter))
                {
                    int value = int.Parse(request.serviceCenter);
                    if (value > -1)
                        query = query.Where(p => p.member.fldSCRank == value);
                    else
                        query = query.Where(p => p.member.fldSCRank > 0);

                }

                if (!string.IsNullOrEmpty(request.registerType))
                {
                    int value = int.Parse(request.registerType);
                    query = query.Where(p => p.member.fldSales == value);

                }

                if (!string.IsNullOrEmpty(request.status))
                {
                    query = query.Where(p => p.member.fldStatus == request.status);
                }

                query = query.OrderBy(p => p.member.fldCode);

                var queryOut = query.Select(x => new
                {
                    x.member.fldCode,
                    x.member.fldName,
                    x.member.fldICNo,
                    x.member.fldRank,
                    package = (
                        x.member.fldRank == 1 ? "Gold" :
                        x.member.fldRank == 2 ? "Platinum" :
                        x.member.fldRank == 3 ? "Diamond" :
                        x.member.fldRank == 4 ? "Crown" : ""
                    ),
                                        x.member.fldSCRank,
                                        serviceCenter = x.sc.fldName == null ? "None" : x.sc.fldName,
                                        x.member.fldSales,
                                        registerType = (
                        x.member.fldSales == 0 ? "False Account" :
                        x.member.fldSales == 1 ? "True Account" :
                        x.member.fldSales == 2 ? "Free Account" : ""
                    ),
                                        x.member.fldStatus,
                                        status = (
                        x.member.fldStatus == "A" ? "Active" :
                        x.member.fldStatus == "A" ? "InActive" : ""
                    )
                });

                var result = CreatePageResult(request.pageSize, request.pageNumber, queryOut);

                return Ok(result);
            }

        }

        [Route("admin/bankinfolist")]
        [HttpPost]
        [BasicAuthentication]
        public IHttpActionResult AdminWithdrawHistoryList([FromBody] BankInfoUpdateListModel request)
        {
            long userId = base.GetUserId();
            if (!isAdmin)
                return BadRequest();

            using (VmallDbEntities entities = new VmallDbEntities())
            {
                DateTime dtFrom = DateTime.Now;
                DateTime dtTo = DateTime.Now;

                if (request.fromDate != null && request.toDate != null)
                {
                    if (DateTime.TryParse(request.fromDate, out dtFrom) && DateTime.TryParse(request.toDate, out dtTo))
                    {
                        dtTo = dtTo.AddHours(23).AddMinutes(59).AddSeconds(59);

                    }
                }

                var query = from bankUpdInfo in entities.tblupdatebankhistories
                            from member in entities.tblmemberships.Where(x => x.fldID == bankUpdInfo.fldMID)
                            from oldBank in entities.tblbanks.Where(x=>x.fldID == bankUpdInfo.fldBankID).DefaultIfEmpty()
                            from newBank in entities.tblbanks.Where(x => x.fldID == bankUpdInfo.fldNewBankID).DefaultIfEmpty()
                            where bankUpdInfo.fldDateTime >= dtFrom && bankUpdInfo.fldDateTime <= dtTo
                            select new
                            {
                                bankUpdInfo,
                                member,
                                oldBank = oldBank.fldName ?? "",
                                newBank = newBank.fldName ?? ""
                            };

                if (!string.IsNullOrEmpty(request.memberId))
                {
                    query = query.Where(p => p.member.fldCode.StartsWith(request.memberId));
                }

                if (!string.IsNullOrEmpty(request.name))
                {
                    query = query.Where(p => p.member.fldName.Contains(request.name));
                }

                if (!string.IsNullOrEmpty(request.icNumber))
                {
                    query = query.Where(p => p.member.fldICNo.Contains(request.icNumber));
                }

                if (!string.IsNullOrEmpty(request.status))
                {
                    query = query.Where(p => p.bankUpdInfo.fldStatus == request.status);
                }

                query = query.OrderByDescending(p => p.bankUpdInfo.fldDateTime);

                var queryOut = query.Select(x => new
                {
                    x.bankUpdInfo.fldID,
                    x.bankUpdInfo.fldMID,
                    x.bankUpdInfo.fldDateTime,
                    x.bankUpdInfo.fldBankID,
                    oldBankName = x.oldBank,
                    x.bankUpdInfo.fldBankCity,
                    x.bankUpdInfo.fldBankState,
                    x.bankUpdInfo.fldBankBranch,
                    x.bankUpdInfo.fldAccHolder,
                    x.bankUpdInfo.fldAccNo,
                    x.bankUpdInfo.fldNewBankID,
                    newBankName = x.newBank,
                    x.bankUpdInfo.fldNewBankCity,
                    x.bankUpdInfo.fldNewBankState,
                    x.bankUpdInfo.fldNewBankBranch,
                    x.bankUpdInfo.fldNewAccHolder,
                    x.bankUpdInfo.fldNewAccNo,
                    x.bankUpdInfo.fldCreator,
                    x.bankUpdInfo.fldCreatorType,
                    x.bankUpdInfo.fldIPAddress,
                    x.bankUpdInfo.fldProcessBy,
                    x.bankUpdInfo.fldProcessComment,
                    x.bankUpdInfo.fldProcessDateTime,
                    fldStatus = x.bankUpdInfo.fldStatus == "A" ? "Approved" : x.bankUpdInfo.fldStatus == "R" ? "Rejected" : "Pending",
                    x.member.fldCode,
                    x.member.fldICNo,
                    x.member.fldName
                });

                var result = CreatePageResult(request.pageSize, request.pageNumber, queryOut);

                return Ok(result);
            }
        }

        [Route("admin/memberupline")]
        [HttpPost]
        [BasicAuthentication]
        public IHttpActionResult AdminMemberUpline([FromBody] MemberUplineSearchModel request)
        {
            long userId = base.GetUserId();
            if (!isAdmin)
                return BadRequest();

            using (VmallDbEntities entities = new VmallDbEntities())
            {
                
                var query = from tree in entities.tblunittrees
                            from member in entities.tblmemberships.Where(x => x.fldID == tree.fldMID)
                            from placeMember in entities.tblmemberships.Where(x => x.fldID == tree.fldPlacementMID)
                            where member.fldCode == request.memberId
                            orderby tree.fldPlacementGroup
                            select new
                            {
                                memberId = member.fldCode,
                                member.fldSponsorCode,
                                PlacementId = placeMember.fldCode,
                                tree.fldPlacementAc,
                                fldPlacementGroup = tree.fldPlacementGroup == 1 ? "Left" : tree.fldPlacementGroup == 2 ? "Right" : "Unknown"
                            };

                
                
                var result = CreatePageResult(request.pageSize, request.pageNumber, query);

                return Ok(result);
            }
        }

        
    }
}
