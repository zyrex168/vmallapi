﻿using System;
using System.Web.Http;
using Microsoft.Owin.Security.OAuth;
using Newtonsoft.Json.Serialization;
using System.Net.Http.Formatting;
using System.Net.Http.Headers;
using System.Web.Http.Cors;
using FluentValidation.WebApi;
using VmallService.Base.Handlers;
using VmallService.Base.Filters;
using System.Web.Http.ExceptionHandling;
using NLog;

namespace VmallService
{
    public static class WebApiConfig 
    {
        public static void Register(HttpConfiguration config)
        {
            // Web API configuration and services
            // Configure Web API to use only bearer token authentication.
            config.SuppressDefaultHostAuthentication();
            config.Filters.Add(new HostAuthenticationFilter(OAuthDefaults.AuthenticationType));

            // Web API routes
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );

            config.Formatters.Add(new CustomJsonFormatter());
            config.Formatters.JsonFormatter.SerializerSettings.Formatting = Newtonsoft.Json.Formatting.Indented;
            config.Formatters.JsonFormatter.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();

            EnableCorsAttribute cors = new EnableCorsAttribute("*", "*", "*");
            config.EnableCors(cors);

            config.Filters.Add(new ValidateModelStateFilter());
            

            //FluentValidation 
            FluentValidationModelValidatorProvider.Configure(config);

            //Wrapping The Response
            config.MessageHandlers.Add(new ResponseWrappingHandler());

            //logging
#if DEBUG
            LogManager.Configuration.Variables["basedir"] = @"c:\log\vmall-dev";
#else
            LogManager.Configuration.Variables["basedir"] = @"c:\log\vmall";
#endif

            config.MessageHandlers.Add(new LoggingHandler());


            config.Services.Replace(typeof(IExceptionHandler), new GlobalExceptionHandler());


            //config.Filters.Add(new BasicAuthenticationAttribute());
        }
    }

    public class CustomJsonFormatter : JsonMediaTypeFormatter
    {
        public CustomJsonFormatter()
        {
            this.SupportedMediaTypes.Add(new System.Net.Http.Headers.MediaTypeHeaderValue("text/html"));
        }

        public override void SetDefaultContentHeaders(Type type, HttpContentHeaders headers, MediaTypeHeaderValue mediaType)
        {
            base.SetDefaultContentHeaders(type, headers, mediaType);
            headers.ContentType = new MediaTypeHeaderValue("application/json");
        }

    }
}
