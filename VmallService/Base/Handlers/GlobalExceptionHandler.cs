﻿using MySql.Data.MySqlClient;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.ExceptionHandling;
using VmallService.CommonFunction;

namespace VmallService.Base.Handlers
{
    public class GlobalExceptionHandler : ExceptionHandler
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        public override void Handle(ExceptionHandlerContext context)
        {

            string errorMessage = "An unexpected error occured";

            var response = context.Request.CreateResponse(HttpStatusCode.InternalServerError,
            new
            {
                Message = context.Exception.Message
            });

            response.Headers.Add("X-Error", errorMessage);
            context.Result = new ErrorMessageResult(context.Request, response);

            
            logger.Error(context.Exception, context.Exception.Message);

            bool blnLogToDb = false;
            if(System.Configuration.ConfigurationManager.AppSettings["LogToDb"] != null)
            {
                if(System.Configuration.ConfigurationManager.AppSettings["LogToDb"].ToString() == "1")
                {
                    blnLogToDb = true;
                }
            }
            if (blnLogToDb)
            {
                string strUser = FuncTool.GetBasicAuthenUser(context.Request.Headers.Authorization.Parameter);
                LogManager.Configuration.Variables["user"].Text = strUser;

                Logger loggerDb = LogManager.GetLogger("databaseLogger");
                loggerDb.Error(context.Exception, context.Exception.Message);
            }

            

        }

        public override bool ShouldHandle(ExceptionHandlerContext context)
        {
            return true;
        }
    }

    public class ErrorMessageResult : IHttpActionResult
    {
        private HttpRequestMessage _request;
        private HttpResponseMessage _httpResponseMessage;


        public ErrorMessageResult(HttpRequestMessage request, HttpResponseMessage httpResponseMessage)
        {
            _request = request;
            _httpResponseMessage = httpResponseMessage;
        }

        public Task<HttpResponseMessage> ExecuteAsync(CancellationToken cancellationToken)
        {
            return Task.FromResult(_httpResponseMessage);
        }
    }
}