﻿using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web;

namespace VmallService.Base.Handlers
{
    public class LoggingHandler : DelegatingHandler
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        protected override async Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
        {
            
            // log request body

            DateTime dtStart = DateTime.Now;
            string requestBody = await request.Content.ReadAsStringAsync();

            requestBody = "[" + request.Method.ToString() + "]\r\n" + requestBody;

            string responseBody = "";

            var response = await base.SendAsync(request, cancellationToken);

            if (response.Content != null)
            {
                // once response body is ready, log it
                responseBody = await response.Content.ReadAsStringAsync();
            }

            responseBody = "[" + (int) response.StatusCode + " " + response.StatusCode.ToString() + "]\r\n" + responseBody;

            log(request.RequestUri.AbsoluteUri, requestBody, responseBody, dtStart);
            return response;
        }

        private void log(string url, string requestBody, string responseBody, DateTime dtStart)
        {
            DateTime dtEnd = DateTime.Now;

            TimeSpan ts = dtEnd.Subtract(dtStart);
            

            StringBuilder sb = new StringBuilder();
            sb.AppendLine("\r\n---------------------------------------------------------------------------------");
            sb.AppendLine("URL: " + url);
            sb.AppendLine("Process Time: " + ts.TotalSeconds + " seconds");
            sb.AppendLine("===REQUEST===");
            sb.AppendLine(requestBody);
            sb.AppendLine("");
            sb.AppendLine("===RESPONSE===");
            sb.AppendLine(responseBody);
            logger.Info(sb.ToString());
        }
    }
}