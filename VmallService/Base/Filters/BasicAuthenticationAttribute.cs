﻿using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Security.Principal;
using System.Text;
using System.Threading;
using System.Web;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;
using VmallDataAccess;
using VmallService.CommonFunction;
using VmallService.Controllers.Base;

namespace VmallService.Base.Filters
{
    public class BasicAuthenticationAttribute: AuthorizationFilterAttribute
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        public override void OnAuthorization(HttpActionContext actionContext)
        {
            if (actionContext.Request.Headers.Authorization == null)
            {
                actionContext.Response = actionContext.Request.CreateResponse(System.Net.HttpStatusCode.Unauthorized);
            }
            else
            {
                try
                {
                    string authenticationToken = actionContext.Request.Headers.Authorization.Parameter;
                    string decodeToken = Encoding.UTF8.GetString(Convert.FromBase64String(authenticationToken));
                    string[] usernamePassword = decodeToken.Split(':');

                    string username = usernamePassword[0];
                    string password = usernamePassword[1];
                    string memberId = "";
                    bool isAdmin = BaseController.GetIsAdmin(actionContext.Request.Headers);

                    if (isValidUser(username, password, out memberId, isAdmin))
                    {
                        Thread.CurrentPrincipal = new GenericPrincipal(new GenericIdentity(username), null);

                        //FuncTool.SetThreadLanguage(language);
                    }
                    else
                    {
                        actionContext.Response = actionContext.Request.CreateResponse(System.Net.HttpStatusCode.Unauthorized);
                    }
                }
                catch(Exception ex)
                {
                    actionContext.Response = actionContext.Request.CreateResponse(System.Net.HttpStatusCode.Unauthorized);
                    logger.Error(ex, ex.Message);
                }
            }
        }

        private bool isValidUser(string userId, string password, out string memberId, bool isAdmin)
        {
            bool isValid = false;
            memberId = "";
            using (VmallDbEntities entities = new VmallDbEntities())
            {
                //member
                //isValid = entities.tblmemberships.Any(user => user.fldCode.Equals(userId, StringComparison.OrdinalIgnoreCase)
                //                                    && user.fldPassword == password
                //                                    && user.fldActivate == "Y"
                //                                    && user.fldStatus == "A");

                if(isAdmin)
                {
                    var admin = entities.tbladmins.SingleOrDefault(x => x.fldCode.Equals(userId, StringComparison.OrdinalIgnoreCase));

                    if (admin != null)
                    {
                        if (admin.fldPassword == password && admin.fldStatus == "A")
                        {
                            isValid = true;
                            memberId = admin.fldID.ToString();
                        }

                        admin.fldLastLoginAttempt = DateTime.Now;

                        if (admin.fldPassword != password)
                        {
                            admin.fldLoginAttempt = admin.fldLoginAttempt + 1;
                        }
                        else
                        {
                            admin.fldLoginAttempt = 0;
                        }
                    }
                }
                else
                {
                    var user = entities.tblmemberships.SingleOrDefault(x => x.fldCode == userId);


                    if (user.fldPassword == password && user.fldActivate == "Y" && user.fldStatus == "A")
                    {
                        isValid = true;
                        memberId = user.fldID.ToString();
                    }

                    user.fldLastLoginAttempt = DateTime.Now;

                    if (user.fldPassword != password)
                    {
                        user.fldLoginAttempt = user.fldLoginAttempt + 1;
                    }
                    else
                    {
                        user.fldLoginAttempt = 0;
                    }
                    entities.SaveChanges();
                    return isValid;

                }

                entities.SaveChanges();
            }
                

            return isValid;
        }

        
    }
}