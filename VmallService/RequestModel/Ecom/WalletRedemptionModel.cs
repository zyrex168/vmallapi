﻿using FluentValidation;
using FluentValidation.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VmallService.RequestModel.Ecom
{
    [Validator(typeof(WalletRedemptionValidator))]
    public class WalletRedemptionModel
    {
        public string referenceId { get; set; }
        public decimal walletAmt { get; set; }
    }

    public class WalletRedemptionValidator : AbstractValidator<WalletRedemptionModel>
    {
        public WalletRedemptionValidator()
        {
            RuleFor(x => x.referenceId).NotEmpty().WithMessage("Reference cannot be blank");
            RuleFor(x => x.walletAmt).NotNull().WithMessage("Wallet amount cannot be blank or zero")
                .GreaterThan(0).WithMessage("Wallet amount cannot be blank or zero");
        }
    }
}