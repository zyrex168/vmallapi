﻿using FluentValidation;
using FluentValidation.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VmallService.RequestModel
{
    [Validator(typeof(BonusSearchValidator))]
    public class BonusSearchModel
    {
        public int pageNumber { get; set; }
        public int pageSize { get; set; }
        public string fromDate { get; set; }
        public string toDate { get; set; }
        public string memberId { get; set; }
        public int bonusType { get; set; }

        /*
         * 2 - Pairing Bonus/宣传奖金
            1 - Matching Bonus/互助奖金
            5- Unilevel Bonus/调度奖金
            3 - Leadership Bonus/贡献奖金
            6 - Sponsor Reward/推荐奖励
         */
    }

    public class BonusSearchValidator : AbstractValidator<BonusSearchModel>
    {
        public BonusSearchValidator()
        {
            RuleFor(x => x.memberId).NotEmpty().When(x => x.memberId != null).WithMessage("*Member ID cannot be blank");
        }
    }
}