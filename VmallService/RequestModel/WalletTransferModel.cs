﻿using FluentValidation;
using FluentValidation.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VmallService.RequestModel
{
    [Validator(typeof(WalletTransferValidator))]
    public class WalletTransferModel
    {
        public string transferType { get; set; }
        public string toMemberId { get; set; }
        public decimal amount { get; set; }
    }

    public class WalletTransferValidator : AbstractValidator<WalletTransferModel>
    {
        public WalletTransferValidator()
        {
            List<string> trfType = new List<string> { "R", "C", "E", "W", "S", "L", "A", "B", "O" };

            RuleFor(x => x.transferType).Must(x=>trfType.Contains(x)).WithMessage("Invalid Type");
            RuleFor(x => x.toMemberId).NotEmpty().WithMessage("Member ID cannot be blank");
            RuleFor(x => x.amount).GreaterThan(0).WithMessage("Transfer amount cannot be blank");
        }
    }

    [Validator(typeof(AdminWalletTransferValidator))]
    public class AdminWalletTransferModel
    {
        public string fromMemberId { get; set; }
        public string toMemberId { get; set; }
        public string fromWalletype { get; set; }
        public string toWalletType { get; set; }
        public decimal amount { get; set; }
        public string remark { get; set; }
    }

    public class AdminWalletTransferValidator : AbstractValidator<AdminWalletTransferModel>
    {
        public AdminWalletTransferValidator()
        {
            //C - CP(Cash Point)
            //E - EP(Register Point)
            //B - MP(Mall Point)
            //W - PP(Purchase Point)
            //A - AP(Ads Point)
            //O - SP(Sold Point)
            //S- Tradable V-Coin
            //L - Locked V-Coin

            List<string> trfType = new List<string> { "R", "C", "E", "W", "S", "L", "A", "B", "O" };

            RuleFor(x => x.fromWalletype).Must(x => trfType.Contains(x)).WithMessage("Invalid From Wallet Type");
            RuleFor(x => x.toWalletType).Must(x => trfType.Contains(x)).WithMessage("Invalid From Wallet Type");
            RuleFor(x => x.toMemberId).NotEmpty().WithMessage("Member ID cannot be blank");
            RuleFor(x => x.toMemberId).NotEmpty().WithMessage("Member ID cannot be blank");
            RuleFor(x => x.amount).GreaterThan(0).WithMessage("Transfer amount cannot be blank");
        }
    }

}