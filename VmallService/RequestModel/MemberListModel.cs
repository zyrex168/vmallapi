﻿using FluentValidation;
using FluentValidation.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using VmallDataAccess;

namespace VmallService.RequestModel
{
    [Validator(typeof(MemberLishValidator))]
    public class MemberListModel
    {
        public int pageNumber { get; set; }
        public int pageSize { get; set; }
        public string memberId { get; set; }
        public string name { get; set; }
        public string icNumber { get; set; }
        public string package { get; set; } //fldRank. 1=Gold, 2=Platinum, 3=Diamond, 4=Crown, blank=All
        public string serviceCenter { get; set; } //fldSCRank blank=All, 0=none, -1=All center, 1 to 9
        public string registerType { get; set; } //fldSales. blank=All, 0=Free account, 1=True account, 2=False account
        public string status { get; set; } //fldActivate. blank=All, A=Active, D=Inactive

    }

    public class MemberLishValidator : AbstractValidator<MemberListModel>
    {
        public MemberLishValidator()
        {
            List<string> registerTypeList = new List<string> { "0", "1", "2" };
            List<string> statusList = new List<string> {"", "A", "D" };

            When(x => x.registerType != null && x.registerType != "", () => {
                RuleFor(x => x.registerType)
                .Must(x => registerTypeList.Contains(x))
                .WithMessage("Invalid Register Type");
            });

            RuleFor(x => x.memberId).NotNull().WithMessage("Member ID cannot null");
            RuleFor(x => x.name).NotNull().WithMessage("Name cannot null");
            RuleFor(x => x.icNumber).NotNull().WithMessage("IC cannot null");
            RuleFor(x => x.status).NotNull().WithMessage("Status cannot null");
            RuleFor(x => x.package).NotNull().WithMessage("Package cannot null");
            RuleFor(x => x.serviceCenter).NotNull().WithMessage("Service Center cannot null");

            When(x => x.status != null && x.status != "", ()=> {
                RuleFor(x => x.status).Must(x => statusList.Contains(x)).WithMessage("Invalid Status");
            });

            When(x => x.package != null && x.package != "", () => {
                RuleFor(x => x.package).Must(isValidPakage).WithMessage("Invalid Package Type");
            });

            When(x => x.serviceCenter != null && x.serviceCenter != "", () => {
                RuleFor(x => x.serviceCenter).Must(isValidCenter).WithMessage("Invalid Service Center Type");
            });

            
        }

        public bool isValidPakage(string id)
        {
            bool blnResult = true;
            int value;

            if (string.IsNullOrEmpty(id))
                return false;

            if (int.TryParse(id,out value))
            {
                List<int> lstPackage = null;
                using (VmallDbEntities entities = new VmallDbEntities())
                {
                    var package = entities.tblpackages.Select(x => x.fldID).ToArray();
                    lstPackage = package.ToList();
                }

                if (!lstPackage.Contains(int.Parse(id)))
                    blnResult = false;
            }
            else
            {
                blnResult = false;

            }
            return blnResult;

        }

        public bool isValidCenter(string id)
        {
            bool blnResult = true;
            int value;

            if (string.IsNullOrEmpty(id))
                return false;

            if (int.TryParse(id, out value))
            {
                if (int.Parse(id) > 0)
                {
                    List<long> lstSc = null;
                    using (VmallDbEntities entities = new VmallDbEntities())
                    {
                        var serviceCenter = entities.tblservicecentertypes.Select(x => x.fldID).ToArray();
                        lstSc = serviceCenter.ToList();
                    }

                    if (!lstSc.Contains(int.Parse(id)))
                        blnResult = false;
                }

            }
            else
            {
                blnResult = false;

            }
            return blnResult;

        }

        public bool ValidInput(MemberListModel input)
        {
            bool isValid = true;

            if (string.IsNullOrEmpty(input.icNumber) &&
               string.IsNullOrEmpty(input.memberId) &&
               string.IsNullOrEmpty(input.name) &&
               string.IsNullOrEmpty(input.package) &&
               string.IsNullOrEmpty(input.serviceCenter) &&
               string.IsNullOrEmpty(input.registerType) &&
               string.IsNullOrEmpty(input.status)
              )

            {
                isValid = false;
            }
            

            return isValid;
        }
    }
}