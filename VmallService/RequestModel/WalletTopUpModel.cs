﻿using FluentValidation;
using FluentValidation.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VmallService.RequestModel
{
    [Validator(typeof(WalletModelValidator))]
    public class WalletTopupModel
    {
        public string memberId { get; set; }
        public string walletType { get; set; }
        public decimal amount { get; set; }
        public string payType { get; set; }
        public string remark { get; set; }

    }

    public class WalletModelValidator : AbstractValidator<WalletTopupModel>
    {
        public WalletModelValidator()
        {
            List<string> walletTypeList = new List<string> { "R", "C", "E", "W", "S", "L", "A", "B", "O" };
            List<string> payTypeList = new List<string> { "CASH", "CREDITCARD", "CHEQUE"};

            RuleFor(x => x.walletType).Must(x => walletTypeList.Contains(x)).WithMessage("Invalid Wallet Type");
            RuleFor(x => x.payType).Must(x => payTypeList.Contains(x)).WithMessage("Invalid Payment Type");

            RuleFor(x => x.amount).GreaterThan(0).WithMessage("Amount cannot be zero");

        }
    }
}