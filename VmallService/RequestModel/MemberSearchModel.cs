﻿using FluentValidation;
using FluentValidation.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VmallService.RequestModel
{
    [Validator(typeof(MemberSearchValidator))]
    public class MemberSearchModel
    {
        public int pageNumber { get; set; }
        public int pageSize { get; set; }
        public string memberId { get; set; }
        public string name { get; set; }
        public string icNumber { get; set; }
    }

    public class MemberSearchValidator : AbstractValidator<MemberSearchModel>
    {
        public MemberSearchValidator()
        {
            RuleFor(x => x.name).NotNull().WithMessage("Name cannot null");
            RuleFor(x => x.memberId).NotNull().WithMessage("Member ID cannot null");
            RuleFor(x => x.icNumber).NotNull().WithMessage("IC Number cannot null");

        }

        public bool ValidInput(MemberSearchModel input)
        {
            bool isValid = true;

            if (string.IsNullOrEmpty(input.icNumber) &&
               string.IsNullOrEmpty(input.memberId) &&
               string.IsNullOrEmpty(input.name))
            {
                isValid = false;
            }

            return isValid;
        }
    }
}