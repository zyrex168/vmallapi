﻿using FluentValidation;
using FluentValidation.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VmallService.RequestModel
{
    [Validator(typeof(WalletAdjustModelValidator))]
    public class WalletAdjustModel
    {
        public string memberId { get; set; }
        public string walletType { get; set; }
        public decimal amount { get; set; }
        public string remark { get; set; }

    }

    public class WalletAdjustModelValidator : AbstractValidator<WalletAdjustModel>
    {
        public WalletAdjustModelValidator()
        {
            List<string> walletTypeList = new List<string> { "R", "C", "E", "W", "S", "L", "A", "B", "O" };

            RuleFor(x => x.walletType).Must(x => walletTypeList.Contains(x)).WithMessage("Invalid Wallet Type");
            RuleFor(x => x.amount).NotNull().NotEqual(0).WithMessage("Amount cannot be zero");

        }
    }
}