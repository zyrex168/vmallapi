﻿using FluentValidation;
using FluentValidation.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VmallService.RequestModel
{
    [Validator(typeof(MemberBankInfoValidator))]
    public class MemberBankInfoModel
    {
        public string memberId { get; set; }
        public string fldAccNo { get; set; }
        public string fldAccHolder { get; set; }
        public int fldBankID { get; set; }
        public string fldBankBranch { get; set; }
    }

    public class MemberBankInfoValidator : AbstractValidator<MemberBankInfoModel>
    {
        public MemberBankInfoValidator()
        {
            RuleFor(x => x.fldAccNo).NotEmpty().WithMessage("*Bank account number cannot be blank")
                                    .Length(0, 40).WithMessage("*Invalid account number");

            RuleFor(x => x.fldAccHolder).NotEmpty().WithMessage("*Bank account name cannot be blank")
                                    .Length(0, 100).WithMessage("*Invalid Bank account name");

            RuleFor(x => x.fldBankID).GreaterThanOrEqualTo(0).WithMessage("*Bank cannot be blank");
                                    

        }
    }
}
 