﻿using FluentValidation;
using FluentValidation.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VmallService.RequestModel
{
    [Validator(typeof(RegisterMemberValidator))]
    public class RegisterMemberModel
    {
        public string fldSponsorCode { get; set; }
        public short fldSales { get; set; } //account type, 0="False Account" 1="True Account" 2="Free Account"
        public string placementId { get; set; }
        public int placementAcct { get; set; }
        public short placementPosition { get; set; } // 1-left, 2-right
        public string memberId { get; set; }
        public int fldRank { get; set; } // package 1-"Gold" 2-"Platinum" 3-"Diamond" 4-"Crown"
        public int freeVclub { get; set; }
        public string paymentType { get; set; }
        //Cash=Cash
        //CreditCard==CreditCard
        //Cheque = Cheque
        //EP/CP=EP(Register Point)+CP(Cash Point)
        //EP/SP=EP(Register Point)+SP(Sold Point)
        //EP/CP/SP=EP(Register Point)+CP(Cash Point)+SP(Sold Point)
        //EP=EP(Register Point)

        //wallet
        //C=CP(Cash Point)
        //E=EP(Register Point)
        //B=MP(Mall Point)
        //W=PP(Purchase Point)
        //A=AP(Ads Point)
        //O=SP(Sold Point)

        public string walletMemberId { get; set; }
        public decimal fldRWallet { get; set; }
        public decimal fldCWallet { get; set; }
        public decimal fldEWallet { get; set; }
        public decimal fldWWallet { get; set; }
        public decimal fldOWallet { get; set; }
        public decimal fldAWallet { get; set; }
        public decimal fldBWallet { get; set; }


        public string fldName { get; set; }
        public string fldICNo { get; set; }
        public string fldEMail { get; set; }
        public string fldWeChatID { get; set; }
        public string fldMobileNo { get; set; }
        public string fldAddress1 { get; set; }
        public string fldPostCode { get; set; }
        public string fldCountryID { get; set; }

        public string fldBRelationship { get; set; }
        public string fldBName { get; set; }
        public string fldBIC { get; set; }
        public string fldBContactNo { get; set; }
        public string fldBAddress { get; set; }
        public string fldBPostcode { get; set; }
        public string fldBCountryID { get; set; }

        public string fldAccNo { get; set; }
        public string fldAccHolder { get; set; }
        public int fldBankID { get; set; }
        public string fldBankBranch { get; set; }
    }

    public class RegisterMemberValidator : AbstractValidator<RegisterMemberModel>
    {
        public RegisterMemberValidator()
        {
            List<int> lstPlacement = new List<int> { 1, 2, 3 };
            List<int> lstSales = new List<int> { 1, 2 };
            List<int> lstPosition = new List<int> { 1, 2 };
            List<string> lstPayment = new List<string> { "CASH", "CREDITCARD", "CHEQUE", "EP/CP","EP/SP", "EP/CP/SP", "EP" , "FREE"};

            RuleFor(x => x.fldRank).NotNull().GreaterThan(0).WithMessage("*Invalid Package");
            RuleFor(x => x.memberId).NotNull().WithMessage("*Invalid member ID");
            RuleFor(x => x.fldName).Length(0, 500).WithMessage("Invalid name");
            RuleFor(x => x.placementId).NotNull().WithMessage("Invalid placement ID");
            RuleFor(x => x.fldICNo).Length(0, 50).WithMessage("Invalid IC number");
            RuleFor(x => x.fldEMail).NotEmpty().WithMessage("*E-mail address cannot be blank")
                                    .EmailAddress().WithMessage("*Invalid e-mail address");
            RuleFor(x => x.fldWeChatID).NotEmpty().WithMessage("*WeChat ID cannot be blank!")
                                       .Length(0, 50).WithMessage("Invalid WeChat ID");
            //RuleFor(x => x.fldBContactNo).NotEmpty().WithMessage("*Contact number cannot be blank")
            //                             .Length(0, 20).WithMessage("Invalid Contact number");

            RuleFor(x => x.fldCountryID).NotEmpty().WithMessage("Invalid Country");

            RuleFor(x => x.fldAccNo).NotEmpty().WithMessage("*Bank account number cannot be blank")
                                    .Length(0, 40).WithMessage("*Invalid account number");

            RuleFor(x => x.fldAccHolder).NotEmpty().WithMessage("*Bank account name cannot be blank")
                                    .Length(0, 100).WithMessage("*Invalid Bank account name");

            RuleFor(x => x.fldBankID).GreaterThanOrEqualTo(0).WithMessage("*Bank cannot be blank");

            RuleFor(x => x.fldBankID).GreaterThanOrEqualTo(0).WithMessage("*Bank cannot be blank");

            RuleFor(x => x.placementAcct).NotEmpty().Must(x => lstPlacement.Contains(x)).WithMessage("Invalid Placement");
            RuleFor(x => x.fldSales).NotEmpty().Must(x => lstSales.Contains(x)).WithMessage("Invalid Register Type");
            RuleFor(x => x.placementPosition).NotEmpty().Must(x => lstPosition.Contains(x)).WithMessage("Invalid Placement Position");
            RuleFor(x => x.paymentType).NotEmpty().Must(x => lstPayment.Contains(x)).WithMessage("Invalid Payment Type");
        }

    }
}