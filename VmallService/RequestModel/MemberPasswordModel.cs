﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FluentValidation;
using FluentValidation.Attributes;

namespace VmallService.RequestModel
{
    [Validator(typeof(MemberPasswordValidator))]
    public class MemberPasswordModel
    {
        public string memberId { get; set; }
        public string oldPassword { get; set; }
        public string newPassword { get; set; }
        public string confirmPassword { get; set; }
    }

    public class MemberPasswordValidator: AbstractValidator<MemberPasswordModel>
    {
        public MemberPasswordValidator()
        {
            When(ValidateOldPassword, () => {
                RuleFor(x => x.oldPassword).NotEmpty().WithMessage("*Current login password cannot be blank");
                RuleFor(x => x.newPassword).NotEqual(x => x.oldPassword).When(x => !string.IsNullOrEmpty(x.newPassword)).WithMessage("*New password cannot same with current password"); ;
            });

            
            RuleFor(x => x.newPassword).NotEmpty().WithMessage("*New login password cannot be blank")
                                       .MinimumLength(6).WithMessage("*Password must more than 6 characters");

            

            RuleFor(x => x.confirmPassword).NotEmpty().WithMessage("*Confirm login password cannot be blank");
            RuleFor(x => x.confirmPassword).Equal(x => x.newPassword).When(x => !string.IsNullOrEmpty(x.confirmPassword)).WithMessage("*Passwords not match");
                             
        }

        //if admin update, skip old password check
        private bool ValidateOldPassword(MemberPasswordModel input)
        {
            return string.IsNullOrEmpty(input.memberId);
        }
    }
}