﻿using FluentValidation;
using FluentValidation.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using VmallDataAccess;
using VmallService.CommonFunction;

namespace VmallService.RequestModel
{
    [Validator(typeof(MemberInfoUpdateValidator))]
    public class MemberInfoUpdateModel
    {
        public string memberId { get; set; }
        public string fldLanguage { get; set; }
        public string fldName { get; set; }
        public string fldICNo { get; set; }
        public string fldEMail { get; set; }
        public string fldWeChatID { get; set; }
        public string fldMobileNo { get; set; }
        public string fldAddress1 { get; set; }
        public string fldPostCode { get; set; }
        public string fldCountryID { get; set; }

        public string fldBRelationship { get; set; }
        public string fldBName { get; set; }
        public string fldBIC { get; set; }
        public string fldBContactNo { get; set; }
        public string fldBAddress { get; set; }
        public string fldBPostcode { get; set; }
        public string fldBCountryID { get; set; }
    }

    public class MemberInfoUpdateValidator : AbstractValidator<MemberInfoUpdateModel>
    {
        public MemberInfoUpdateValidator()
        {
           
            List<string> lstLang = new List<string> { "ZH-CN", "EN-US" };
            
            RuleFor(x => x.fldName).NotEmpty().WithMessage("*Name cannot be blank")
                                    .Length(0, 500).WithMessage("Invalid name");
//            RuleFor(x => x.fldICNo).NotEmpty().Length(0,50).WithMessage("Invalid IC number");
            RuleFor(x => x.fldLanguage).Must(x => lstLang.Contains(x)).WithMessage("Invalid Language");
            RuleFor(x => x.fldEMail).NotEmpty().WithMessage("*E-mail address cannot be blank")
                                    .EmailAddress().WithMessage("*Invalid e-mail address");
            RuleFor(x => x.fldWeChatID).NotEmpty().WithMessage("*WeChat ID cannot be blank!")
                                       .Length(0,50).WithMessage("Invalid WeChat ID");
            RuleFor(x => x.fldMobileNo).NotEmpty().WithMessage("*Contact number cannot be blank")
                                         .Length(0, 20).WithMessage("Invalid Contact number");

            RuleFor(x => x.fldCountryID).NotEmpty().WithMessage("Invalid Country");

        }

    }
}