﻿using FluentValidation;
using FluentValidation.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using VmallDataAccess;

namespace VmallService.RequestModel
{
    [Validator(typeof(MemberServiceCenterValidator))]
    public class MemberServiceCenterModel
    {
        public string memberId;
        public int serviceCenter;

    }

    public class MemberServiceCenterValidator : AbstractValidator<MemberServiceCenterModel>
    {
        public MemberServiceCenterValidator()
        {
            

            RuleFor(x => x.memberId).NotNull().NotEmpty().WithMessage("Invalid Member ID");
            RuleFor(x => x.memberId).NotNull().NotEmpty().When(isValidMember).WithMessage("*Member ID not found");

            RuleFor(x => x.serviceCenter).NotNull().WithMessage("Service Center cannot blank");
            RuleFor(x => x.serviceCenter).Must(isValidCenter).WithMessage("Invalid Service Center");
        }

        public bool isValidCenter(int sc)
        {
            bool blnResult = true;

            if(sc > 0)
            {
                List<long> lstSc = null;
                using (VmallDbEntities entities = new VmallDbEntities())
                {
                    var serviceCenter = entities.tblservicecentertypes.Select(x => x.fldID).ToArray();
                    lstSc = serviceCenter.ToList();
                }

                if (!lstSc.Contains(sc))
                    blnResult = false;
            }

            return blnResult;

        }

        public bool isValidMember(MemberServiceCenterModel input)
        {
            using (VmallDbEntities entities = new VmallDbEntities())
            {
                var member = entities.tblmemberships.SingleOrDefault(x => x.fldCode == input.memberId);

                if (member == null)
                    return false;
                else
                    return true;
            }

        }
    }
}