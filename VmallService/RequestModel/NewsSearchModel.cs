﻿using FluentValidation;
using FluentValidation.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VmallService.RequestModel
{
    [Validator(typeof(NewsSearchModelValidator))]
    public class NewsSearchModel
    {
        public int pageNumber { get; set; }
        public int pageSize { get; set; }
        public string fromDate { get; set; }
        public string toDate { get; set; }
        public string creatorName { get; set; }
        public string subject { get; set; }
        public string status { get; set; } // Y-active N-inactive blank-all
    }

    public class NewsSearchModelValidator : AbstractValidator<NewsSearchModel>
    {
        public NewsSearchModelValidator()
        {
            List<string> lstStatus = new List<string> {"", "Y", "D", "N" };

            RuleFor(x => x.status).Must(x => lstStatus.Contains(x)).WithMessage("Invalid status");
        }
    }
}