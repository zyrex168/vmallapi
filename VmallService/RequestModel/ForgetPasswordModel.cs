﻿using FluentValidation;
using FluentValidation.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VmallService.RequestModel
{
    [Validator(typeof(ForgetPasswordValidator))]
    public class ForgetPasswordModel
    {
        public string fldCode { get; set; }
        public string fldEMail { get; set; }
    }

    public class ForgetPasswordValidator : AbstractValidator<ForgetPasswordModel>
    {
        public ForgetPasswordValidator()
        {
            RuleFor(x => x.fldCode).NotNull().WithMessage("*Login ID cannot be blank");
            RuleFor(x => x.fldEMail).NotEmpty().WithMessage("*E-mail address cannot be blank")
                                    .EmailAddress().WithMessage("*Invalid e-mail address");
        }
    }

}