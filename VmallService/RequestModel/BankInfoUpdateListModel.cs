﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VmallService.RequestModel
{
    public class BankInfoUpdateListModel
    {
        public int pageNumber { get; set; }
        public int pageSize { get; set; }
        public string fromDate { get; set; }
        public string toDate { get; set; }
        public string memberId { get; set; }
        public string name { get; set; }
        public string icNumber { get; set; }
        public string status { get; set; } //blank=All, A=Approve, R=Reject, P=Pending
    }
}