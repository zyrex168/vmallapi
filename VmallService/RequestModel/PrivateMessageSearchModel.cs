﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VmallService.RequestModel
{
    public class PrivateMessageSearchModel
    {
        public int pageNumber { get; set; }
        public int pageSize { get; set; }
        public string fromDate { get; set; }
        public string toDate { get; set; }
        public string subject { get; set; }
        public string status { get; set; } // -1 ALL , 0 Unread, 1 Read
        public string fromMemberCode { get; set; }
        public string toMemberCode { get; set; }
    }
}