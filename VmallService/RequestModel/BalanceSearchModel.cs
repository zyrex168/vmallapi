﻿using FluentValidation;
using FluentValidation.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VmallService.RequestModel
{
    [Validator(typeof(BalanceSearchValidator))]
    public class BalanceSearchModel
    {
        public int pageNumber { get; set; }
        public int pageSize { get; set; }
        public string memberId { get; set; }
        public string name { get; set; }
        public string idNumber { get; set; }
    }

    public class BalanceSearchValidator : AbstractValidator<BalanceSearchModel>
    {
        public BalanceSearchValidator()
        {
            RuleFor(x => x.name).NotNull().WithMessage("Name cannot null");
            RuleFor(x => x.memberId).NotNull().WithMessage("Member ID cannot null");
            RuleFor(x => x.idNumber).NotNull().WithMessage("IC Number cannot null");
            
        }

        public bool ValidInput(BalanceSearchModel input)
        {
            bool isValid = true;

            if(string.IsNullOrEmpty(input.idNumber) &&
               string.IsNullOrEmpty(input.memberId) &&
               string.IsNullOrEmpty(input.name))
            {
                isValid = false;
            }

            return isValid;
        }
    }
}