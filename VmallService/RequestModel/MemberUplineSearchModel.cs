﻿using FluentValidation;
using FluentValidation.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VmallService.RequestModel
{
    [Validator(typeof(MemberUplineSearcValidator))]
    public class MemberUplineSearchModel
    {
        public int pageNumber { get; set; }
        public int pageSize { get; set; }
        public string memberId { get; set; }
    }

    public class MemberUplineSearcValidator : AbstractValidator<MemberUplineSearchModel>
    {
        public MemberUplineSearcValidator()
        {
            RuleFor(x => x.memberId).NotEmpty().WithMessage("*Member ID cannot be blank");
        }
    }
}