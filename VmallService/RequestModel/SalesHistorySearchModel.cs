﻿using FluentValidation;
using FluentValidation.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VmallService.RequestModel
{
    [Validator(typeof(SalesHistorySearchValidator))]
    public class SalesHistorySearchModel
    {
        public string memberCode { get; set; }
        public int pageNumber { get; set; }
        public int pageSize { get; set; }
        public string fromDate { get; set; }
        public string toDate { get; set; }
        public string tranType { get; set; }
        public string payType { get; set; }
        public string package { get; set; }
    }

    public class SalesHistorySearchValidator : AbstractValidator<SalesHistorySearchModel>
    {
        public SalesHistorySearchValidator()
        {
            //List<string> lsPayType = new List<string> {"", "EP", "EP/CP", "EP/SP", "Cash", "EP/CP/SP" };
            //List<string> lsTrxType = new List<string> { "", "UPGRADE", "REGISTER"};
            //List<string> lsTrxType = new List<string> { "", "UPGRADE", "REGISTER" };

            //RuleFor(x => x.payType).Must(x => lsPayType.Contains(x)).WithMessage("Invalid Payment type");
            //RuleFor(x => x.tranType).Must(x => lsTrxType.Contains(x)).WithMessage("Invalid Payment type");


        }
        
    }
}