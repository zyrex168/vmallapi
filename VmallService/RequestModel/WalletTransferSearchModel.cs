﻿using FluentValidation;
using FluentValidation.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VmallService.RequestModel
{
    public class WalletTransferSearchModel
    {
        public int pageNumber { get; set; }
        public int pageSize { get; set; }
        public string fromDate { get; set; }
        public string toDate { get; set; }
        public string walletType { get; set; } // 
    }

    [Validator(typeof(AdminWalletTransferSearchValidator))]
    public class AdminWalletTransferSearchModel
    {
        public int pageNumber { get; set; }
        public int pageSize { get; set; }
        public string memberId { get; set; }
        public string fromDate { get; set; }
        public string toDate { get; set; }
        public string walletType { get; set; } // { "R", "C", "E", "W", "S", "L", "A", "B", "O" };
    }

    public class AdminWalletTransferSearchValidator : AbstractValidator<AdminWalletTransferSearchModel>
    {
        public AdminWalletTransferSearchValidator()
        {
            List<string> trfType = new List<string> { "R", "C", "E", "W", "S", "L", "A", "B", "O" };

            RuleFor(x => x.memberId).NotEmpty().WithMessage("Member ID cannot be blank");
            RuleFor(x => x.walletType).Must(x => trfType.Contains(x)).WithMessage("Invalid Wallet Type");
        }
    }
}