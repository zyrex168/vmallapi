﻿using FluentValidation;
using FluentValidation.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VmallService.RequestModel
{
    [Validator(typeof(WalletTranHistSearchValidator))]
    public class WalletTranHistSearchModel
    {
        public int pageNumber { get; set; }
        public int pageSize { get; set; }
        public string fromDate { get; set; }
        public string toDate { get; set; }
        public string walletType { get; set; } // { "R", "C", "E", "W", "S", "L", "A", "B", "O" };
        public string tranType { get; set; } //ADJUST, TRANSFER, UPGRADE, REGISTER, TOPUP WALLET
        public string memberId { get; set; }
    }

    public class WalletTranHistSearchValidator : AbstractValidator<WalletTranHistSearchModel>
    {
        public WalletTranHistSearchValidator()
        {
            List<string> walletTypeList = new List<string> {"", "R", "C", "E", "W", "S", "L", "A", "B", "O" };
            List<string> tranTypeList = new List<string> {"", "ADJUST", "TRANSFER", "UPGRADE", "REGISTER", "TOPUP WALLET" };

            RuleFor(x => x.memberId).NotEmpty().When(x=>x.memberId != null).WithMessage("*Member ID cannot be blank");
            RuleFor(x => x.walletType).Must(x => walletTypeList.Contains(x)).WithMessage("Invalid Wallet Type");
            RuleFor(x => x.tranType.ToUpper())
                    .Must(x => tranTypeList.Contains(x))
                    .When(x=>x.tranType != null)
                    .WithMessage("Invalid Transaction Type");
        }

    }
}