﻿using FluentValidation;
using FluentValidation.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VmallService.RequestModel
{
    [Validator(typeof(PackageUpgradeValidator))]
    public class PackageUpgradeModel
    {
        public string memberId { get; set; }
        public int fldRank { get; set; } // package 1-"Gold" 2-"Platinum" 3-"Diamond" 4-"Crown"
        public int freeVclub { get; set; }
        public string paymentType { get; set; }
        public int placementAcct { get; set; }
        public decimal fldCWallet { get; set; }
        public decimal fldEWallet { get; set; }
        public decimal fldOWallet { get; set; }

        public class PackageUpgradeValidator : AbstractValidator<PackageUpgradeModel>
        {
            public PackageUpgradeValidator()
            {
                List<string> lstPayment = new List<string> { "CASH", "CREDITCARD", "CHEQUE", "EP/CP", "EP/SP", "EP/CP/SP", "EP", "FREE" };
                List<int> lstPlacement = new List<int> { 1, 2, 3 };

                RuleFor(x => x.fldRank).NotNull().GreaterThan(0).WithMessage("*Invalid Package");
                RuleFor(x => x.memberId).NotNull().WithMessage("*Invalid member ID");
                RuleFor(x => x.paymentType).NotEmpty().Must(x => lstPayment.Contains(x)).WithMessage("Invalid Payment Type");
                RuleFor(x => x.placementAcct).NotEmpty().Must(x => lstPlacement.Contains(x)).WithMessage("Invalid Placement");
            }
        }
    }
}