﻿using FluentValidation;
using FluentValidation.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using VmallDataAccess;

namespace VmallService.RequestModel
{
    [Validator(typeof(WalletWithdrawValidator))]
    public class WalletWithdrawModel
    {
        public string walletType { get; set; }
        public decimal amount { get; set; }
    }

    public class WalletWithdrawValidator : AbstractValidator<WalletWithdrawModel>
    {
        public WalletWithdrawValidator()
        {
            decimal minWithdraw = 0;

            using (VmallDbEntities entities = new VmallDbEntities())
            {
                var setting = entities.tblsettings.SingleOrDefault(x => x.fldSetting == "MinWithdrawAmount"); //MinWithdrawAmount
                decimal.TryParse(setting.fldValue, out minWithdraw);
            }
            List<string> trfType = new List<string> { "C", "E", "O" };

            RuleFor(x => x.walletType).Must(x => trfType.Contains(x)).WithMessage("Invalid Type");
            RuleFor(x => x.amount).GreaterThan(0).WithMessage("Transfer amount cannot be blank")
                                   .GreaterThan(0).WithMessage("Amount must equal or greather than " + Convert.ToString(minWithdraw));
        }
    }

}