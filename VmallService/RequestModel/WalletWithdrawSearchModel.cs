﻿using FluentValidation;
using FluentValidation.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VmallService.RequestModel
{
    [Validator(typeof(WalletWithdrawSearchValidator))]
    public class WalletWithdrawSearchModel
    {
        public int pageNumber { get; set; }
        public int pageSize { get; set; }
        public string fromDate { get; set; }
        public string toDate { get; set; }
        public string walletType { get; set; } //"", "R", "C", "E", "W", "S", "L", "A", "B", "O" 
        public string status { get; set; } // ""=All, A=Approved, R=Rejected, P=Pending 

        public string memberId { get; set; }
    }

    public class WalletWithdrawSearchValidator : AbstractValidator<WalletWithdrawSearchModel>
    {
        public WalletWithdrawSearchValidator()
        {
            List<string> walletTypeList = new List<string> {"", "R", "C", "E", "W", "S", "L", "A", "B", "O" };

            RuleFor(x => x.walletType)
                .Must(x => walletTypeList.Contains(x))
                .When(x=>x.walletType != null)
                .WithMessage("Invalid Wallet Type");
        }
    }
}