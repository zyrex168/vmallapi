﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VmallService.Models
{
    public class PrivateMessage
    {
        public string fldDateTime {get; set;}
        public string fldFrMID { get; set; }
        public string fldFrType { get; set; }
        public string fldToMID { get; set; }
        public string fldToType { get; set; }
        public string fldSubject { get; set; }
        public string fldContent { get; set; }
        public string fldFrRead { get; set; }
        public string fldToRead { get; set; }
        public string fldFrDelete { get; set; }
        public string fldToDelete { get; set; }
        public string fldReplyID { get; set; }
    }
}