﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Web;
using VmallDataAccess;

namespace VmallService.CommonFunction
{
    public static class FuncTool
    {
        public static string GenerateRef(string prefix)
        {
            DateTime dt = DateTime.Now;
            string month = "";

            switch (dt.Month)
            {
                case 1:
                    month = "J";
                    break;
                case 2:
                    month = "F";
                    break;
                case 3:
                    month = "M";
                    break;
                case 4:
                    month = "A";
                    break;
                case 5:
                    month = "Y";
                    break;
                case 6:
                    month = "N";
                    break;
                case 7:
                    month = "L";
                    break;
                case 8:
                    month = "G";
                    break;
                case 9:
                    month = "S";
                    break;
                case 10:
                    month = "O";
                    break;
                case 11:
                    month = "V";
                    break;
                case 12:
                    month = "D";
                    break;
            }

            string str = prefix + month + dt.ToString("dd") + dt.ToString("yy") + dt.ToString("HHmmfff");
            return str;
        }

        public static string Translate(string lang, string enText)
        {
            string strText = enText;

            using (VmallDbEntities entities = new VmallDbEntities())
            {

                if (enText.Length > 100)
                    return enText;

                var query = entities.tblmultilanguages
                                    .Where(p => p.fldEN_US.Equals(enText, StringComparison.OrdinalIgnoreCase))
                                    .ToList();
                if (query != null && query.Count > 0)
                {
                    var language = query.Select(x => new
                    {
                        fldZH_CN = (x.fldZH_CN.Equals("") || x.fldZH_CN == null) ? x.fldEN_US : x.fldZH_CN
                    })
                    .SingleOrDefault();

                    if(lang.ToUpper() == "ZH-CN")
                        strText = language.fldZH_CN;
                }
                else
                {
                    tblmultilanguage objLang = new tblmultilanguage();
                    objLang.fldDefault = enText;
                    objLang.fldEN_US = enText;
                    objLang.fldText = enText;
                    objLang.fldZH_CN = "";

                    entities.tblmultilanguages.Add(objLang);
                    entities.SaveChanges();

                }

                
            }
            return strText;
        }

        public static string GetThreadLanguage()
        {
            LocalDataStoreSlot lds = System.Threading.Thread.GetNamedDataSlot("lang");
            string strLang = System.Threading.Thread.GetData(lds).ToString();
            return strLang;
        }

        public static void SetThreadLanguage(string strLang)
        {
            LocalDataStoreSlot lds = System.Threading.Thread.AllocateNamedDataSlot("lang");
            System.Threading.Thread.SetData(lds, strLang);
        }

        public static string GetBasicAuthenUser(string base64String)
        {
            if (string.IsNullOrEmpty(base64String))
                return "";

            string decodeToken = Encoding.UTF8.GetString(Convert.FromBase64String(base64String));
            string[] usernamePassword = decodeToken.Split(':');

            string username = usernamePassword[0];
            string password = usernamePassword[1];

            return username;
        }

        public static DataTable ExceuteSql(DbContext context, string sqlQuery)
        {
            DbProviderFactory dbFactory = DbProviderFactories.GetFactory(context.Database.Connection);

            using (var cmd = dbFactory.CreateCommand())
            {
                cmd.Connection = context.Database.Connection;
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = sqlQuery;
                using (DbDataAdapter adapter = dbFactory.CreateDataAdapter())
                {
                    adapter.SelectCommand = cmd;

                    DataTable dt = new DataTable();
                    adapter.Fill(dt);

                    return dt;
                }
            }
        }

        public static void SendEmail(string smtpServer, int port, string userId, string password, bool isSsl,
                                    string from, string subject, string emailAddr, string content)
        {

            //MailMessage message = new MailMessage(txtFrom.Text, txtAddr.Text, txtSubject.Text, txtText.Text);

            SmtpClient emailClient = new SmtpClient(smtpServer);

            emailClient.Port = port;


            using (MailMessage message = new MailMessage())
            {
                message.From = new MailAddress(from);
                message.Subject = subject;
                message.Body = content;
                message.IsBodyHtml = true;
                message.To.Add(new MailAddress(emailAddr));

                NetworkCredential SMTPUserInfo = new NetworkCredential(userId, password);
                emailClient.UseDefaultCredentials = false;
                emailClient.Credentials = SMTPUserInfo;


                emailClient.EnableSsl = isSsl;

                emailClient.DeliveryMethod = SmtpDeliveryMethod.Network;

                emailClient.Send(message);
            }
        }
    }


}